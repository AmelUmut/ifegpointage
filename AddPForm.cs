﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace AttLogs
{
    public partial class AddPForm : MetroForm
    {
        public AddPForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void addPoint_Click(object sender, EventArgs e)
        {
            
            string NomP = nameP.Text;
            string IpPoint = ipAdres.Text;
            string Port = port.Text;
            bool isprinc = IsPrinc.Checked;
            System.Net.IPAddress b;
            int c;
            if (string.IsNullOrWhiteSpace(NomP))
                MessageBox.Show("Veuillez Introduire le nom", "Alert");
            else
                if (string.IsNullOrWhiteSpace(IpPoint) )
                MessageBox.Show("Veuillez Introduire l'adresse IP ", "Alert");
            else
                if (string.IsNullOrWhiteSpace(Port))
                MessageBox.Show("Veuillez Introduire le Port", "Alert");
            else
                if (System.Net.IPAddress.TryParse(IpPoint, out b) == false)
            {
                MessageBox.Show("Format de donnée inccorcet,veuillez respecter le format d'adresse IP", "Erreur");
            }
            else
                 if (Int32.TryParse(Port, out c) == false)
            {
                MessageBox.Show("Format de donnée inccorcet,veuillez vérifier la valeur du Port", "Erreur");
               
            }
            else
            {
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                DataRow row;
               // DataSet ds = new PointeuseDBDataSet1();
                DataTable dataTable = new DataTable();
                Cursor = Cursors.WaitCursor;
                try
                {
                    conn.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT *  FROM Pointeuse", conn);
                    OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                    adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    adapter.Fill(dataTable);
                    bool insert = true;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i]["Nom"].ToString() == NomP)
                        {
                            MessageBox.Show("Vous avez déja déclaré une pointeuse avec le méme nom,veuillez vérifier la liste des pointeuses.", "Alert");
                            insert = false;
                            i = dataTable.Rows.Count;
                        }
                        else
                         if (dataTable.Rows[i]["IP"].ToString() == IpPoint && dataTable.Rows[i]["Port"].ToString() == Port)
                        {
                            MessageBox.Show("Vous avez déja déclaré une pointeuse avec ces paramétres,veuillez vérifier la liste des pointeuses.", "Alert");
                            insert = false;
                            i = dataTable.Rows.Count;
                        }
                        else

                        if (isprinc == true && dataTable.Rows[i]["IsPrincipal"].ToString() == "1")
                        {
                            MessageBox.Show("Vous avez déja déclaré une pointeuse Principale,veuillez vérifier la liste des pointeuses.", "Alert");
                            insert = false;
                            i = dataTable.Rows.Count;
                        }
                       
                    }
                    //Insert new Row
                    if (insert == true)
                    {
                        row = dataTable.NewRow();
                        row["Nom"] = NomP;
                        row["IP"] = IpPoint;
                        row["Port"] = Port;
                        if(isprinc==true)
                        row["IsPrincipal"] = "1";
                        else
                            row["IsPrincipal"] = "0";
                        dataTable.Rows.Add(row);
                        adapter.Update(dataTable);
                        dataTable.AcceptChanges();
                        MessageBox.Show("la pointeuse a été enregistrée,l'application va redémarrer pour appliquer les changement", "succès");
                      
                        this.Close();
                        Application.Restart();
                        Program.log.Info("Ajouter une pointeuse,"+ NomP);
                       
                    }
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message.ToString());
                    Program.log.Error("Ajouter une pointeuse," + exp.Message.ToString());
                }

                if (conn.State == ConnectionState.Open)
                    conn.Close();
                

                Cursor = Cursors.Default;
            }
        }
    }
}
