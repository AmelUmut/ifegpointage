﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.OleDb;
using System.Windows.Forms;

namespace AttLogs
{
    static class Program
    {
        /// <summary>
        ///The main entry point for the application.
        /// </summary>
        /// 
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [STAThread]
        static void Main()
        {
            log.Info("Application is Starting");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());

            
        }
    }
}