﻿using System.Windows.Forms;

namespace AttLogs
{
    partial class ParamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParamForm));
            this.heurE = new System.Windows.Forms.Label();
            this.labelE2 = new System.Windows.Forms.Label();
            this.labelS1 = new System.Windows.Forms.Label();
            this.labelS2 = new System.Windows.Forms.Label();
            this.heureE1 = new System.Windows.Forms.DateTimePicker();
            this.heureS1 = new System.Windows.Forms.DateTimePicker();
            this.heureE2 = new System.Windows.Forms.DateTimePicker();
            this.heureS2 = new System.Windows.Forms.DateTimePicker();
            this.valider = new System.Windows.Forms.Button();
            this.annuler = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // heurE
            // 
            this.heurE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heurE.AutoSize = true;
            this.heurE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heurE.Location = new System.Drawing.Point(68, 67);
            this.heurE.Name = "heurE";
            this.heurE.Size = new System.Drawing.Size(87, 14);
            this.heurE.TabIndex = 2;
            this.heurE.Text = "Heure Entrée";
            // 
            // labelE2
            // 
            this.labelE2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelE2.AutoSize = true;
            this.labelE2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelE2.Location = new System.Drawing.Point(68, 168);
            this.labelE2.Name = "labelE2";
            this.labelE2.Size = new System.Drawing.Size(64, 14);
            this.labelE2.TabIndex = 3;
            this.labelE2.Text = "Fin Pause";
            // 
            // labelS1
            // 
            this.labelS1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelS1.AutoSize = true;
            this.labelS1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelS1.Location = new System.Drawing.Point(68, 115);
            this.labelS1.Name = "labelS1";
            this.labelS1.Size = new System.Drawing.Size(85, 14);
            this.labelS1.TabIndex = 4;
            this.labelS1.Text = "Début Pause";
            // 
            // labelS2
            // 
            this.labelS2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelS2.AutoSize = true;
            this.labelS2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelS2.Location = new System.Drawing.Point(68, 220);
            this.labelS2.Name = "labelS2";
            this.labelS2.Size = new System.Drawing.Size(88, 14);
            this.labelS2.TabIndex = 6;
            this.labelS2.Text = "Heure Sortie ";
            // 
            // heureE1
            // 
            this.heureE1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heureE1.CustomFormat = "HH:mm:ss";
            this.heureE1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heureE1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.heureE1.Location = new System.Drawing.Point(285, 65);
            this.heureE1.Name = "heureE1";
            this.heureE1.ShowUpDown = true;
            this.heureE1.Size = new System.Drawing.Size(95, 21);
            this.heureE1.TabIndex = 8;
            // 
            // heureS1
            // 
            this.heureS1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heureS1.CustomFormat = "HH:mm:ss";
            this.heureS1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heureS1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.heureS1.Location = new System.Drawing.Point(285, 112);
            this.heureS1.Name = "heureS1";
            this.heureS1.ShowUpDown = true;
            this.heureS1.Size = new System.Drawing.Size(95, 21);
            this.heureS1.TabIndex = 9;
            // 
            // heureE2
            // 
            this.heureE2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heureE2.CustomFormat = "HH:mm:ss";
            this.heureE2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heureE2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.heureE2.Location = new System.Drawing.Point(285, 165);
            this.heureE2.Name = "heureE2";
            this.heureE2.ShowUpDown = true;
            this.heureE2.Size = new System.Drawing.Size(95, 21);
            this.heureE2.TabIndex = 11;
            // 
            // heureS2
            // 
            this.heureS2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heureS2.CustomFormat = "HH:mm:ss";
            this.heureS2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heureS2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.heureS2.Location = new System.Drawing.Point(285, 217);
            this.heureS2.Name = "heureS2";
            this.heureS2.ShowUpDown = true;
            this.heureS2.Size = new System.Drawing.Size(95, 21);
            this.heureS2.TabIndex = 12;
            // 
            // valider
            // 
            this.valider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.valider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.valider.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valider.Location = new System.Drawing.Point(378, 417);
            this.valider.Name = "valider";
            this.valider.Size = new System.Drawing.Size(91, 31);
            this.valider.TabIndex = 14;
            this.valider.Text = "Enregistrer";
            this.valider.UseVisualStyleBackColor = true;
            this.valider.Click += new System.EventHandler(this.valider_Click);
            // 
            // annuler
            // 
            this.annuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.annuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.annuler.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.annuler.Location = new System.Drawing.Point(23, 417);
            this.annuler.Name = "annuler";
            this.annuler.Size = new System.Drawing.Size(85, 31);
            this.annuler.TabIndex = 15;
            this.annuler.Text = "Annuler";
            this.annuler.UseVisualStyleBackColor = true;
            this.annuler.Click += new System.EventHandler(this.annuler_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.heureE2);
            this.groupBox1.Controls.Add(this.heurE);
            this.groupBox1.Controls.Add(this.labelE2);
            this.groupBox1.Controls.Add(this.labelS1);
            this.groupBox1.Controls.Add(this.labelS2);
            this.groupBox1.Controls.Add(this.heureE1);
            this.groupBox1.Controls.Add(this.heureS1);
            this.groupBox1.Controls.Add(this.heureS2);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(23, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(446, 307);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Location = new System.Drawing.Point(25, 204);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(392, 42);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Location = new System.Drawing.Point(25, 151);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(392, 42);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Location = new System.Drawing.Point(25, 98);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(392, 42);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Location = new System.Drawing.Point(25, 50);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(392, 42);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            // 
            // ParamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 465);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.annuler);
            this.Controls.Add(this.valider);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ParamForm";
            this.Text = "Emploi du Temps";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Label heurE;
        private Label labelE2;
        private Label labelS1;
        private Label labelS2;
        public DateTimePicker heureE1;
        public DateTimePicker heureS1;
        public DateTimePicker heureE2;
        public DateTimePicker heureS2;
        public Button valider;
        public Button annuler;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox5;
        private GroupBox groupBox4;
        private GroupBox groupBox3;
    }
}