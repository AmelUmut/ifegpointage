﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Collections;

namespace AttLogs
{
    public partial class UploadUserForm : MetroForm
    {
        public UploadUserForm()
        {
            InitializeComponent();
            get_Struct();
            structure.SelectedIndexChanged += structure_SelectedIndexChanged;
        }
        private void structure_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBox1.Text;
            string matrcicule = Matricule.Text;
            string prenom = textBox2.Text;
            string username = name + " " + prenom;
            string emp = emploie.Text;
            bool controlVal = true;
            int selectedIndex =0,c;
            string id = ident.Text;
            DictionaryEntry deImgType;
            string structur = "";
            if (structure.SelectedItem != null)
            {
                deImgType = (DictionaryEntry)structure.SelectedItem;
                structur = deImgType.Value.ToString();
            }
            if (id == "")
            {
                MessageBox.Show("Veuillez indiquer l'identifiant", "Alert");
            }
            else
            if (Int32.TryParse(id, out c) == false)
            {
                MessageBox.Show("formt de l'identifiant est incorrect", "Alert");
            }
            else

            if (name == "")
            {
                MessageBox.Show("Veuillez indiquer le nom de l'employé","Alert");
            }
            else
                 if (prenom == "")
            {
                MessageBox.Show("Veuillez indiquer le Prénom de l'employé", "Alert");
            }
            else
                 if (matrcicule == "")
            {
                MessageBox.Show("Veuillez indiquer le Matricule de l'employé", "Alert");
            }
            else
                 if (structur == "")
            {
                MessageBox.Show("Veuillez indiquer la structure", "Alert");
            }
           
            else
            {
                // Search for new user;
                Cursor = Cursors.WaitCursor;

                if (AttLogsMain.IsConnected.Count == 0)
                {
                    MessageBox.Show("l'application ne contient aucune pointeuse,veuillez ajouter au moins une pointeuse!", "Error");
                    Cursor = Cursors.Default;
                    return;
                }
                if (AttLogsMain.inedxP == -1)
                {
                    MessageBox.Show("veuillez indiquer la pointeuse principale dans la liste des pointeuses!", "Error");
                    Cursor = Cursors.Default;
                    return;
                }

                if (AttLogsMain.IsConnected[AttLogsMain.inedxP] == false)
                {
                    MessageBox.Show("la pointeuse principale n'est pas Connectées!", "Error");
                    Cursor = Cursors.Default;
                    return;
                }

               
                    //AttLogsMain.LastId(AttLogsMain.axCZKEM[AttLogsMain.inedxP]).ToString();
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

                try
                {
                    conn.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM Employer", conn);
                    OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        string mat = dataTable.Rows[i][2].ToString();
                        if (mat == matrcicule)
                        {
                            MessageBox.Show("vous avez déja inséré un employer avec le même matricule!", "Error");
                            Cursor = Cursors.Default;
                            return;
                        }
                    }
                    int idwErrorCode = 0;
                    string Password = null;
                    AttLogsMain.axCZKEM[AttLogsMain.inedxP].EnableDevice(AttLogsMain.iMachineNumber, false);


                    if (AttLogsMain.axCZKEM[AttLogsMain.inedxP].SSR_SetUserInfo(AttLogsMain.iMachineNumber, id, username, Password, selectedIndex, controlVal))//upload user information to the device
                    {
                        DataRow row;
                        
                        row = dataTable.NewRow();
                        row["Identifiant"] = id;
                        row["Matricule"] = matrcicule;
                        row["Nom"] = name;
                        row["Prenom"] = prenom;
                        row["LibSP"] = structur;
                        row["LibEmp"] = emp;
                        row["IsActif"] = "1";
                        row["Pointeuse"] = AttLogsMain.CheckBoxes[AttLogsMain.inedxP].Text;
                        dataTable.Rows.Add(row);
                        adapter.Update(dataTable);
                        dataTable.AcceptChanges();
                        MessageBox.Show(AttLogsMain.CheckBoxes[AttLogsMain.inedxP].Text + ":Utilisateur créé avec succès");
                    }
                    else
                    {
                        AttLogsMain.axCZKEM[AttLogsMain.inedxP].GetLastError(ref idwErrorCode);
                        MessageBox.Show(AttLogsMain.CheckBoxes[AttLogsMain.inedxP].Text + ":Echec failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                        AttLogsMain.axCZKEM[AttLogsMain.inedxP].EnableDevice(AttLogsMain.iMachineNumber, true);
                        Cursor = Cursors.Default;
                        return;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                    Program.log.Error("UploadUser" + ex.Message.ToString());

                }
                finally
                {
                    conn.Close();

                }

                AttLogsMain.axCZKEM[AttLogsMain.inedxP].RefreshData(AttLogsMain.iMachineNumber);//the data in the device should be refreshe                     
                AttLogsMain.axCZKEM[AttLogsMain.inedxP].EnableDevice(AttLogsMain.iMachineNumber, true);

                Cursor = Cursors.Default;
                this.Close();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void get_Struct()
        {

            structure.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib from Structures order by lib", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();

                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["ID"].ToString();
                    nom = dataTable.Rows[i]["lib"].ToString();
                    htPointInf.Add(ident, nom);
                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        structure.Items.Add(PointInf);
                    }
                    //users.Items.Add("");
                    structure.DisplayMember = "value";
                    structure.ValueMember = "key";
                    structure.AutoCompleteMode = AutoCompleteMode.Suggest;
                    structure.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("uploaduserform_listeStruct" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }
    }
}
