﻿using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using MetroFramework.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace AttLogs
{
    public partial class Pointages : MetroForm
    {
        
        public Pointages()
        {
            InitializeComponent();
            get_Users();
            get_Struct();
            get_calc_pointage();
        }
        private void get_calc_pointage()
        {
            DataTable dataTable = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
        
            try
            {
                conn.Open();
                string sql = "select IIF(Jour IS NULL,DateValue('" + get_LastPres() + "'),Jour)AS aa,Nom,Prenom, IIF(AM IS NULL,'Absent',AM)AS AMidi, IIF(PM IS NULL,'Absent',PM)As ApMidi,IIF(Retard IS NULL,'--',Retard)AS Retard1,IIF(Retard IS NULL,'--',Retard1)AS Retard2,LibSP from Employer LEFT OUTER JOIN Presence on(Presence.IdUser = Employer.Identifiant and DateValue(Presence.Jour) = DateValue('" + get_LastPres() + "') ) where IsActif='1' order by Nom,Jour";
                
                OleDbDataAdapter dAdapter = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.Fill(dataTable);
                pointageGrid.DataSource = dataTable;
                pointageGrid.Columns["aa"].HeaderText = "Jour";
                pointageGrid.Columns["AMidi"].HeaderText = "Matin";
                pointageGrid.Columns["ApMidi"].HeaderText = "Après-Midi";
                pointageGrid.Columns["Prenom"].HeaderText = "Prénom";
                pointageGrid.Columns["LibSP"].HeaderText = "Structure";
                result.Text = dataTable.Rows.Count.ToString();
               
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("FichePointage_getcal" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
           
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
           this.Close();
        }
        private void absent_Click(object sender, EventArgs e)
        {
           
            if (matin.Checked == true || soir.Checked == true)
            {
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                DataTable t1=new DataTable();
                string jour = begin_date.Value.ToShortDateString();
                try
                {
                    conn.Open();
                    string sql = "select IIF(Jour IS NULL,DateValue('" + jour + "'),Jour)AS aa,Nom,Prenom, IIF(AM IS NULL,'Absent',AM)AS AMidi, IIF(PM IS NULL,'Absent',PM)As ApMidi,IIF(Retard IS NULL,'--',Retard)AS Retard1,IIF(Retard IS NULL,'--',Retard1)AS Retard2,LibSP from Employer LEFT OUTER JOIN Presence on(Presence.IdUser = Employer.Identifiant and DateValue(Presence.Jour) = DateValue('" + jour + "') ) where IsActif='1'  order by Nom,Jour";

                    OleDbDataAdapter dAdapter = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);

                    OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                    dAdapter.Fill(t1);

                    pointageGrid.DataSource = t1;
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                    Program.log.Error("Pointage_getcal" + ex.Message.ToString());
                }
                finally
                {

                    conn.Close();
                }
              
                if (matin.Checked == true && soir.Checked == false)
                {

                    (pointageGrid.DataSource as DataTable).DefaultView.RowFilter = string.Format("AMidi = '{0}'", "Absent");

                }
               
                if (matin.Checked == false && soir.Checked == true)
                {
                    (pointageGrid.DataSource as DataTable).DefaultView.RowFilter = string.Format("ApMidi = '{0}'", "Absent");

                }
                if (matin.Checked == true && soir.Checked == true)
                {
                    (pointageGrid.DataSource as DataTable).DefaultView.RowFilter = string.Format("AMidi = '{0}'", "Absent")+ string.Format("AND ApMidi = '{0}'", "Absent");

                }
                
                //pointageGrid.Columns["aa"].HeaderText = "Jour";
                //pointageGrid.Columns["AMidi"].HeaderText = "Matin";
                //pointageGrid.Columns["ApMidi"].HeaderText = "Après-Midi";
                //pointageGrid.Columns["Prenom"].HeaderText = "Prénom";
                //pointageGrid.Columns["LibSP"].HeaderText = "Structure";
            }
            else
            {
                MessageBox.Show("veuillez cochez au moins une case(Matin,Après midi)", "Alert");
                return;
            }

        }
        public void btnSearch_Click(object sender, EventArgs e)
        {
            if ((startdate.Text != enddate.Text) && (Convert.ToDateTime(startdate.Text.ToString()) > Convert.ToDateTime(enddate.Text.ToString())))
            {
                MessageBox.Show("la date de début doit être inférieure ou égale à la date de fin!! ", "Alert");
                return;
            }
            
            DictionaryEntry deImgType;
            string ident = "";
            if (users.SelectedItem != null)
            {
                deImgType = (DictionaryEntry)users.SelectedItem;
                ident = deImgType.Key.ToString();
            }
            DataTable dataTable = new DataTable();
            DataTable dataTable1 = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                string sql = null,jour=null;
                sql = "select distinct Jour from Presence where DateValue(Jour) >= DateValue('" + startdate.Text + "') and DateValue(Jour) <= DateValue('" + enddate.Text + "')  ";
                OleDbDataAdapter dAdapter;
                OleDbCommandBuilder cBuilder;
                OleDbDataAdapter dAdapter1 = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilde1r = new OleDbCommandBuilder(dAdapter1);
                dAdapter1.Fill(dataTable1);
                if (dataTable1.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        jour = dataTable1.Rows[i][0].ToString();
                        if (ident != "")
                            sql = "select IIF(Jour IS NULL,DateValue('" + jour + "'),Jour)AS aa,Nom,Prenom, IIF(AM IS NULL,'Absent',AM)AS AMidi, IIF(PM IS NULL,'Absent',PM)As ApMidi,IIF(Retard IS NULL,'--',Retard)AS Retard1,IIF(Retard IS NULL,'--',Retard1)AS Retard2,LibSP from Employer LEFT OUTER JOIN Presence on(Presence.IdUser = Employer.Identifiant and DateValue(Presence.Jour) = DateValue('" + jour + "') ) where Employer.Identifiant='"+ident+"' and IsActif='1' order by Nom";

                        else
                            sql = "select IIF(Jour IS NULL,DateValue('" + jour + "'),Jour)AS aa,Nom,Prenom, IIF(AM IS NULL,'Absent',AM)AS AMidi, IIF(PM IS NULL,'Absent',PM)As ApMidi,IIF(Retard IS NULL,'--',Retard)AS Retard1,IIF(Retard IS NULL,'--',Retard1)AS Retard2,LibSP from Employer LEFT OUTER JOIN Presence on(Presence.IdUser = Employer.Identifiant and DateValue(Presence.Jour) = DateValue('" + jour + "') ) where IsActif='1' order by Nom";

                        dAdapter = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                        cBuilder = new OleDbCommandBuilder(dAdapter);
                        dAdapter.Fill(dataTable);
                    }
                }
                if (dataTable.Rows.Count > 0)
                {
                    pointageGrid.DataSource = dataTable;
                    pointageGrid.Columns["aa"].HeaderText = "Jour";
                    pointageGrid.Columns["AMidi"].HeaderText = "Matin";
                    pointageGrid.Columns["ApMidi"].HeaderText = "Après-Midi";
                    pointageGrid.Columns["Prenom"].HeaderText = "Prénom";
                    pointageGrid.Columns["LibSP"].HeaderText = "Structure";
                    result.Text = dataTable.Rows.Count.ToString();
                }
                else
                {
                    MessageBox.Show("Aucune information trouvée", "Alert");
                    pointageGrid.DataSource = null;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageBrute_search" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }

        }

        private void search1_Click(object sender, EventArgs e)
        {
            if ((startdate1.Text != enddate1.Text) && (Convert.ToDateTime(startdate1.Text.ToString()) > Convert.ToDateTime(enddate1.Text.ToString())))
            {
                MessageBox.Show("la date de début doit être inférieure ou égale à la date de fin!! ", "Alert");
                return;
            }


            DictionaryEntry deImgType;
            string ident = "";
            if (structure.SelectedItem != null)
            {
                deImgType = (DictionaryEntry)structure.SelectedItem;
                ident = deImgType.Key.ToString();
            }
            DataTable dataTable = new DataTable();
            DataTable dataTable1 = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();

                string sql = null,jour;
                sql = "select distinct Jour from Presence where DateValue(Jour) >= DateValue('" + startdate1.Text + "') and DateValue(Jour) <= DateValue('" + enddate1.Text + "')  ";
                OleDbDataAdapter dAdapter;
                OleDbCommandBuilder cBuilder;
                OleDbDataAdapter dAdapter1 = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilde1r = new OleDbCommandBuilder(dAdapter1);
                dAdapter1.Fill(dataTable1);
                if (dataTable1.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        jour = dataTable1.Rows[i][0].ToString();
                        if (ident != "")
                           sql = "select IIF(Jour IS NULL,DateValue('" + jour + "'),Jour)AS aa,Nom,Prenom, IIF(AM IS NULL,'Absent',AM)AS AMidi, IIF(PM IS NULL,'Absent',PM)As ApMidi,IIF(Retard IS NULL,'--',Retard)AS Retard1,IIF(Retard IS NULL,'--',Retard1)AS Retard2,LibSP from Employer LEFT OUTER JOIN Presence on(Presence.IdUser = Employer.Identifiant and DateValue(Presence.Jour) = DateValue('" + jour + "') ) where LibSP like'%" + ident + "%' and IsActif='1' order by Nom";

                        else
                           sql = "select IIF(Jour IS NULL,DateValue('" + jour + "'),Jour)AS aa,Nom,Prenom, IIF(AM IS NULL,'Absent',AM)AS AMidi, IIF(PM IS NULL,'Absent',PM)As ApMidi,IIF(Retard IS NULL,'--',Retard)AS Retard1,IIF(Retard IS NULL,'--',Retard1)AS Retard2,LibSP from Employer LEFT OUTER JOIN Presence on(Presence.IdUser = Employer.Identifiant and DateValue(Presence.Jour) = DateValue('" + jour + "') ) where IsActif='1' order by Nom";

                        dAdapter = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                        cBuilder = new OleDbCommandBuilder(dAdapter);
                        dAdapter.Fill(dataTable);
                    }
                }
                if (dataTable.Rows.Count > 0)
                {
                    pointageGrid.DataSource = dataTable;
                    pointageGrid.Columns["aa"].HeaderText = "Jour";
                    pointageGrid.Columns["AMidi"].HeaderText = "Matin";
                    pointageGrid.Columns["ApMidi"].HeaderText = "Après-Midi";
                    pointageGrid.Columns["Prenom"].HeaderText = "Prénom";
                    pointageGrid.Columns["LibSP"].HeaderText = "Structure";
                    result.Text = dataTable.Rows.Count.ToString();
                }
                else
                {
                    MessageBox.Show("Aucune information trouvée", "Alert");
                    pointageGrid.DataSource = null;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointagefiche_search" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }


        }

        public void get_Users()
        {

            users.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Identifiant,Nom,Prenom from Employer where IsActif='1'", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();

                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["Identifiant"].ToString();
                    nom = dataTable.Rows[i]["Nom"].ToString() + " " + dataTable.Rows[i]["Prenom"].ToString();
                    htPointInf.Add(ident, nom);

                }
                if (i > 0)
                {
                   
                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        users.Items.Add(PointInf);
                    }
                    //users.Items.Add("");
                    users.DisplayMember = "value";
                    users.ValueMember = "key";
                    users.AutoCompleteMode = AutoCompleteMode.Suggest;
                    users.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageBrute_getuser" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }

        private void Export_Click(object sender, EventArgs e)
        {

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "export.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ToCsV(dataGridView1, @"c:\export.xls");
                ToCsV(pointageGrid, sfd.FileName); // Here dataGridview1 is your grid view name
            }

        }

        private void ToCsV(DataGridView dGV, string filename)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                // creating new WorkBook within Excel application
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                // creating new Excelsheet in workbook
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                // see the excel sheet behind the program
                app.Visible = true;
                // get the reference of first sheet. By default its name is Sheet1.
                // store its reference to worksheet
                worksheet = (Excel.Worksheet)workbook.Sheets[1];
                //worksheet = workbook.ActiveSheet;
                // changing the name of active sheet
                worksheet.Name = "Pointages";

                // storing header part in Excel
                int k = 0;
                for (int i = 1; i < pointageGrid.Columns.Count; i++)
                {
                    worksheet.Cells[1, i] = pointageGrid.Columns[k].HeaderText;
                    k++;
                }
                // storing Each row and column value to excel sheet

                for (int i = 0; i < pointageGrid.Rows.Count; i++)
                {
                    k = 0;
                    for (int j = 0; j < pointageGrid.Columns.Count - 1; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = pointageGrid.Rows[i].Cells[k].Value.ToString();
                        k++;
                    }
                }
                app.DisplayAlerts = false; // Without this you will get two confirm overwrite prompts
                                           // Save the excel file under the captured location from the SaveFileDialog
                workbook.SaveAs(filename, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                workbook.Close(true, Type.Missing, Type.Missing);
                app.Quit();
                releaseObject(worksheet);
                releaseObject(workbook);
                releaseObject(app);
                // Clear Clipboard and DataGridView selection
                Clipboard.Clear();
                dGV.ClearSelection();
                // Open the newly saved excel file
                if (File.Exists(filename))
                    System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {
            
                Program.log.Error("To CSV," + ex.ToString());
            }

        }
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception lors de généraion du fichier excel" + ex.ToString());
                Program.log.Error("pointageBrute_Excel" + ex.Message.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        public void get_Struct()
        {

            this.structure.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib from Structures", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                System.Data.DataTable dataTable = new System.Data.DataTable();

                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["lib"].ToString();
                    nom = dataTable.Rows[i]["lib"].ToString();
                    htPointInf.Add(ident, nom);

                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        structure.Items.Add(PointInf);
                    }
                    structure.DisplayMember = "value";
                    structure.ValueMember = "key";
                    structure.SelectedIndex = -1;
                    structure.AutoCompleteMode = AutoCompleteMode.Suggest;
                    structure.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_get_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            var myForm = new PresentRepport();
            myForm.Show();
        }
        public static string get_LastPres()
        {
            string ident = "01/01/" + DateTime.Now.Year;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select top 1 Jour from Presence", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                System.Data.DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    dAdapter = new OleDbDataAdapter("select max(Jour) from Presence", Settings.Default.PointeuseDBConnectionString);
                    cBuilder = new OleDbCommandBuilder(dAdapter);
                    DataTable dataTable1 = new DataTable();
                    dAdapter.Fill(dataTable1);
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        ident = dataTable1.Rows[i][0].ToString();

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {
                conn.Close();

            }
            return ident;

        }

        public static string get_FirstPres()
        {
            string ident = "01/01/" + DateTime.Now.Year;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select top 1 Jour from Presence", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    dAdapter = new OleDbDataAdapter("select min(Jour) from Presence", Settings.Default.PointeuseDBConnectionString);
                    cBuilder = new OleDbCommandBuilder(dAdapter);
                    DataTable dataTable1 = new DataTable();
                    dAdapter.Fill(dataTable1);
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        ident = dataTable1.Rows[i][0].ToString();

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {
                conn.Close();

            }
            return ident;

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF Documents (*.pdf)|*.pdf";
            sfd.FileName = "liste_presence.pdf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                export_PDF(sfd.FileName); // Here dataGridview1 is your grid view name
            }

        }
        private void export_PDF(string path)
        {
            //Creating iTextSharp Table from the DataTable data
            PdfPTable pdfTable = new PdfPTable(pointageGrid.ColumnCount);
            pdfTable.DefaultCell.PaddingLeft = 5;
            pdfTable.DefaultCell.PaddingRight = 5;
            pdfTable.DefaultCell.PaddingTop = 8;
            pdfTable.DefaultCell.PaddingBottom = 8;
            pdfTable.WidthPercentage = 99;
            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable.DefaultCell.BorderWidth = 1;
            pdfTable.SetTotalWidth(new float[] {50f, 100f, 100f, 60f, 60f, 60f, 60f, 150f});
            //Adding Header row
            foreach (DataGridViewColumn column in pointageGrid.Columns)
            {
                
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                    cell.FixedHeight = (30);
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    pdfTable.AddCell(cell);
                
            }

            //Adding DataRow
            foreach (DataGridViewRow row in pointageGrid.Rows)
            {

                foreach (DataGridViewCell cell in row.Cells)
                {
                  
                        pdfTable.AddCell(cell.Value.ToString());
                }
            }
            
            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 20f, 40f);
                PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Resources.header, System.Drawing.Imaging.ImageFormat.Png);
                jpg.ScaleToFit(900f, 400f);
                jpg.SpacingBefore = 10f;
                jpg.SpacingAfter = 20f;
                jpg.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(jpg);
                var FontColour = new BaseColor(35, 31, 32);
                var Calibri8 = FontFactory.GetFont("Calibri", 20, FontColour);
                Paragraph paragraph = new Paragraph("Liste Des Présences du " + startdate.Value.ToShortDateString() + " Au " + enddate.Value.ToShortDateString() + " :", Calibri8);
                paragraph.Alignment = Element.ALIGN_LEFT;
                paragraph.SpacingAfter = 20f;
                pdfDoc.Add(paragraph);
                pdfDoc.Add(pdfTable);
                pdfDoc.Close();
                stream.Close();
            }
            if (File.Exists(path))
                System.Diagnostics.Process.Start(path);
            AddPageNumber(path, path);
        }
        void AddPageNumber(string fileIn, string fileOut)
        {
            byte[] bytes = File.ReadAllBytes(fileIn);
            iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString(), blackFont), 568f, 15f, 0);
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(DateTime.Now.ToString(), blackFont), 1165f, 15f, 0);

                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(fileOut, bytes);
        }

       
    }
}
