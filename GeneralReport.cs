﻿using AttLogs.Properties;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.OleDb;
using System.Collections;

namespace AttLogs
{
    public partial class GeneralReport : MetroForm
    {
        public GeneralReport()
        {
            InitializeComponent();
            get_Struct();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Program.log.Info("Display general repport");
                ReportDocument cryRpt = new AttLogs.CrystalReport1();
                cryRpt.DataSourceConnections[0].SetConnection("", Settings.Default.DBLocation, false);
                Tables myTables = cryRpt.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table myTable in myTables)
                {
                    TableLogOnInfo myTableLogonInfo = myTable.LogOnInfo;
                    myTableLogonInfo.ConnectionInfo.Password = Settings.Default.Passe;
                    myTable.ApplyLogOnInfo(myTableLogonInfo);
                }
                //string c = System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName;
                //cryRpt.Load(c + "/CrystalReport1.rpt");

                ParameterFieldDefinitions crParameterFieldDefinitions;
                ParameterFieldDefinition crParameterFieldDefinition;
                ParameterValues crParameterValues = new ParameterValues();
                ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();
                string emp = "";
                DictionaryEntry deImgType;
                if (structure.SelectedItem != null)
                {
                    deImgType = (DictionaryEntry)structure.SelectedItem;
                    emp = deImgType.Value.ToString();
                }
                crParameterDiscreteValue.Value = jour.Text;
                crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["orderdate"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crParameterDiscreteValue.Value = jour1.Text;
                crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["orderdate1"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);
                crParameterDiscreteValue.Value = emp;
                crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["Structure"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crystalReportViewer1.ReportSource = cryRpt;
                crystalReportViewer1.Refresh();
                crystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
                crystalReportViewer1.ShowGroupTreeButton = false;
                crystalReportViewer1.ShowParameterPanelButton = false;
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message.ToString());
                Program.log.Error("rapport generale," + exp.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           this.Close();
        }
        public void get_Struct()
        {

            this.structure.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib from Structures", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();

                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["lib"].ToString();
                    nom = dataTable.Rows[i]["lib"].ToString();
                    htPointInf.Add(ident, nom);

                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        structure.Items.Add(PointInf);
                    }
                    structure.DisplayMember = "value";
                    structure.ValueMember = "key";
                    structure.SelectedIndex = -1;
                    structure.AutoCompleteMode = AutoCompleteMode.Suggest;
                    structure.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_get_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }
    }
}
