﻿using System.Drawing;

namespace AttLogs
{
    partial class ListePointages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListePointages));
            this.pointageGrid = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.deleteP = new System.Windows.Forms.Button();
            this.editP = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.startdate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.enddate = new System.Windows.Forms.DateTimePicker();
            this.users = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SearchRetard = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Tretard = new System.Windows.Forms.DateTimePicker();
            this.Tavant = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.SearchAvant = new System.Windows.Forms.Button();
            this.Export = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.structure = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.toPdf = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.date_ret2 = new System.Windows.Forms.DateTimePicker();
            this.date_ret1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.pointageGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pointageGrid
            // 
            this.pointageGrid.AllowUserToAddRows = false;
            this.pointageGrid.AllowUserToDeleteRows = false;
            this.pointageGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pointageGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.pointageGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.pointageGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.pointageGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.pointageGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pointageGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.pointageGrid.Location = new System.Drawing.Point(11, 185);
            this.pointageGrid.MultiSelect = false;
            this.pointageGrid.Name = "pointageGrid";
            this.pointageGrid.Size = new System.Drawing.Size(1261, 346);
            this.pointageGrid.TabIndex = 0;
            this.pointageGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.pointageGrid_CellBeginEdit);
            this.pointageGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.pointageGrid_CellEndEdit);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1173, 536);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 31);
            this.button1.TabIndex = 2;
            this.button1.Text = "Fermer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // deleteP
            // 
            this.deleteP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteP.Location = new System.Drawing.Point(894, 113);
            this.deleteP.Name = "deleteP";
            this.deleteP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.deleteP.Size = new System.Drawing.Size(165, 34);
            this.deleteP.TabIndex = 8;
            this.deleteP.Text = "Supprimer un pointage";
            this.deleteP.UseVisualStyleBackColor = true;
            this.deleteP.Visible = false;
            this.deleteP.Click += new System.EventHandler(this.deleteP_Click);
            // 
            // editP
            // 
            this.editP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.editP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editP.Location = new System.Drawing.Point(1107, 116);
            this.editP.Name = "editP";
            this.editP.Size = new System.Drawing.Size(165, 34);
            this.editP.TabIndex = 9;
            this.editP.Text = "Aj Observation";
            this.editP.UseMnemonic = false;
            this.editP.UseVisualStyleBackColor = true;
            this.editP.Click += new System.EventHandler(this.editP_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(933, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(140, 30);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Chercher";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(616, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Du";
            // 
            // startdate
            // 
            this.startdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startdate.Location = new System.Drawing.Point(647, 9);
            this.startdate.Name = "startdate";
            this.startdate.Size = new System.Drawing.Size(106, 20);
            this.startdate.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(763, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "Au";
            // 
            // enddate
            // 
            this.enddate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.enddate.Location = new System.Drawing.Point(796, 8);
            this.enddate.Name = "enddate";
            this.enddate.Size = new System.Drawing.Size(102, 20);
            this.enddate.TabIndex = 14;
            // 
            // users
            // 
            this.users.FormattingEnabled = true;
            this.users.Location = new System.Drawing.Point(88, 73);
            this.users.Name = "users";
            this.users.Size = new System.Drawing.Size(175, 21);
            this.users.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Employé:";
            // 
            // SearchRetard
            // 
            this.SearchRetard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SearchRetard.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchRetard.Location = new System.Drawing.Point(292, 6);
            this.SearchRetard.Name = "SearchRetard";
            this.SearchRetard.Size = new System.Drawing.Size(102, 46);
            this.SearchRetard.TabIndex = 17;
            this.SearchRetard.Text = "Chercher";
            this.SearchRetard.UseVisualStyleBackColor = true;
            this.SearchRetard.Click += new System.EventHandler(this.SearchRetard_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 14);
            this.label5.TabIndex = 18;
            this.label5.Text = "Arrivées après";
            // 
            // Tretard
            // 
            this.Tretard.CustomFormat = "HH:mm:ss";
            this.Tretard.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Tretard.Location = new System.Drawing.Point(120, 8);
            this.Tretard.Name = "Tretard";
            this.Tretard.ShowUpDown = true;
            this.Tretard.Size = new System.Drawing.Size(119, 20);
            this.Tretard.TabIndex = 19;
            // 
            // Tavant
            // 
            this.Tavant.CustomFormat = "HH:mm:ss";
            this.Tavant.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Tavant.Location = new System.Drawing.Point(112, 8);
            this.Tavant.Name = "Tavant";
            this.Tavant.ShowUpDown = true;
            this.Tavant.Size = new System.Drawing.Size(121, 20);
            this.Tavant.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Sorties avant";
            // 
            // SearchAvant
            // 
            this.SearchAvant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SearchAvant.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchAvant.Location = new System.Drawing.Point(287, 6);
            this.SearchAvant.Name = "SearchAvant";
            this.SearchAvant.Size = new System.Drawing.Size(102, 46);
            this.SearchAvant.TabIndex = 20;
            this.SearchAvant.Text = "Chercher";
            this.SearchAvant.UseVisualStyleBackColor = true;
            this.SearchAvant.Click += new System.EventHandler(this.SearchAvant_Click);
            // 
            // Export
            // 
            this.Export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Export.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Export.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Export.ForeColor = System.Drawing.Color.MediumAquamarine;
            this.Export.Location = new System.Drawing.Point(1107, 73);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(165, 34);
            this.Export.TabIndex = 23;
            this.Export.Text = "Exporter la liste en XLS";
            this.Export.UseVisualStyleBackColor = true;
            this.Export.Click += new System.EventHandler(this.Export_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(280, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 14);
            this.label1.TabIndex = 24;
            this.label1.Text = "Structures:";
            // 
            // structure
            // 
            this.structure.FormattingEnabled = true;
            this.structure.Location = new System.Drawing.Point(358, 72);
            this.structure.Name = "structure";
            this.structure.Size = new System.Drawing.Size(249, 21);
            this.structure.TabIndex = 25;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.enddate);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.startdate);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(11, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 39);
            this.panel1.TabIndex = 26;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.date_ret1);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.SearchRetard);
            this.panel2.Controls.Add(this.Tretard);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(11, 112);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(407, 67);
            this.panel2.TabIndex = 27;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.date_ret2);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.Tavant);
            this.panel3.Controls.Add(this.SearchAvant);
            this.panel3.Location = new System.Drawing.Point(445, 112);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(407, 67);
            this.panel3.TabIndex = 28;
            // 
            // toPdf
            // 
            this.toPdf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.toPdf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toPdf.ForeColor = System.Drawing.Color.Red;
            this.toPdf.Location = new System.Drawing.Point(1107, 29);
            this.toPdf.Name = "toPdf";
            this.toPdf.Size = new System.Drawing.Size(165, 34);
            this.toPdf.TabIndex = 29;
            this.toPdf.Text = "Exporter la liste en PDF";
            this.toPdf.UseVisualStyleBackColor = true;
            this.toPdf.Click += new System.EventHandler(this.toPdf_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 14);
            this.label7.TabIndex = 20;
            this.label7.Text = "Jour:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(16, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 14);
            this.label8.TabIndex = 23;
            this.label8.Text = "Jour:";
            // 
            // date_ret2
            // 
            this.date_ret2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_ret2.Location = new System.Drawing.Point(112, 36);
            this.date_ret2.Name = "date_ret2";
            this.date_ret2.Size = new System.Drawing.Size(121, 20);
            this.date_ret2.TabIndex = 24;
            // 
            // date_ret1
            // 
            this.date_ret1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_ret1.Location = new System.Drawing.Point(120, 36);
            this.date_ret1.Name = "date_ret1";
            this.date_ret1.Size = new System.Drawing.Size(119, 20);
            this.date_ret1.TabIndex = 21;
            // 
            // ListePointages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 572);
            this.Controls.Add(this.toPdf);
            this.Controls.Add(this.structure);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Export);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.users);
            this.Controls.Add(this.editP);
            this.Controls.Add(this.deleteP);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pointageGrid);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ListePointages";
            this.Text = "Liste des Pointages";
            ((System.ComponentModel.ISupportInitialize)(this.pointageGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView pointageGrid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button deleteP;
        private System.Windows.Forms.Button editP;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker startdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker enddate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button SearchRetard;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox users;
        private System.Windows.Forms.DateTimePicker Tretard;
        private System.Windows.Forms.DateTimePicker Tavant;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button SearchAvant;
        private System.Windows.Forms.Button Export;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox structure;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button toPdf;
        private System.Windows.Forms.DateTimePicker date_ret1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker date_ret2;
        private System.Windows.Forms.Label label8;
    }
}