﻿using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace AttLogs
{
    public partial class Utilisateurs : MetroForm
    {
        public Utilisateurs()
        {
            InitializeComponent();
            Cursor = Cursors.WaitCursor;
            Display_Users();
            Cursor = Cursors.Default;
           
        }
        public static void Display_Users()
        {
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataSet ds = new DataSet();
            //Cursor = Cursors.WaitCursor;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Users.ID as ID,UserName as Login,Pwd as MotDePasse,Nom as Profile from Users inner join Profile on Users.IdProfile=Profile.ID order by Users.ID", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                UsersGrid.DataSource = dataTable;
                UsersGrid.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                Program.log.Error("Utilisateur_Display_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
                
            }
        }

        private void close_Click(object sender, EventArgs e)
        {
           this.Close();
        }

        private void AddUser_Click(object sender, EventArgs e)
        {
            var myForm = new AddUtilisateur();
            myForm.Show();
        }

        private void DeleteUser_Click(object sender, EventArgs e)
        {
            if (UsersGrid.RowCount > 0)
            {
                DialogResult dialogResult = MessageBox.Show("voulez-vous Supprimer cet utilsateur?", "Confirmation", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    foreach (DataGridViewRow item in UsersGrid.SelectedRows)
                    {

                        string id = UsersGrid.CurrentRow.Cells[0].Value.ToString();

                        OleDbCommand oleDbCmd = new OleDbCommand();
                        System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                        conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                        DataSet ds = new DataSet();
                        Cursor = Cursors.WaitCursor;
                        conn.Open();
                        OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                        string sql = null;
                        sql = "delete * from Users where ID =" + id + "";
                        try
                        {
                            oledbAdapter.DeleteCommand = conn.CreateCommand();
                            oledbAdapter.DeleteCommand.CommandText = sql;
                            oledbAdapter.DeleteCommand.ExecuteNonQuery();
                            MessageBox.Show("L'utilsateur a été supprimé", "Succès");
                            UsersGrid.Rows.RemoveAt(item.Index);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                            Program.log.Error("Utilisateur_Del_Users" + ex.Message.ToString());
                        }
                        finally
                        {
                            conn.Close();
                            Cursor = Cursors.Default;
                        }
                    }
                }
            }
        }

        private void EditUser_Click(object sender, EventArgs e)
        {
            if (UsersGrid.RowCount > 0)
            {
                if (UsersGrid.CurrentCell.ColumnIndex == 3)
                {
                    MessageBox.Show("vous ne pouvez pas modifier cette valeur!", "alert");
                }
                else
                {
                   
                    UsersGrid.CurrentCell.ReadOnly = false;
                    UsersGrid.BeginEdit(true);
                }
                
            }
        }
        private void UsersGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            UsersGrid.Tag = UsersGrid.CurrentCell.Value;
            //if (UsersGrid.CurrentCell.ColumnIndex == 3)
            //{
            //    List<Profile> profils = new List<Profile>();
            //    profils.Add(new Profile { Name = "Super Admin", ID = 0 });
            //    profils.Add(new Profile { ID = 1, Name = "Admin" });
            //    profils.Add(new Profile { ID = 2, Name = "Utilisateur" });
            //    DataGridViewComboBoxCell c = new DataGridViewComboBoxCell();

            //    c.DataSource = profils;
            //    c.Value = 1;
            //    c.ValueMember = "ID";
            //    c.DisplayMember = "Name";
            //    UsersGrid[3,UsersGrid.CurrentRow.Index] = new DataGridViewComboBoxCell();
            //    UsersGrid[3, UsersGrid.CurrentRow.Index]=c;
            //}
        }
        public class Profile
        {
            public string Name { get;  set; }
            public int ID { get; set; }

            //public Profile(string name, int id)
            //{
            //    Name = name;
            //    ID = id;
            //}
        }
        private void UsersGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var oldValue = UsersGrid.Tag;
            bool change = true;
            string newValue = UsersGrid.CurrentCell.Value.ToString();
            if ((oldValue.ToString() != newValue) && (newValue != ""))
            {
                    int columnIndex = UsersGrid.CurrentCell.ColumnIndex;
                    string columnName = UsersGrid.Columns[columnIndex].Name;
                    DialogResult dialogResult = MessageBox.Show("vous avez changé la valeur de '" + columnName + "',voulez-vous l'enregistrer?", "Confirmation", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.OK)
                    {
                        OleDbCommand oleDbCmd = new OleDbCommand();
                        System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                        conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                    DataTable dataTable = new DataTable();
                    OleDbDataAdapter adapter;
                        OleDbCommandBuilder cmdBuilder;
                        if (UsersGrid.CurrentCell.ColumnIndex == 1)
                        {
                            try
                            {
                                conn.Open();
                                adapter = new OleDbDataAdapter("SELECT *  FROM Users", conn);
                                cmdBuilder = new OleDbCommandBuilder(adapter);
                                adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                                adapter.Fill(dataTable);
                                for (int i = 0; i < dataTable.Rows.Count; i++)
                                {
                                    if (dataTable.Rows[i]["UserName"].ToString() == newValue)
                                    {
                                        MessageBox.Show("Vous avez déja déclaré un utilisateur avec le méme login !", "Alert");
                                        UsersGrid.CurrentCell.Value = oldValue;
                                        change = false;
                                        i = dataTable.Rows.Count;
                                    }
                                }

                            }
                            catch (Exception exp)
                            {
                                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                                Program.log.Error("Utilisateur_CellEndEdit" + exp.Message.ToString());
                             }

                            if (conn.State == ConnectionState.Open)
                                conn.Close();

                        }
                        if (change == true)
                        {
                            string id = UsersGrid.CurrentRow.Cells[0].Value.ToString();
                            string colName = "";
                            if (columnIndex == 1)
                                colName = "UserName";
                            if (columnIndex == 2)
                                colName = "Pwd";
                            if (columnIndex == 3)
                                colName = "IdProfile";
                            conn.Open();
                            adapter = new OleDbDataAdapter();
                            string sql = "update Users set " + colName + " = '" + newValue + "' where ID =" + id + "";
                            try
                            {
                                adapter.DeleteCommand = conn.CreateCommand();
                                adapter.DeleteCommand.CommandText = sql;
                                adapter.DeleteCommand.ExecuteNonQuery();
                                MessageBox.Show("La valeur a été modifiée", "Succès");


                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.ToString());
                                Program.log.Error("Utilisateur_update" + ex.Message.ToString());
                        }
                            finally
                            {
                                conn.Close();
                                Cursor = Cursors.Default;
                            }
                        }
                    }
                    else
                    {
                        UsersGrid.CurrentCell.Value = UsersGrid.Tag;
                    }
                }
             
            else
            {
                UsersGrid.CurrentCell.Value = UsersGrid.Tag;
            }

         }
        /*  public void get_Profile()
 {

     profile.Items.Clear();
     OleDbCommand oleDbCmd = new OleDbCommand();
     System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
     conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

     try
     {
         conn.Open();
         OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,Nom from Profile", Settings.Default.PointeuseDBConnectionString);
         OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
         DataTable dataTable = new DataTable();
         dAdapter.Fill(dataTable);
         string ident, nom;
         Hashtable htPointInf = new Hashtable();
         int i;
         for (i = 0; i < dataTable.Rows.Count; i++)
         {
             ident = dataTable.Rows[i]["ID"].ToString();
             nom = dataTable.Rows[i]["Nom"].ToString();
             htPointInf.Add(nom, ident);
         }
         if (i > 0)
         {

             foreach (DictionaryEntry PointInf in htPointInf)
             {
                 profile.Items.Add(PointInf);
             }
             profile.DisplayMember = "key";
             profile.ValueMember = "value";
             profile.AutoCompleteMode = AutoCompleteMode.Suggest;
             profile.AutoCompleteSource = AutoCompleteSource.ListItems;

         }
     }
     catch (Exception ex)
     {
         MessageBox.Show("Failed to connect to data source");
     }
     finally
     {

         conn.Close();
     }
 }*/

    }

}
