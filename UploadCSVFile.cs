﻿using AttLogs.Properties;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class UploadCSVFile : MetroForm
    {
        bool duplicate = false;
        public UploadCSVFile()
        {
            InitializeComponent();
            
        }
        private void btBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            // To list only csv files, we need to add this filter
            openFileDialog.Filter = "|*.csv";
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                tbCSVPath.Text = openFileDialog.FileName;
            }
        }

        private void btUpload_Click(object sender, EventArgs e)
        {
            try
            {
                dgCSVData.DataSource = GetDataTableFromCSVFile(tbCSVPath.Text);
                duplicate=HighlightDuplicate(dgCSVData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Import CSV File", MessageBoxButtons.OK,MessageBoxIcon.Error);
                Program.log.Error("Import CSV File" + ex.Message.ToString());
            }
        }
        private static DataTable GetDataTableFromCSVFile(string csvfilePath)
        {
            DataTable csvData = new DataTable();
            using (TextFieldParser csvReader = new TextFieldParser(csvfilePath))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;

                //Read columns from CSV file, remove this line if columns not exits  
                string[] colFields = csvReader.ReadFields();
                bool id = false ,name = false, prenom = false,LibSP=false,libemp=false;
                for (int i=0; i< colFields.Length;i++)
                {
                    if (colFields[i] == "Matricule")
                        id = true;
                    else
                        if (colFields[i] == "Nom")
                        name = true;
                    else
                        if (colFields[i] == "Prenom")
                             prenom = true;
                    else
                        if (colFields[i] == "Lib SP")
                        LibSP = true;
                    else
                        if (colFields[i] == "Lib emploi")
                        libemp = true;
                }

                if(id==false)
                {
                    MessageBox.Show("veuillez vérifier votre fichier CSV,il manque la colonne 'Matricule'", "Error");
                    
                }
                else
                if(name == false )
                {
                    MessageBox.Show( "veuillez vérifier votre fichier CSV,il manque la colonne 'Nom'", "Error");

                }
                else
                if(prenom == false)
                {
                    MessageBox.Show("veuillez vérifier votre fichier CSV,il manque la colonne 'Prenom'", "Error");

                }
                else
                if (LibSP == false)
                {
                    MessageBox.Show( "veuillez vérifier votre fichier CSV,il manque la colonne 'Lib SP'", "Error");

                }
                else
                if (libemp == false)
                {
                    MessageBox.Show("veuillez vérifier votre fichier CSV,il manque la colonne 'Lib emploie'", "Error");

                }
                else
                {
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
                
            }
            return csvData;
        }

        private void CancelUpList_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UploadList_Click(object sender, EventArgs e)
        {
            
            
            if (dgCSVData.Rows.Count > 0)
            {
                if (duplicate == false)
                {
                    string Password = null;
                    int privileg = 0;
                    int idwErrorCode = 0;
                    bool actif = true;
                    Cursor = Cursors.WaitCursor;
                    //verifier les données du fichier csv:ID=integer

                    if (AttLogsMain.IsConnected.Count == 0)
                    {
                        MessageBox.Show("l'application ne contient aucune pointeuse,veuillez ajouter au moins une pointeuse!", "Error");
                        Cursor = Cursors.Default;
                        return;
                    }
                    if (AttLogsMain.inedxP == -1)
                    {
                        MessageBox.Show("veuillez indiquer la pointeuse principale dans la liste des pointeuses!", "Error");
                        Cursor = Cursors.Default;
                        return;
                    }

                    if (AttLogsMain.IsConnected[AttLogsMain.inedxP] == false)
                    {
                        MessageBox.Show("la pointeuse principale n'est pas Connectées!", "Error");
                        Cursor = Cursors.Default;
                        return;
                    }

                    int size =AttLogsMain.LastId(AttLogsMain.axCZKEM[AttLogsMain.inedxP]);

                    OleDbCommand oleDbCmd = new OleDbCommand();
                    System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                    conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                    conn.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM Employer", conn);
                    OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);

                    bool check = check_matricule(dataTable, dgCSVData);
                    if (check == false)
                    {
                        for (int rows = 0; rows < dgCSVData.Rows.Count; rows++)
                        {
                            string name = dgCSVData.Rows[rows].Cells["Nom"].Value.ToString();
                            string prenom = dgCSVData.Rows[rows].Cells["Prenom"].Value.ToString();
                            string matricule = dgCSVData.Rows[rows].Cells["Matricule"].Value.ToString();
                            string username = name + " " + prenom;
                            string id = size.ToString();
                            string structur = dgCSVData.Rows[rows].Cells["Lib SP"].Value.ToString();
                            string emp = dgCSVData.Rows[rows].Cells["Lib emploi"].Value.ToString();

                            try
                            {
                                AttLogsMain.axCZKEM[AttLogsMain.inedxP].EnableDevice(AttLogsMain.iMachineNumber, false);

                                if (AttLogsMain.axCZKEM[AttLogsMain.inedxP].SSR_SetUserInfo(AttLogsMain.iMachineNumber, id, username, Password, privileg, actif))//upload user information to the device
                                {
                                    //MessageBox.Show("Utilisateur crée avec succes");
                                    DataRow row;
                                    row = dataTable.NewRow();
                                    row["Identifiant"] = id;
                                    row["Matricule"] = matricule;
                                    row["Nom"] = name;
                                    row["Prenom"] = prenom;
                                    row["LibSP"] = structur;
                                    row["LibEmp"] = emp;
                                    row["IsActif"] = "1";
                                    row["Pointeuse"] = AttLogsMain.CheckBoxes[AttLogsMain.inedxP].Text;
                                    dataTable.Rows.Add(row);
                                    adapter.Update(dataTable);
                                    dataTable.AcceptChanges();

                                    size++;
                            }
                                else
                                {
                                AttLogsMain.axCZKEM[AttLogsMain.inedxP].GetLastError(ref idwErrorCode);
                                MessageBox.Show("Echec,ErrorCode=" + idwErrorCode.ToString(), "Error");
                                AttLogsMain.axCZKEM[AttLogsMain.inedxP].EnableDevice(AttLogsMain.iMachineNumber, true);
                                Cursor = Cursors.Default;
                                return;
                            }

                        }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                                Program.log.Error("Import CSV File_UploadList" + ex.Message.ToString());

                            }
                            finally
                            {
                                conn.Close();

                            }

                        }

                        MessageBox.Show("Tous les employés ont été crées avec succès dans la pointeuse:" + AttLogsMain.CheckBoxes[AttLogsMain.inedxP].Text, "Succes");
                        AttLogsMain.axCZKEM[AttLogsMain.inedxP].RefreshData(AttLogsMain.iMachineNumber);//the data in the device should be refreshed
                        AttLogsMain.axCZKEM[AttLogsMain.inedxP].EnableDevice(AttLogsMain.iMachineNumber, true);

                        Cursor = Cursors.Default;
                        this.Close();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                    }
                }
                else
                {
                    MessageBox.Show("veuillez corriger les doublons dans votre fichier CSV,et réinsérer le fichier", "Alert");
                }
            }
            else
            {
                    MessageBox.Show("vous n'avez aucune donnée à inserer", "Alert");

            }
        }

        public bool  check_matricule(DataTable dataTable, DataGridView grv)
        {
            bool find = false;
            string matricule = null;
            for (int rows = 0; rows < dgCSVData.Rows.Count; rows++)
            {
                matricule= dgCSVData.Rows[rows].Cells["Matricule"].Value.ToString(); 
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    string mat = dataTable.Rows[i]["Matricule"].ToString();
                    if (mat == matricule)
                    {
                        MessageBox.Show("un employé avec le même matricule" + "'" + matricule + "' existe dèja,veuillez vérifiez votre fichier CSV", "Error");
                        find = true;
                        break;
                    }
                }
                if (find == true)
                    break;
            }
            return find;
        }
        public bool HighlightDuplicate(DataGridView grv)
        {
            bool find = false;
            var rows = grv.Rows.OfType<DataGridViewRow>().Reverse().Skip(0);
            var dupRos = rows.GroupBy(r => r.Cells["Matricule"].Value.ToString())
            .Where(g => g.Count() > 1)
            .SelectMany(r => r.ToList());
            foreach (var r in dupRos)
            {
                r.DefaultCellStyle.BackColor = Color.Red;
                find = true;
            }
            foreach (var r in rows.Except(dupRos))
                r.DefaultCellStyle.BackColor = Color.White;
            
            return find;
        }
    }
}
