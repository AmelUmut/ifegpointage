﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace AttLogs
{
    public partial class ChangePwd : MetroForm
    {
        public ChangePwd()
        {
            InitializeComponent();
        }

        private void savePwd_Click(object sender, EventArgs e)
        {
            string old = oldPwd.Text;
            string newp = newPwd.Text;
            string conf = confPwd.Text;
            bool change = true;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            //DataSet ds = new PointeuseDBDataSet1();
            DataTable dataTable = new DataTable();
            OleDbDataAdapter adapter;
            OleDbCommandBuilder cmdBuilder;
            try
            {
                conn.Open();
                adapter = new OleDbDataAdapter("SELECT *  FROM Users where ID="+Login.idUser, conn);
                cmdBuilder = new OleDbCommandBuilder(adapter);
                adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                adapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    change = false;
                else
                {


                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i]["Pwd"].ToString() != old)
                        {
                            change = false;
                            i = dataTable.Rows.Count;

                        }
                    }
                }
                if(change==false)
                    MessageBox.Show("La mot de passe actuelle est incorrect", "Alert");
                else
                {
                    if(newp!=conf)
                        MessageBox.Show("La mot de passe de confirmation est incorrect", "Alert");
                    else
                    {
                        adapter = new OleDbDataAdapter();
                        string sql = "update Users set Pwd = '" + newp + "' where ID =" + Login.idUser + "";
                        try
                        {
                            adapter.DeleteCommand = conn.CreateCommand();
                            adapter.DeleteCommand.CommandText = sql;
                            adapter.DeleteCommand.ExecuteNonQuery();
                            MessageBox.Show("Le mot de passe a été modifiée", "Succès");
                            this.Close();
                            Application.Restart();

                            Program.log.Info("Change pwd,user id" + Login.idUser);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                            Program.log.Error("Change pwd," + ex.ToString());
                        }
                       

                    }
                }

            }
            catch (Exception exp)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                Program.log.Error("Change pwd," + exp.ToString());
            }

            if (conn.State == ConnectionState.Open)
                conn.Close();
        }

        private void cancelPwd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
