﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class ListePForm : MetroForm
    {
         bool change = false;
        
        public ListePForm()
        {
            InitializeComponent();
            if(Login.idPro=="3" || Login.idPro=="4")
            {
                editP.Enabled = false;
                deleteP.Enabled = false;
                saveP.Enabled = false;
            }
             DisplayData();
            foreach (DataGridViewRow row in pointGridView.Rows)
            {
                row.ReadOnly = true;
            }
        }

        private void DisplayData()
        {
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataSet ds = new DataSet();
            Cursor = Cursors.WaitCursor;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,Nom,IP,Port,IsPrincipal as Principal from Pointeuse", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                pointGridView.DataSource = dataTable;
                pointGridView.Columns[0].Visible = false;
                pointGridView.Height = dgvHeight();
                Program.log.Info("Display pointeuse list");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                Program.log.Error("Display pointeuse list"+ex.Message.ToString());
            }
            finally
            {

                conn.Close();
                Cursor = Cursors.Default;
            }
        }

        private void saveP_Click(object sender, EventArgs e)
        {
            if (pointGridView.Rows.Count > 0)
            {
                bool prin = false;
                for (int i = 0; i < pointGridView.RowCount; i++)
                {
                    if (pointGridView.Rows[i].Cells[4].Value.ToString() == "1")
                    
                        prin = true;
                    
                }
                if (prin == false)
                {
                    MessageBox.Show("Veuillez indiquer la pointeuse principale!","Alert");
                }
                else
                {
                    OleDbCommand oleDbCmd = new OleDbCommand();
                    System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                    conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                    DataSet ds = new DataSet();
                    Cursor = Cursors.WaitCursor;
                    try
                    {
                        conn.Open();
                        OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                        string sql = null;
                        sql = "delete * from Pointeuse";
                        try
                        {
                            oledbAdapter.DeleteCommand = conn.CreateCommand();
                            oledbAdapter.DeleteCommand.CommandText = sql;
                            oledbAdapter.DeleteCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            Program.log.Error("pointeuse list" + ex.Message.ToString());
                        }
                        try
                        {
                            OleDbDataAdapter dAdapter = new OleDbDataAdapter("SELECT * FROM Pointeuse", conn);

                            OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(dAdapter);
                            dAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                            DataRow row;
                            dAdapter.Fill(ds, "Pointeuse");
                            foreach (DataGridViewRow rows in pointGridView.Rows)
                            {
                                row = ds.Tables["Pointeuse"].NewRow();
                                row["ID"] = rows.Cells[0].Value;
                                row["IP"] = rows.Cells[2].Value;
                                row["Nom"] = rows.Cells[1].Value;
                                row["Port"] = rows.Cells[3].Value;
                                row["IsPrincipal"] = rows.Cells[4].Value;
                                ds.Tables["Pointeuse"].Rows.Add(row);
                            }
                            dAdapter.Update(ds, "Pointeuse");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                            Program.log.Error("pointeuse list" + ex.Message.ToString());
                        }

                        MessageBox.Show("les valeurs sont enregistrées,l'application va redémarrer pour appliquer les changement", "succès");
                        
                        this.Close();
                        Application.Restart();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                        Program.log.Error("pointeuse list"+ex.Message.ToString());
                    }
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                    
                }
            }
           // AttLogsMain.get_pointeuse();
            
            Cursor = Cursors.Default;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (change == true)
            {
                DialogResult dialogResult = MessageBox.Show("voulez vous quitter sans enregistrer?", "Confirmation", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    this.Close();
                }
            }
            else

                this.Close();
        }
        private int dgvHeight()
        {
            int sum = pointGridView.ColumnHeadersHeight;

            foreach (DataGridViewRow row in pointGridView.Rows)
                sum += row.Height + 1; // I dont think the height property includes the cell border size, so + 1

            return sum;
        }

        private void deleteP_Click(object sender, EventArgs e)
        {
            if (pointGridView.Rows.Count > 0)
            {
                foreach (DataGridViewRow item in pointGridView.SelectedRows)
                {
                    pointGridView.Rows.RemoveAt(item.Index);
                }
                pointGridView.Height = dgvHeight();
                change = true;
            }
        }

        private void editP_Click(object sender, EventArgs e)
        {
            if (pointGridView.Rows.Count > 0)
            {
                pointGridView.CurrentRow.ReadOnly = false;

                pointGridView.BeginEdit(true);
            }
            //pointGridView.EditMode = DataGridViewEditMode.EditOnEnter;
        }

        private void pointGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var oldValue = pointGridView.Tag;
            System.Net.IPAddress b;
            int c;
            string newValue = pointGridView.CurrentCell.Value.ToString();
            if ((oldValue.ToString() != newValue) && (newValue != ""))
            {
                int columnIndex = pointGridView.CurrentCell.ColumnIndex;
                string columnName = pointGridView.Columns[columnIndex].Name;
                
                    if (pointGridView.CurrentCell.ColumnIndex == 2)
                    {
                        if (System.Net.IPAddress.TryParse(newValue, out b) == false)
                        {
                            MessageBox.Show("Format de donnée inccorcet,veuillez respecter le format d'adresse IP", "Erreur");
                            pointGridView.CurrentCell.Value = oldValue;
                            
                        }
                    }
                    else
                        if(pointGridView.CurrentCell.ColumnIndex == 3)
                    {
                        if (Int32.TryParse(newValue, out c) == false)
                        {
                            MessageBox.Show("Format de donnée inccorcet,veuillez vérifier la valeur du Port", "Erreur");
                            pointGridView.CurrentCell.Value = oldValue;
                            
                        }
                    }
                    else
                        if (pointGridView.CurrentCell.ColumnIndex == 4)
                    {
                        if (Int32.TryParse(newValue, out c) == false)
                        {
                            MessageBox.Show("Format de donnée inccorcet,veuillez insérez 0 ou 1", "Erreur");
                            pointGridView.CurrentCell.Value = oldValue;
                          
                        }
                        else
                            if(c!=0 && c!=1)
                            {
                            MessageBox.Show("Format de donnée inccorcet,veuillez insérez 0 ou 1", "Erreur");
                            pointGridView.CurrentCell.Value = oldValue;
                            }
                    }
                        string nomp = pointGridView.CurrentRow.Cells[1].Value.ToString().Replace("'", "''");
                        string ip = pointGridView.CurrentRow.Cells[2].Value.ToString();
                        string port= pointGridView.CurrentRow.Cells[3].Value.ToString();
                        string prin = pointGridView.CurrentRow.Cells[4].Value.ToString();
                        change = true;
                        for (int i=0;i< pointGridView.RowCount;i++)
                         {
                            if (pointGridView.Rows[i].Cells[1].Value.ToString() == nomp )
                            {
                                if (i != pointGridView.CurrentRow.Index)
                                {
                                    MessageBox.Show("Vous avez deux pointeuses avec le même Nom!!", "Erreur");
                                    pointGridView.CurrentCell.Value = oldValue;
                                    i = pointGridView.RowCount;
                                    change = false;

                                }

                            }
                            else
                            if (pointGridView.Rows[i].Cells[2].Value.ToString()==ip && pointGridView.Rows[i].Cells[3].Value.ToString() == port)
                              {
                                        if(i!= pointGridView.CurrentRow.Index)
                                        {
                                            MessageBox.Show("Vous avez deux pointeuses avec les mêmes paramétres!!", "Erreur");
                                            pointGridView.CurrentCell.Value = oldValue;
                                            i = pointGridView.RowCount;
                                            change = false;

                                        }
                             }
                            else
                            if (pointGridView.Rows[i].Cells[4].Value.ToString() == prin && prin== "1")
                            {
                                if (i != pointGridView.CurrentRow.Index)
                                {
                                    DialogResult dialogResult = MessageBox.Show("Vous avez déja une pointeuse principale,si vous la modifiez vous risquer de perdre des données,voulez-vous continuer?", "Confirmation", MessageBoxButtons.OKCancel);
                                    if (dialogResult == DialogResult.OK)
                                    {
                                        pointGridView.Rows[i].Cells[4].Value = 0;
                                        pointGridView.CurrentCell.Value = prin;

                                    }
                                    else
                                    {
                                        pointGridView.CurrentCell.Value = oldValue;
                                        change = false;
                                    }
                                            i = pointGridView.RowCount;
                                  }
                            }

                         }
                }
                else
                {
                    pointGridView.CurrentCell.Value = oldValue;
                }
           

          }

        private void pointGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            pointGridView.Tag = pointGridView.CurrentCell.Value;
        }

       
    }
}

