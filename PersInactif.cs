﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Windows.Forms;
using MetroFramework.Forms;
using AttLogs.Properties;

namespace AttLogs
{
    public partial class PersInactif : MetroForm
    {
        public PersInactif()
        {
            InitializeComponent();
            DisplayData();
            foreach (DataGridViewRow row in pointGridView.Rows)
            {
                row.ReadOnly = true;
            }
        }

        private void DisplayData()
        {
            Cursor.Current = Cursors.WaitCursor;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Identifiant,Matricule,Nom,Prenom,LibSP as Structure,LibEmp as Emploi from Employer where IsActif='0' order by LibSP", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                DataTable dataTable1 = new DataTable();
                dAdapter.Fill(dataTable);
                pointGridView.DataSource = dataTable;
                dAdapter = new OleDbDataAdapter("select lib from Structures order by lib", Settings.Default.PointeuseDBConnectionString);
                cBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.Fill(dataTable1);
                IList<string> lstFirst = new List<string>();
                foreach (DataRow row in dataTable1.Rows)
                {
                    lstFirst.Add(row.Field<string>("lib"));

                }
                this.listep.Items.AddRange(lstFirst.ToArray<string>());
                this.listep.AutoCompleteMode = AutoCompleteMode.Suggest;
                this.listep.AutoCompleteSource = AutoCompleteSource.ListItems;


            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("UsersList" + ex.Message.ToString());

            }
            finally
            {

                conn.Close();
                Cursor = Cursors.Default;

            }
        }
        private int dgvHeight()
        {
            int sum = this.pointGridView.ColumnHeadersHeight;

            foreach (DataGridViewRow row in this.pointGridView.Rows)
                sum += row.Height + 1; // I dont think the height property includes the cell border size, so + 1

            return sum;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchP_Click(object sender, EventArgs e)
        {
            string ident = "";
            if (listep.SelectedItem != null)
            {
                ident = listep.SelectedItem.ToString();
            }
            BindingSource bs = new BindingSource();
            bs.DataSource = pointGridView.DataSource;
            bs.Filter = pointGridView.Columns[4].HeaderText.ToString() + " LIKE '%" + ident + "%'";
            pointGridView.DataSource = bs;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pointGridView.RowCount > 0)
            {
                bool del = false;
                foreach (DataGridViewRow item in this.pointGridView.SelectedRows)
                {
                    del = true;
                    DialogResult dialogResult = MessageBox.Show("voulez-vous Activer cet employé?", "Confirmation", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.OK)
                    {
                        string id = pointGridView.CurrentRow.Cells[0].Value.ToString();

                        OleDbCommand oleDbCmd = new OleDbCommand();
                        System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                        conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                        DataSet ds = new DataSet();
                        Cursor = Cursors.WaitCursor;
                        conn.Open();
                        OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                        string sql = null;
                        sql = "update Employer set IsActif = '1' where Identifiant = '" + id + "'";
                        try
                        {
                            oledbAdapter.DeleteCommand = conn.CreateCommand();
                            oledbAdapter.DeleteCommand.CommandText = sql;
                            oledbAdapter.DeleteCommand.ExecuteNonQuery();
                            MessageBox.Show("L'employé a été activé", "Succès");
                            pointGridView.Rows.RemoveAt(item.Index);
                            UsersList.DisplayData();

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        finally
                        {
                            conn.Close();
                            Cursor = Cursors.Default;
                        }
                    }
                }
                if (del == false)
                    MessageBox.Show("Veuillez sélectionner toute la ligne de l'employé à supprimer", "Alert");
            }
        }
    }
}
