﻿using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace AttLogs
{
    public partial class AddUtilisateur : MetroForm
    {
        public AddUtilisateur()
        {
            InitializeComponent();
            get_Profile();
        }

        private void AddUtilis_Click(object sender, EventArgs e)
        {
            string username = login.Text;
            string passe = pwd.Text;
            string passn = confpwd.Text;
            DictionaryEntry deImgType;
            string ident = "";
            string prof = "";
            if (profile.SelectedItem != null)
            {
                deImgType = (DictionaryEntry)profile.SelectedItem;
                ident = deImgType.Key.ToString();
                prof = deImgType.Value.ToString();
            }
            if (string.IsNullOrWhiteSpace(username))
                MessageBox.Show("Veuillez Introduire le login", "Alert");
            else
                if (string.IsNullOrWhiteSpace(passe))
                MessageBox.Show("Veuillez Introduire le mot de passe ", "Alert");
            else
                if (string.IsNullOrWhiteSpace(passn))
                MessageBox.Show("Veuillez confirmer le mot de passe inséré", "Alert");
            else
                if (passe != passn)
                MessageBox.Show("le mot de passe de confirmation est incorrect", "Alert");
            else
                if (string.IsNullOrWhiteSpace(prof))
                MessageBox.Show("Veuillez selectionner un profile", "Alert");
            
            else
            {
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                DataRow row;
                //DataSet ds = new PointeuseDBDataSet1();
                DataTable dataTable = new DataTable();
                Cursor = Cursors.WaitCursor;
                try
                {
                    conn.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT *  FROM Users", conn);
                    OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                    adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    adapter.Fill(dataTable);
                    bool insert = true;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i]["UserName"].ToString() == username)
                        {
                            MessageBox.Show("Vous avez déja déclaré un utilisateur avec le méme login !", "Alert");
                            insert = false;
                            i = dataTable.Rows.Count;
                        }
                    }
                    if (insert == true)
                    {
                        row = dataTable.NewRow();
                        row["UserName"] = username;
                        row["Pwd"] = passe;
                        row["IdProfile"] =ident;

                        dataTable.Rows.Add(row);
                        adapter.Update(dataTable);
                        dataTable.AcceptChanges();
                        MessageBox.Show("l'utilisateur a été enregistré", "succès");

                        this.Close();
                        Utilisateurs.Display_Users();
                        Program.log.Info("Add user," + username);

                    }
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message.ToString());
                    Program.log.Error("Add user," + exp.Message.ToString());
                }

                if (conn.State == ConnectionState.Open)
                    conn.Close();


                Cursor = Cursors.Default;

            }


        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void get_Profile()
        {

            profile.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,Nom from Profile", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["ID"].ToString();
                    nom = dataTable.Rows[i]["Nom"].ToString();
                    htPointInf.Add(ident, nom);
                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        profile.Items.Add(PointInf);
                    }
                    profile.DisplayMember = "value";
                    profile.ValueMember = "key";
                    profile.AutoCompleteMode = AutoCompleteMode.Suggest;
                    profile.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données","Erreur");
                Program.log.Error("get profile," + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }
    }
}
