﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.OleDb;
using System.Collections;
using AttLogs.Properties;

namespace AttLogs
{
    public partial class EditUser : MetroForm
    {
        public EditUser()
        {
            InitializeComponent();
            //Display Values
            get_Struct();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Modifier
            string strt = "",nom="",prenom="",matricule="",ident="",titre="",sql="";
            DictionaryEntry deImgType;
            
            if (structure.SelectedItem != null)
            {
                deImgType = (DictionaryEntry)structure.SelectedItem;
                strt = deImgType.Value.ToString().Replace("'", "''");
            }
            nom = this.noms.Text.Replace("'", "''");
            prenom = this.prenoms.Text.Replace("'", "''");
            matricule = this.matricule.Text.Replace("'", "''"); ;
            ident = this.ident.Text;
            titre = this.titres.Text.Replace("'", "''");
            bool edit = check_matr(matricule,ident);
            if (edit == true)
            {
                sql = "update Employer set Matricule= '" + matricule + "',Nom='" + nom + "',Prenom='" + prenom + "',LibSP='" + strt + "', LibEmp='" + titre + "' where Identifiant ='" + ident + "'";

                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                DataSet ds = new DataSet();
                Cursor = Cursors.WaitCursor;
                conn.Open();
                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();

                try
                {
                    oledbAdapter.DeleteCommand = conn.CreateCommand();
                    oledbAdapter.DeleteCommand.CommandText = sql;
                    oledbAdapter.DeleteCommand.ExecuteNonQuery();
                    MessageBox.Show("La valeur a été modifiée", "Succès");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    Program.log.Error("pointageGrid_CellEndEdit:update" + ex.Message.ToString());
                }

                finally
                {
                    conn.Close();
                    Cursor = Cursors.Default;
                    this.Close();
                    UsersList.DisplayData();
                }
            }
           
        }

        public bool check_matr(string matricule,string ident)
        {
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            bool check = true;
            try
            {
                conn.Open();
                OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT Identifiant,Matricule,IsActif FROM Employer", conn);
                OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    string mat = dataTable.Rows[i][1].ToString();
                    string id= dataTable.Rows[i][0].ToString();
                    string actif= dataTable.Rows[i][2].ToString();
                    if (mat == matricule && id!=ident )
                    {
                        MessageBox.Show("il existe un employer avec le même matricule!", "Error");
                        check = false;
                        this.matricule.Text = matricule;
                        return check;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Program.log.Error("ckeck_matricule:update" + ex.Message.ToString());
            }

            finally
            {
                conn.Close();
            }
            return check;

        }
        public void get_Struct()
        {

            this.structure.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib from Structures", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();

                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["lib"].ToString();
                    nom = dataTable.Rows[i]["lib"].ToString();
                    htPointInf.Add(ident, nom);

                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        structure.Items.Add(PointInf);
                    }
                    structure.DisplayMember = "value";
                    structure.ValueMember = "key";
                    structure.SelectedIndex = -1;
                    structure.AutoCompleteMode = AutoCompleteMode.Suggest;
                    structure.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_get_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }

    }
}
