﻿using AttLogs.Properties;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Windows.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace AttLogs
{
    public partial class IndivRepport : MetroForm
    {
        public IndivRepport()
        {
            InitializeComponent();
            get_Users();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Program.log.Info("Display Indiv repport");
                DictionaryEntry deImgType;
                string ident = "";
                string emp = "";
                if (users.SelectedItem != null)
                {
                    deImgType = (DictionaryEntry)users.SelectedItem;
                    ident = deImgType.Key.ToString();
                    emp = deImgType.Value.ToString();
                }
                ReportDocument cryRpt = new AttLogs.CrystalReport2();
                cryRpt.DataSourceConnections[0].SetConnection("", Settings.Default.DBLocation, false);
                Tables myTables = cryRpt.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table myTable in myTables)
                {
                    TableLogOnInfo myTableLogonInfo = myTable.LogOnInfo;
                    myTableLogonInfo.ConnectionInfo.Password = Settings.Default.Passe;
                    myTable.ApplyLogOnInfo(myTableLogonInfo);
                }
                //  string c = System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName;
                // cryRpt.Load(AttLogs.CrystalReport2);

                ParameterFieldDefinitions crParameterFieldDefinitions;
                ParameterFieldDefinition crParameterFieldDefinition;
                ParameterValues crParameterValues = new ParameterValues();
                ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();

                crParameterDiscreteValue.Value = jour.Text;
                crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["orderdate"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crParameterDiscreteValue.Value = jour1.Text;
                crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["orderdate1"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crParameterDiscreteValue.Value =ident;
                crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["user"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crParameterDiscreteValue.Value = emp;
                crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["employé"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crystalReportViewer1.ReportSource = cryRpt;
                crystalReportViewer1.Refresh();
                crystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
                crystalReportViewer1.ShowGroupTreeButton = false;
                crystalReportViewer1.ShowParameterPanelButton = false;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message.ToString());
                Program.log.Error("Change pwd," + exp.ToString());
            }
        }
        public  void get_Users()
        {

            users.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Identifiant,Nom,Prenom from Employer where IsActif='1'", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["Identifiant"].ToString();
                    nom = dataTable.Rows[i]["Nom"].ToString()+" "+ dataTable.Rows[i]["Prenom"].ToString(); ;
                    htPointInf.Add(ident, nom);
                }
                if (i > 0)
                {
                    
                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        users.Items.Add(PointInf);
                    }
                    users.DisplayMember = "value";
                    users.ValueMember = "key";
                    users.AutoCompleteMode = AutoCompleteMode.Suggest;
                    users.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to connect to data source");
                Program.log.Error("Indiv repport users list," + ex.ToString());
            }
            finally
            {

                conn.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
