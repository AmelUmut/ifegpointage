﻿using System.Drawing;

namespace AttLogs
{
    partial class Pointages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pointages));
            this.pointageGrid = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.enddate = new System.Windows.Forms.DateTimePicker();
            this.users = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Export = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.structure = new System.Windows.Forms.ComboBox();
            this.enddate1 = new System.Windows.Forms.DateTimePicker();
            this.search1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.startdate1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.startdate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.soir = new System.Windows.Forms.CheckBox();
            this.matin = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.begin_date = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.absent = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pointageGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pointageGrid
            // 
            this.pointageGrid.AllowUserToAddRows = false;
            this.pointageGrid.AllowUserToDeleteRows = false;
            this.pointageGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pointageGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.pointageGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.pointageGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.pointageGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.pointageGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pointageGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.pointageGrid.Location = new System.Drawing.Point(3, 133);
            this.pointageGrid.MultiSelect = false;
            this.pointageGrid.Name = "pointageGrid";
            this.pointageGrid.Size = new System.Drawing.Size(1269, 325);
            this.pointageGrid.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1182, 464);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 27);
            this.button1.TabIndex = 2;
            this.button1.Text = "Fermer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(269, 7);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(82, 48);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Chercher";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // enddate
            // 
            this.enddate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enddate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.enddate.Location = new System.Drawing.Point(172, 40);
            this.enddate.Name = "enddate";
            this.enddate.Size = new System.Drawing.Size(91, 22);
            this.enddate.TabIndex = 12;
            // 
            // users
            // 
            this.users.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.users.FormattingEnabled = true;
            this.users.Location = new System.Drawing.Point(58, 10);
            this.users.Name = "users";
            this.users.Size = new System.Drawing.Size(205, 22);
            this.users.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Employé";
            // 
            // Export
            // 
            this.Export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Export.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Export.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Export.ForeColor = System.Drawing.Color.MediumAquamarine;
            this.Export.Location = new System.Drawing.Point(1103, 58);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(169, 23);
            this.Export.TabIndex = 24;
            this.Export.Text = "Exporter la liste en XLS";
            this.Export.UseVisualStyleBackColor = true;
            this.Export.Click += new System.EventHandler(this.Export_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 32;
            this.label5.Text = "Structure";
            // 
            // structure
            // 
            this.structure.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.structure.FormattingEnabled = true;
            this.structure.Location = new System.Drawing.Point(68, 66);
            this.structure.Name = "structure";
            this.structure.Size = new System.Drawing.Size(246, 22);
            this.structure.TabIndex = 31;
            // 
            // enddate1
            // 
            this.enddate1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enddate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.enddate1.Location = new System.Drawing.Point(198, 43);
            this.enddate1.Name = "enddate1";
            this.enddate1.Size = new System.Drawing.Size(112, 22);
            this.enddate1.TabIndex = 28;
            // 
            // search1
            // 
            this.search1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.search1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.search1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search1.Location = new System.Drawing.Point(316, 6);
            this.search1.Name = "search1";
            this.search1.Size = new System.Drawing.Size(80, 47);
            this.search1.TabIndex = 26;
            this.search1.Text = "Chercher";
            this.search1.UseVisualStyleBackColor = false;
            this.search1.Click += new System.EventHandler(this.search1_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.startdate1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.search1);
            this.panel1.Controls.Add(this.enddate1);
            this.panel1.Location = new System.Drawing.Point(3, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 71);
            this.panel1.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(172, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 14);
            this.label1.TabIndex = 31;
            this.label1.Text = "Au";
            // 
            // startdate1
            // 
            this.startdate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startdate1.Location = new System.Drawing.Point(64, 44);
            this.startdate1.Name = "startdate1";
            this.startdate1.Size = new System.Drawing.Size(106, 20);
            this.startdate1.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(33, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Du";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.startdate);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.enddate);
            this.panel2.Controls.Add(this.users);
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(418, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(357, 71);
            this.panel2.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(147, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Au";
            // 
            // startdate
            // 
            this.startdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startdate.Location = new System.Drawing.Point(58, 43);
            this.startdate.Name = "startdate";
            this.startdate.Size = new System.Drawing.Size(86, 20);
            this.startdate.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Du";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Red;
            this.button2.Location = new System.Drawing.Point(1103, 95);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(169, 23);
            this.button2.TabIndex = 35;
            this.button2.Text = "Exporter la liste en PDF";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(0, 478);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Résultat:";
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.ForeColor = System.Drawing.SystemColors.Highlight;
            this.result.Location = new System.Drawing.Point(55, 478);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(0, 13);
            this.result.TabIndex = 37;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.soir);
            this.panel3.Controls.Add(this.matin);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.begin_date);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.absent);
            this.panel3.Location = new System.Drawing.Point(785, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(287, 71);
            this.panel3.TabIndex = 35;
            // 
            // soir
            // 
            this.soir.AutoSize = true;
            this.soir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.soir.Location = new System.Drawing.Point(84, 19);
            this.soir.Name = "soir";
            this.soir.Size = new System.Drawing.Size(91, 21);
            this.soir.TabIndex = 28;
            this.soir.Text = "Après-Mdi";
            this.soir.UseVisualStyleBackColor = true;
            // 
            // matin
            // 
            this.matin.AutoSize = true;
            this.matin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matin.Location = new System.Drawing.Point(7, 19);
            this.matin.Name = "matin";
            this.matin.Size = new System.Drawing.Size(61, 21);
            this.matin.TabIndex = 27;
            this.matin.Text = "Matin";
            this.matin.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 15);
            this.label8.TabIndex = 25;
            this.label8.Text = "Absences:";
            // 
            // begin_date
            // 
            this.begin_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.begin_date.Location = new System.Drawing.Point(45, 43);
            this.begin_date.Name = "begin_date";
            this.begin_date.Size = new System.Drawing.Size(121, 20);
            this.begin_date.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 17);
            this.label11.TabIndex = 22;
            this.label11.Text = "Jour:";
            // 
            // absent
            // 
            this.absent.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.absent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.absent.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.absent.Location = new System.Drawing.Point(196, 7);
            this.absent.Name = "absent";
            this.absent.Size = new System.Drawing.Size(82, 46);
            this.absent.TabIndex = 20;
            this.absent.Text = "Chercher";
            this.absent.UseVisualStyleBackColor = false;
            this.absent.Click += new System.EventHandler(this.absent_Click);
            // 
            // Pointages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 502);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.structure);
            this.Controls.Add(this.Export);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pointageGrid);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Pointages";
            this.Text = "Liste de Présence";
            ((System.ComponentModel.ISupportInitialize)(this.pointageGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView pointageGrid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker enddate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox users;
        private System.Windows.Forms.Button Export;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox structure;
        private System.Windows.Forms.DateTimePicker enddate1;
        private System.Windows.Forms.Button search1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker startdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker startdate1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label result;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker begin_date;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button absent;
        private System.Windows.Forms.CheckBox soir;
        private System.Windows.Forms.CheckBox matin;
        private System.Windows.Forms.Label label8;
    }
}
