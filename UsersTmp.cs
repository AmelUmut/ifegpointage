﻿using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class UsersTmp : MetroForm
    {
        public UsersTmp()
        {
            InitializeComponent();
            get_pointeuses();
        }

        private void sendTmp_Click(object sender, EventArgs e)
        {
            try
            {
                DictionaryEntry deImgType, deImgType1;
                string ident = "", point = "", ident2 = "", point2 = "";
                zkemkeeper.CZKEMClass axCZKEM1 = null;
                zkemkeeper.CZKEMClass axCZKEM2 = null;
                if (listeP1.SelectedItem != null)
                {
                    deImgType = (DictionaryEntry)listeP1.SelectedItem;
                    ident = deImgType.Key.ToString();
                    point = deImgType.Value.ToString();
                }
                if (listeP2.SelectedItem != null)
                {
                    deImgType1 = (DictionaryEntry)listeP2.SelectedItem;
                    ident2 = deImgType1.Key.ToString();
                    point2 = deImgType1.Value.ToString();

                }

                if (ident == "" || ident2 == "")
                {
                    MessageBox.Show("Veuillez Indiquer les deux pointeuses!", "Erreur");
                }
                else

                    if (ident == ident2)
                {
                    MessageBox.Show("les pointeuses séléctionées sont identiques!", "Erreur");
                }
                else
                {

                    string[] infs = ident.Split('/');
                    string[] infs2 = ident2.Split('/');

                if (AttLogsMain.IsConnected[Convert.ToInt32(infs[2])] == false)
                        MessageBox.Show("Veuillez connecter la pointeuse: " + point + "!", "Erreur");
                    else
                       if (AttLogsMain.IsConnected[Convert.ToInt32(infs2[2])] == false)
                        MessageBox.Show("Veuillez connecter la pointeuse: " + point2 + "!", "Erreur");

                    else
                    {
                        //Get fingerprint from first machine
                        string sdwEnrollNumber = "";
                        string sName = "";
                        string sPassword = "";
                        int iPrivilege = 0;
                        bool bEnabled = false;
                        int iMachineNumber = 1;
                        int idwFingerIndex;
                        string sTmpData = "";
                        int iTmpLength = 0;
                        int iFlag = 0;
                        lvDownload.Items.Clear();
                        lvDownload.BeginUpdate();
                        axCZKEM1 = AttLogsMain.axCZKEM[Convert.ToInt32(infs[2])];
                        axCZKEM2 = AttLogsMain.axCZKEM[Convert.ToInt32(infs2[2])];
                        axCZKEM1.EnableDevice(iMachineNumber, false);
                        Cursor = Cursors.WaitCursor;
                        axCZKEM1.ReadAllUserID(iMachineNumber);
                        axCZKEM1.ReadAllTemplate(iMachineNumber);//read all the users' fingerprint templates to the memory
                        while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
                        {
                            for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                            {
                                if (axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                                {
                                    ListViewItem list = new ListViewItem();
                                    list.Text = sdwEnrollNumber;
                                    list.SubItems.Add(sName);
                                    list.SubItems.Add(idwFingerIndex.ToString());
                                    list.SubItems.Add(sTmpData);
                                    list.SubItems.Add(iPrivilege.ToString());
                                    list.SubItems.Add(sPassword);
                                    if (bEnabled == true)
                                    {
                                        list.SubItems.Add("true");
                                    }
                                    else
                                    {
                                        list.SubItems.Add("false");
                                    }
                                    list.SubItems.Add(iFlag.ToString());
                                    lvDownload.Items.Add(list);
                                }
                            }
                        }
                        lvDownload.EndUpdate();
                        axCZKEM1.EnableDevice(iMachineNumber, true);

                        if (lvDownload.Items.Count == 0)
                        {
                            MessageBox.Show("aucune empreinte trouvée à charger!", "Error");
                            Cursor = Cursors.Default;

                        }
                        else
                        {
                            //Send fingerprint
                            int idwErrorCode = 0;

                            sdwEnrollNumber = "";
                            sName = "";
                            idwFingerIndex = 0;
                            sTmpData = "";
                            iPrivilege = 0;
                            sPassword = "";
                            iFlag = 0;
                            string sEnabled = "";
                            bEnabled = false;

                            axCZKEM2.EnableDevice(iMachineNumber, false);
                            for (int i = 0; i < lvDownload.Items.Count; i++)
                            {
                                sdwEnrollNumber = lvDownload.Items[i].SubItems[0].Text.Trim();
                                sName = lvDownload.Items[i].SubItems[1].Text.Trim();
                                idwFingerIndex = Convert.ToInt32(lvDownload.Items[i].SubItems[2].Text.Trim());
                                sTmpData = lvDownload.Items[i].SubItems[3].Text.Trim();
                                iPrivilege = Convert.ToInt32(lvDownload.Items[i].SubItems[4].Text.Trim());
                                sPassword = lvDownload.Items[i].SubItems[5].Text.Trim();

                                sEnabled = lvDownload.Items[i].SubItems[6].Text.Trim();
                                iFlag = Convert.ToInt32(lvDownload.Items[i].SubItems[7].Text);
                                if (sEnabled == "true")
                                {
                                    bEnabled = true;
                                }
                                else
                                {
                                    bEnabled = false;
                                }

                                if (axCZKEM2.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled))//upload user information to the device
                                {
                                    axCZKEM2.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData);//upload templates information to the device
                                }
                                else
                                {
                                    axCZKEM2.GetLastError(ref idwErrorCode);
                                    MessageBox.Show("Echec,ErrorCode=" + idwErrorCode.ToString(), "Error");
                                    Cursor = Cursors.Default;
                                    axCZKEM2.EnableDevice(iMachineNumber, true);
                                    return;
                                }
                            }
                            axCZKEM2.RefreshData(iMachineNumber);//the data in the device should be refreshed
                            Cursor = Cursors.Default;
                            axCZKEM2.EnableDevice(iMachineNumber, true);
                            MessageBox.Show("Les Utilisateurs sont bien envoyés, " + "total:" + lvDownload.Items.Count.ToString(), "Success");
                            this.Close();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Program.log.Error("FingerPrint" + ex.ToString());
            }
        }

        private void closeP_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void get_pointeuses()
        {
            listeP1.Items.Clear();
            listeP2.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select * from Pointeuse order by ID", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                string ident, nom,isprinc;
                Hashtable htPointInf = new Hashtable();
                Hashtable htPointInfs = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["IP"].ToString()+"/"+ dataTable.Rows[i]["Port"].ToString()+"/"+i;
                    nom = dataTable.Rows[i]["Nom"].ToString();
                    isprinc= dataTable.Rows[i]["IsPrincipal"].ToString();
                    if(isprinc=="1")
                    htPointInfs.Add(ident, nom);
                    else
                        htPointInf.Add(ident, nom);
                }
                if (i > 0)
                {
                   
                    foreach (DictionaryEntry PointInfs in htPointInfs)
                    {
                        listeP1.Items.Add(PointInfs);
                    }
                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        listeP2.Items.Add(PointInf);
                    }
                    listeP1.DisplayMember = "value";
                    listeP1.ValueMember = "key";
                    listeP2.DisplayMember = "value";
                    listeP2.ValueMember = "key";

                    listeP1.AutoCompleteMode = AutoCompleteMode.Suggest;
                    listeP1.AutoCompleteSource = AutoCompleteSource.ListItems;
                    listeP2.AutoCompleteMode = AutoCompleteMode.Suggest;
                    listeP2.AutoCompleteSource = AutoCompleteSource.ListItems;


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                Program.log.Error("UsersTmp_get_pointeuses" + ex.Message.ToString());

            }
            finally
            {

                conn.Close();
            }
        }
    
    }
}
