﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Specialized;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class ParamForm : MetroForm
    {
        public ParamForm()
        {
            InitializeComponent();
            if (Login.idPro == "3")
            {
                heureE1.Enabled = false;
                heureS1.Enabled = false;
                heureE2.Enabled = false;
                heureS2.Enabled = false;
             //   heureE0.Enabled = false;
             //   heureS3.Enabled = false;
                valider.Enabled = false;
            }


        }

        private void annuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void valider_Click(object sender, EventArgs e)
        {
           
            string HeureE1 = heureE1.Text;
            string HeureS1 = heureS1.Text;
            string HeureE2 = heureE2.Text;
            string HeureS2 = heureS2.Text;
       //     string HeureE0 = heureE0.Text;
       //     string HeureS3 = heureS3.Text;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataRow row;
            DataSet ds = new DataSet();
            Cursor = Cursors.WaitCursor;
            try
            {
                conn.Open();
                OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM parametrage", conn);
                OleDbCommandBuilder cmdBuilder =new OleDbCommandBuilder(adapter);
                adapter.MissingSchemaAction =MissingSchemaAction.AddWithKey;
                adapter.Fill(ds, "parametrage");
                //Insert new Row
                if (ds.Tables["parametrage"].Rows.Count == 0)
                {
                    row = ds.Tables["parametrage"].NewRow();
             //       row["HeureE0"] = HeureE0;
                    row["HeureE1"] = HeureE1;
                    row["HeureE2"] = HeureE2;
                    row["HeureS1"] = HeureS1;
                    row["HeureS2"] = HeureS2;
             //       row["HeureS3"] = HeureS3;
                    ds.Tables["parametrage"].Rows.Add(row);
                }
                else
                {
                    //Update
                    DataRow row1 = ds.Tables["parametrage"].Rows[0];
               //     row1["HeureE0"] = HeureE0;
                    row1["HeureE1"] = HeureE1;
                    row1["HeureE2"] = HeureE2;
                    row1["HeureS1"] = HeureS1;
                    row1["HeureS2"] = HeureS2;
           //         row1["HeureS3"] = HeureS3;
                }

                adapter.Update(ds, "parametrage");
                ds.AcceptChanges();
                MessageBox.Show("les paramétres ont été bien enregistrés");
            }
            catch (Exception exp)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("SetParam" + exp.Message.ToString());
            }

            if (conn.State == ConnectionState.Open)
                conn.Close();
            AttLogsMain.get_param();

            Cursor = Cursors.Default;
            ParamForm.ActiveForm.Close();


        }
    }
}
