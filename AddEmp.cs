﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.OleDb;
using AttLogs.Properties;
using System.Collections;

namespace AttLogs
{
    public partial class AddEmp : MetroForm
    {
        public AddEmp()
        {
            InitializeComponent();
            get_Struct();
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void add_Click(object sender, EventArgs e)
        {
            string name = namSt.Text;
            if (name == "")
            {
                MessageBox.Show("veuillez Introduire le titre", "Alert");
            }
            else
            if (listStruct.SelectedItem == null)
            {
                MessageBox.Show("veuillez Sélectionner la Structure", "Alert");
            }
            
            else
            {
                DictionaryEntry deImgType = (DictionaryEntry)listStruct.SelectedItem;
                int ident1 =Convert.ToInt32( deImgType.Value.ToString());
                DataTable dataTable = new DataTable();
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                DataRow row;
                try
                {
                    conn.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT *  FROM Postes", conn);
                    OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                    adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    adapter.Fill(dataTable);

                    //Insert new Row

                    row = dataTable.NewRow();
                    row["Structure"] = ident1;
                    row["Titre"] = name.ToString();
                    dataTable.Rows.Add(row);
                    adapter.Update(dataTable);
                    dataTable.AcceptChanges();
                    MessageBox.Show("le nouveau titre a été enregistré", "succès");

                    this.Close();
                    Emploi.DisplayData();
                    Program.log.Info("Ajouter une Structure," + namSt);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                    Program.log.Error("AddTitre" + ex.Message.ToString());
                }
                finally
                {

                    conn.Close();
                }
            }
         }
        public void get_Struct()
        {

            this.listStruct.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib from Structures", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();

                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["ID"].ToString();
                    nom = dataTable.Rows[i]["lib"].ToString();
                    htPointInf.Add(ident, nom);

                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        listStruct.Items.Add(PointInf);
                    }
                    listStruct.DisplayMember = "value";
                    listStruct.ValueMember = "key";
                    listStruct.SelectedIndex = -1;
                    listStruct.AutoCompleteMode = AutoCompleteMode.Suggest;
                    listStruct.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_get_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }
    }
}
