﻿namespace AttLogs
{
    partial class Structure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pointGridView = new System.Windows.Forms.DataGridView();
            this.close = new System.Windows.Forms.Button();
            this.update = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(pointGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // pointGridView
            // 
            pointGridView.AllowUserToAddRows = false;
            pointGridView.AllowUserToDeleteRows = false;
            pointGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            pointGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            pointGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            pointGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            pointGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            pointGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            pointGridView.Location = new System.Drawing.Point(40, 96);
            pointGridView.Name = "pointGridView";
            pointGridView.Size = new System.Drawing.Size(714, 279);
            pointGridView.TabIndex = 1;
            pointGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.pointGridView_CellBeginEdit);
            pointGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.pointGridView_CellEndEdit);
            // 
            // close
            // 
            this.close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.close.Location = new System.Drawing.Point(626, 381);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(128, 33);
            this.close.TabIndex = 3;
            this.close.Text = "Fermer";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(632, 48);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(122, 33);
            this.update.TabIndex = 8;
            this.update.Text = "Modifier";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(509, 48);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(122, 33);
            this.button4.TabIndex = 10;
            this.button4.Text = "Ajouter";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Structure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 424);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.update);
            this.Controls.Add(this.close);
            this.Controls.Add(pointGridView);
            this.Name = "Structure";
            this.Text = "Structures";
            ((System.ComponentModel.ISupportInitialize)(pointGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button button4;
        public static System.Windows.Forms.DataGridView pointGridView;
    }
}