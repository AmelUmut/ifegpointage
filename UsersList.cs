﻿using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class UsersList : MetroForm
    {
        public UsersList()
        {
            InitializeComponent();
            DisplayData();
            foreach (DataGridViewRow row in pointGridView.Rows)
            {
                row.ReadOnly = true;
            }
        }
        public static void DisplayData()
        {
            Cursor.Current = Cursors.WaitCursor;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
           
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Identifiant,Matricule,Nom,Prenom,LibSP as Structure,LibEmp as Emploi from Employer where IsActif='1' order by LibSP", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                DataTable dataTable1 = new DataTable();
                dAdapter.Fill(dataTable);
                pointGridView.DataSource = dataTable;
                dAdapter = new OleDbDataAdapter("select lib from Structures order by lib", Settings.Default.PointeuseDBConnectionString);
                cBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.Fill(dataTable1);
                IList<string> lstFirst = new List<string>();
                foreach (DataRow row in dataTable1.Rows)
                {
                    lstFirst.Add(row.Field<string>("lib"));
                    
                }
                listep.Items.AddRange(lstFirst.ToArray<string>());
                listep.AutoCompleteMode = AutoCompleteMode.Suggest;
                listep.AutoCompleteSource = AutoCompleteSource.ListItems;
                



            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("UsersList" + ex.Message.ToString());
                Cursor.Current = Cursors.Default;
            }
            finally
            {

                conn.Close();

                Cursor.Current = Cursors.Default; 

            }
        }
        private int dgvHeight()
        {
            int sum = pointGridView.ColumnHeadersHeight;

            foreach (DataGridViewRow row in pointGridView.Rows)
                sum += row.Height + 1; // I dont think the height property includes the cell border size, so + 1

            return sum;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchP_Click(object sender, EventArgs e)
        {
            
            string ident = "";
            if (listep.SelectedItem != null)
            {
                ident = listep.SelectedItem.ToString();
            }
            BindingSource bs = new BindingSource();
            bs.DataSource = pointGridView.DataSource;
            bs.Filter = pointGridView.Columns[4].HeaderText.ToString() + " LIKE '%" + ident + "%'";
            pointGridView.DataSource = bs;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pointGridView.RowCount > 0)
            {
                bool del = false;
                string sel = "";
                foreach (DataGridViewRow item in pointGridView.SelectedRows)
                {
                    del = true;

                    //Identifiant
                    string id = pointGridView.CurrentRow.Cells[0].Value.ToString();
                    //New Form
                    var myForm = new EditUser();
                    myForm.Show();
                     ////Set Data
                     sel= pointGridView.CurrentRow.Cells[4].Value.ToString();
                    System.Windows.Forms.Form f = System.Windows.Forms.Application.OpenForms["EditUser"];
                    ((EditUser)f).noms.Text= pointGridView.CurrentRow.Cells[2].Value.ToString();
                    ((EditUser)f).prenoms.Text = pointGridView.CurrentRow.Cells[3].Value.ToString();
                    ((EditUser)f).titres.Text = pointGridView.CurrentRow.Cells[5].Value.ToString();
                    ((EditUser)f).matricule.Text = pointGridView.CurrentRow.Cells[1].Value.ToString();
                    ((EditUser)f).ident.Text = pointGridView.CurrentRow.Cells[0].Value.ToString();
                    ((EditUser)f).structure.SelectedIndex = ((EditUser)f).structure.FindStringExact(sel);
                }
                if (del == false)
                    MessageBox.Show("Veuillez sélectionner toute la ligne à Modifier", "Alert");
            }

        }

       
        private void pointGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            pointGridView.Tag = pointGridView.CurrentCell.Value;
        }

        private bool get_struct(string lib)
        {
            bool find = false;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataTable dataTable = new DataTable();
            OleDbDataAdapter dAdapter = new OleDbDataAdapter("select lib from Structures where lib= '"+ lib +"'", Settings.Default.PointeuseDBConnectionString);
            OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
            dAdapter.Fill(dataTable);
            if ( dataTable.Rows.Count>0)
            {
                find = true;

            }

            return find;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (pointGridView.RowCount > 0)
            {
                bool del = false;
                foreach (DataGridViewRow item in pointGridView.SelectedRows)
                {
                    del = true;
                    DialogResult dialogResult = MessageBox.Show("voulez-vous Supprimer cet employé?", "Confirmation", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.OK)
                    {
                        string id = pointGridView.CurrentRow.Cells[0].Value.ToString();

                        OleDbCommand oleDbCmd = new OleDbCommand();
                        System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                        conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                        DataSet ds = new DataSet();
                        Cursor = Cursors.WaitCursor;
                        conn.Open();
                        OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                        string sql = null;
                        sql = "update Employer set IsActif = '0' where Identifiant = '" + id + "'";
                        try
                        {
                            oledbAdapter.DeleteCommand = conn.CreateCommand();
                            oledbAdapter.DeleteCommand.CommandText = sql;
                            oledbAdapter.DeleteCommand.ExecuteNonQuery();
                            MessageBox.Show("L'employé a été supprimé", "Succès");
                            pointGridView.Rows.RemoveAt(item.Index);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        finally
                        {
                            conn.Close();
                            Cursor = Cursors.Default;
                        }
                    }
                }
                if (del == false)
                    MessageBox.Show("Veuillez sélectionner toute la ligne à supprimer", "Alert");
            }
        }

        

        private void label3_Click(object sender, EventArgs e)
        {
            var myForm = new PersInactif();
            myForm.Show();
        }
    }
}
