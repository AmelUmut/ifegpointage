﻿namespace AttLogs
{
    partial class Emploi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.add = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.update = new System.Windows.Forms.Button();
            this.close = new System.Windows.Forms.Button();
            pointGridView = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.searchP = new System.Windows.Forms.Button();
            listep = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(pointGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(690, 50);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(122, 33);
            this.add.TabIndex = 15;
            this.add.Text = "Ajouter";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(936, 50);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(122, 33);
            this.delete.TabIndex = 14;
            this.delete.Text = "Supprimer";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(813, 50);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(122, 33);
            this.update.TabIndex = 13;
            this.update.Text = "Modifier";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // close
            // 
            this.close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.close.Location = new System.Drawing.Point(936, 401);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(128, 33);
            this.close.TabIndex = 12;
            this.close.Text = "Fermer";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // pointGridView
            // 
            pointGridView.AllowUserToAddRows = false;
            pointGridView.AllowUserToDeleteRows = false;
            pointGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            pointGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            pointGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            pointGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            pointGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            pointGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            pointGridView.Location = new System.Drawing.Point(40, 89);
            pointGridView.Name = "pointGridView";
            pointGridView.Size = new System.Drawing.Size(1018, 279);
            pointGridView.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(45, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 18;
            this.label2.Text = "Structure:";
            // 
            // searchP
            // 
            this.searchP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.searchP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchP.Location = new System.Drawing.Point(404, 60);
            this.searchP.Name = "searchP";
            this.searchP.Size = new System.Drawing.Size(116, 23);
            this.searchP.TabIndex = 17;
            this.searchP.Text = "Chercher";
            this.searchP.UseVisualStyleBackColor = true;
            this.searchP.Click += new System.EventHandler(this.searchP_Click);
            // 
            // listep
            // 
            listep.FormattingEnabled = true;
            listep.Location = new System.Drawing.Point(115, 60);
            listep.Name = "listep";
            listep.Size = new System.Drawing.Size(259, 21);
            listep.TabIndex = 16;
            // 
            // Emploi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 457);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchP);
            this.Controls.Add(listep);
            this.Controls.Add(this.add);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.update);
            this.Controls.Add(this.close);
            this.Controls.Add(pointGridView);
            this.Name = "Emploi";
            this.Text = "Emplois";
            ((System.ComponentModel.ISupportInitialize)(pointGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button close;
        public static System.Windows.Forms.DataGridView pointGridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button searchP;
        public static System.Windows.Forms.ComboBox listep;
    }
}