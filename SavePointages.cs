﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.OleDb;
using AttLogs.Properties;

namespace AttLogs
{
    public partial class SavePointages : MetroForm
    {
        public SavePointages()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void save_Click(object sender, EventArgs e)
        {

            bool bIsConnected = true;
            for (int i = 0; i < AttLogsMain.IsConnected.Count; i++)
            {
                if (AttLogsMain.IsConnected[i] == false)
                    bIsConnected = false;
            }
            if (bIsConnected == false)
            {
                MessageBox.Show("Veuillez connecter toute les pointeuses!", "Error");
                return;
            }


            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            conn.Open();
            Cursor = Cursors.WaitCursor;
            
            bool cl =false;
            for (int i = 0; i < AttLogsMain.axCZKEM.Count; i++)
            {
                
                if (AttLogsMain.IsConnected[i] == true)
                {
                    string sdwEnrollNumber = "";
                    int idwVerifyMode = 0;
                    int idwInOutMode = 0;
                    int idwYear = 0;
                    int idwMonth = 0;
                    int idwDay = 0;
                    int idwHour = 0;
                    int idwMinute = 0;
                    int idwSecond = 0;
                    int idwWorkcode = 0;
                    int idwErrorCode = 0;
                    bool clear = true;
                    //Insert_Users(axCZKEM[i], ChfalseeckBoxes[i].Text);

                    AttLogsMain.axCZKEM[i].EnableDevice(AttLogsMain.iMachineNumber, false);//disable the device
                    if (AttLogsMain.axCZKEM[i].ReadGeneralLogData(AttLogsMain.iMachineNumber))//read all the attendance records to the memory
                    {
                        
                        try
                        {
                            DataRow row;
                            DataSet ds = new DataSet();
                            OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM Pointage", conn);
                            OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                            adapter.Fill(ds, "Pointage");
                            int rw = 0;
                            while (AttLogsMain.axCZKEM[i].SSR_GetGeneralLogData(AttLogsMain.iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                                    out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                            {

                                //Insert new Row
                                    DateTime dt = new DateTime(idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond);
                               
                                    row = ds.Tables["Pointage"].NewRow();
                                    row["IdUser"] =sdwEnrollNumber;
                                    row["DateP"] = dt;
                                    row["InOutMode"] = idwInOutMode;
                                    row["Pointeuse"] = AttLogsMain.CheckBoxes[i].Text;
                                    ds.Tables["Pointage"].Rows.Add(row);
                                    rw++;
                             }

                            adapter.Update(ds, "Pointage");
                            ds.AcceptChanges();
                            cl = true;
                            if (rw > 0)
                                MessageBox.Show(AttLogsMain.CheckBoxes[i].Text + ":les pointages ont été bien enregistrés");
                            else
                            {
                                MessageBox.Show(AttLogsMain.CheckBoxes[i].Text + ":aucun pointage trouvé !");
                                cl = false;
                            }

                        }
                        catch (OleDbException exp)
                        {
                            cl = false;
                            clear = false;
                            MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                        }

                    }


                    else
                    {
                        cl = false;
                        clear = false;
                        Cursor = Cursors.Default;
                        AttLogsMain.axCZKEM[i].GetLastError(ref idwErrorCode);

                        if (idwErrorCode != 0)
                        {
                            MessageBox.Show(AttLogsMain.CheckBoxes[i].Text + ":la Lecture des pointages a échoué ,ErrorCode: " + idwErrorCode.ToString(), "Error");
                        }
                        else
                        {
                            MessageBox.Show(AttLogsMain.CheckBoxes[i].Text + ":la pointeuse est vide!", "Error");
                        }
                    }
                    AttLogsMain.axCZKEM[i].EnableDevice(AttLogsMain.iMachineNumber, true);//enable the device
                    Cursor = Cursors.Default;
                    if(clear==true)
                    AttLogsMain.ClearGLog(AttLogsMain.axCZKEM[i], AttLogsMain.CheckBoxes[i].Text);
                }
            }
            
            
            if (conn.State == ConnectionState.Open)
                conn.Close();
            Cursor = Cursors.Default;
            this.Close();

        }
        public static void insertnew(DateTime datex)
        {
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataRow row = null;
            DataSet ds = new DataSet();
            try
            {
                DateTime dt = Convert.ToDateTime(datex);
                string st = dt.ToString("yyyy/MM/dd HH:mm:ss");
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select IdUser,DateP,DateValue(DateP) as Jour,FORMAT(DateP, 'hh:mm:ss') as Heure from Pointage where Pointage.DateP> #" + st + "# order by DateValue(DateP) desc, IdUser desc,  FORMAT(DateP, 'hh:mm:ss') asc ", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    string jj = "", datep = "", id = "",heurex,heure="";
                    
                    int val = 0;
                    dAdapter = new OleDbDataAdapter("SELECT top 1 * FROM Temp", conn);
                    OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(dAdapter);
                    dAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    dAdapter.Fill(ds, "Temp");
                    TimeSpan plus,moins,tt, tol;
                    int j = 0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        heurex = "";
                        plus = TimeSpan.Parse("00:00:00");
                        moins = TimeSpan.Parse("00:00:00");
                        tol = TimeSpan.Parse(Settings.Default.Tolerance);

                        if (val == 2)
                        {
                            val = 0;
                        }
                        else
                        if (id != dataTable.Rows[i][0].ToString() || jj != dataTable.Rows[i][2].ToString())
                        {
                            
                            val = 0;
                        }
                        
                        datep = dataTable.Rows[i][1].ToString();
                        id = dataTable.Rows[i][0].ToString();
                        jj= dataTable.Rows[i][2].ToString();
                        heure = dataTable.Rows[i][3].ToString();
                        if (i > 0)
                        {
                            if (id == dataTable.Rows[j][0].ToString() && jj == dataTable.Rows[j][2].ToString())
                            {
                                heurex = dataTable.Rows[j][3].ToString();
                            }
                        }
                        if (TimeSpan.TryParse(heure, out tt) == true)
                            plus = TimeSpan.Parse(heure);
                        if (TimeSpan.TryParse(heurex, out tt) == true)
                            moins = TimeSpan.Parse(heurex);

                        if (plus > moins + tol)
                        {
                            row = ds.Tables["Temp"].NewRow();
                            row["IdUser"] = id;
                            var res = Convert.ToDateTime(datep);
                            row["Jour"] = res.ToString("dd/MM/yyyy HH:mm:ss");
                            if (val % 2 == 0)
                                row["Mode"] = "0";
                            else
                                row["Mode"] = "1";
                            val++;
                            j = i;
                            ds.Tables["Temp"].Rows.Add(row);
                        }
                    }
                    dAdapter.Update(ds, "Temp");
                    ds.AcceptChanges();
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                Program.log.Error("pointage list" + ex.Message.ToString());

            }
            finally
            { conn.Close(); }

        }
    }
}
