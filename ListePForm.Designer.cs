﻿namespace AttLogs
{
    partial class ListePForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListePForm));
            this.pointGridView = new System.Windows.Forms.DataGridView();
            this.saveP = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.deleteP = new System.Windows.Forms.Button();
            this.editP = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pointGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pointGridView
            // 
            this.pointGridView.AllowUserToAddRows = false;
            this.pointGridView.AllowUserToDeleteRows = false;
            this.pointGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pointGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.pointGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.pointGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pointGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pointGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.pointGridView.Location = new System.Drawing.Point(19, 61);
            this.pointGridView.Name = "pointGridView";
            this.pointGridView.Size = new System.Drawing.Size(477, 153);
            this.pointGridView.TabIndex = 0;
            this.pointGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.pointGridView_CellBeginEdit);
            this.pointGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.pointGridView_CellEndEdit);
            // 
            // saveP
            // 
            this.saveP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.saveP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveP.Location = new System.Drawing.Point(431, 322);
            this.saveP.Name = "saveP";
            this.saveP.Size = new System.Drawing.Size(100, 42);
            this.saveP.TabIndex = 2;
            this.saveP.Text = "Enregistrer";
            this.saveP.UseVisualStyleBackColor = true;
            this.saveP.Click += new System.EventHandler(this.saveP_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(13, 322);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 42);
            this.button2.TabIndex = 3;
            this.button2.Text = "Annuler";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // deleteP
            // 
            this.deleteP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteP.Location = new System.Drawing.Point(339, 19);
            this.deleteP.Name = "deleteP";
            this.deleteP.Size = new System.Drawing.Size(157, 36);
            this.deleteP.TabIndex = 4;
            this.deleteP.Text = "Supprimer Pointeuse";
            this.deleteP.UseVisualStyleBackColor = true;
            this.deleteP.Click += new System.EventHandler(this.deleteP_Click);
            // 
            // editP
            // 
            this.editP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.editP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editP.Location = new System.Drawing.Point(174, 19);
            this.editP.Name = "editP";
            this.editP.Size = new System.Drawing.Size(140, 36);
            this.editP.TabIndex = 5;
            this.editP.Text = "Modifer une valeur";
            this.editP.UseVisualStyleBackColor = true;
            this.editP.Click += new System.EventHandler(this.editP_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pointGridView);
            this.groupBox1.Controls.Add(this.editP);
            this.groupBox1.Controls.Add(this.deleteP);
            this.groupBox1.Location = new System.Drawing.Point(13, 82);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(518, 234);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // ListePForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 391);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.saveP);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ListePForm";
            this.Text = "Liste Pointeuses";
            ((System.ComponentModel.ISupportInitialize)(this.pointGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView pointGridView;
        private System.Windows.Forms.Button saveP;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button deleteP;
        private System.Windows.Forms.Button editP;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}