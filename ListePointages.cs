﻿using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using MetroFramework.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Resources;

namespace AttLogs
{
    public partial class ListePointages : MetroForm
    {
        bool edit = false;
        public ListePointages()
        {
            InitializeComponent();
            startdate.Text = "01/01/" + DateTime.Now.Year;
            if (Login.idPro == "3" || Login.idPro == "2")
            {
                editP.Enabled = false;
                deleteP.Enabled = false;
            }

            
            get_Users();
            get_Struct();
            get_calc_pointage();
        }

        public static void DisplayData(DateTime k)
        {
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataRow row = null,row1=null;
            DataSet ds=new DataSet();
            try
            {
                conn.Open();
                DateTime dt = Convert.ToDateTime(k);
                string st = dt.ToString("yyyy/MM/dd HH:mm:ss");
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Temp.IdUser,DateValue(Jour) as datep,FORMAT(Jour, 'hh:mm:ss') as Heure,Nom,Prenom,Matricule,LibEmp,LibSP,Mode,Jour from Temp inner join Employer on Employer.Identifiant=Temp.IdUser where IsActif='1' and Temp.Jour > #" + st + "# order by DateValue(Jour) desc, IdUser desc,  FORMAT(Jour, 'hh:mm:ss') asc ", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                dAdapter = new OleDbDataAdapter("SELECT top 1 * FROM PointageCal", conn);
                OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                dAdapter.Fill(ds, "PointageCal");
                OleDbDataAdapter dAdapter1 = new OleDbDataAdapter("SELECT top 1 * FROM Presence", conn);
                cmdBuilder = new OleDbCommandBuilder(dAdapter1);
                dAdapter1.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                dAdapter1.Fill(ds, "Presence");
                if (dataTable.Rows.Count > 0)
                {
                    string  heure = "",mode="", datep = "",jj="", id = "",nom="",prenom="",mat="",poste="",structure="";
                    TimeSpan tt, ret= TimeSpan.Parse("00:00:00"), ret1 = TimeSpan.Parse("00:00:00");
                    int val = 0,j=0,j1=0;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        
                        if (val==2)
                        {
                            ds.Tables["PointageCal"].Rows.Add(row);
                            row = ds.Tables["PointageCal"].NewRow();
                            val = 0;
                        }
                        else
                        if (id != dataTable.Rows[i][0].ToString() || datep != dataTable.Rows[i][1].ToString())
                        {
                            if (id != "" && datep != "")
                            {

                                ds.Tables["PointageCal"].Rows.Add(row);

                            }
                            row = ds.Tables["PointageCal"].NewRow();
                            
                            val = 0;
                        }

                        if (id != dataTable.Rows[i][0].ToString() || datep != dataTable.Rows[i][1].ToString())
                        {
                            if (id != "" && datep != "")
                            {
                                ds.Tables["Presence"].Rows.Add(row1);
                                ret = TimeSpan.Parse("00:00:00");
                                ret1 = TimeSpan.Parse("00:00:00");
                                j = 0;
                                j1 = 0;
                            }
                            row1 = ds.Tables["Presence"].NewRow();
                            row1["AM"] = "Absent";
                            row1["PM"] = "Absent";
                        }

                        datep = dataTable.Rows[i][1].ToString();
                        heure = dataTable.Rows[i][2].ToString();
                        id = dataTable.Rows[i][0].ToString();
                        nom = dataTable.Rows[i][3].ToString();
                        prenom = dataTable.Rows[i][4].ToString();
                        mat = dataTable.Rows[i][5].ToString();
                        poste = dataTable.Rows[i][6].ToString();
                        structure = dataTable.Rows[i][7].ToString();
                        mode = dataTable.Rows[i][8].ToString();
                        jj = dataTable.Rows[i][9].ToString();
                        var res = Convert.ToDateTime(datep);
                        row["IdUser"] = id;
                        row["Jour"] = res.ToString("dd/MM/yyyy"); 
                        row["NomEmp"] = nom;
                        row["Prenomemp"] = prenom;
                        row["Matricule"] = mat;
                        row["Poste"] = poste;
                        row["Struct"] = structure;
                        row["Observation"] = "";
                        row["DateP"]= jj;
                        row1["IdUser"] = id;
                        row1["Jour"] = res.ToString("dd/MM/yyyy");


                        ///////////////
                        if (mode == "0")
                        {
                            row["Entrée"] = heure;
                            //Matin
                            if (TimeSpan.Parse(heure) <= TimeSpan.Parse(AttLogsMain.heurS1))
                            {
                                if (j == 0)
                                {
                                    if (TimeSpan.Parse(heure) > TimeSpan.Parse(AttLogsMain.heurE1))
                                        ret = ret + (TimeSpan.Parse(heure) - TimeSpan.Parse(AttLogsMain.heurE1));

                                }
                                else
                                {
                                    if (TimeSpan.Parse(heure) > TimeSpan.Parse(AttLogsMain.heurE1))
                                        ret = ret + (TimeSpan.Parse(heure) - TimeSpan.Parse(AttLogsMain.heurS1));

                                }
                                j++;
                            }
                            else
                            {
                                if (j1 == 0)
                                {
                                    if (TimeSpan.Parse(heure) > TimeSpan.Parse(AttLogsMain.heurE2))
                                        ret1 = ret1 + (TimeSpan.Parse(heure) - TimeSpan.Parse(AttLogsMain.heurE2));

                                }
                                else
                                {
                                    if (TimeSpan.Parse(heure) > TimeSpan.Parse(AttLogsMain.heurE2))
                                        ret1 = ret1 + (TimeSpan.Parse(heure) - TimeSpan.Parse(AttLogsMain.heurS2));

                                }
                                
                                j1++;
                            }
                        }
                        else
                        {
                            row["Sortie"] = heure;
                            if (TimeSpan.Parse(heure) < TimeSpan.Parse(AttLogsMain.heurS1))
                                ret = ret + (TimeSpan.Parse(AttLogsMain.heurS1)- TimeSpan.Parse(heure));
                            if (TimeSpan.Parse(heure) < TimeSpan.Parse(AttLogsMain.heurS2) && TimeSpan.Parse(heure)> TimeSpan.Parse(AttLogsMain.heurE2))
                                ret1 = ret1 + (TimeSpan.Parse(AttLogsMain.heurS2) - TimeSpan.Parse(heure));
                        }
                        val++;
                        if (TimeSpan.TryParse(AttLogsMain.heurE2, out tt) == true)
                        {
                            if (TimeSpan.TryParse(row["Entrée"].ToString(), out tt) == true)
                            {
                                if (TimeSpan.Parse(row["Entrée"].ToString()) <= TimeSpan.Parse(AttLogsMain.heurS1))
                                    row1["AM"] = "Présent";
                            }
                            if (TimeSpan.TryParse(row["Sortie"].ToString(), out tt) == true)
                            {
                                if (TimeSpan.Parse(row["Sortie"].ToString()) > TimeSpan.Parse(AttLogsMain.heurE2))
                                    row1["PM"] = "Présent";
                            }
                        }
                        
                        row1["Retard"] = ret.ToString();
                        
                        row1["Retard1"] = ret1.ToString();

                    }
                    ds.Tables["PointageCal"].Rows.Add(row);
                    ds.Tables["Presence"].Rows.Add(row1);
                    dAdapter.Update(ds, "PointageCal");
                    dAdapter1.Update(ds, "Presence");
                    ds.AcceptChanges();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                Program.log.Error("pointage list" + ex.Message.ToString());

            }
            finally
            {
                conn.Close();
            }
        }
        private void get_calc_pointage()
        {
            DataTable dataTable = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                string query = "select top 200 ID,NomEmp as Nom,PrenomEmp as Prénom,Matricule,Jour,Entrée,Sortie,Struct as Structure,poste as Emploi,Observation from PointageCal order by DateValue(Jour) desc, IdUser desc,Entrée asc ";
                OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.Fill(dataTable);
                pointageGrid.DataSource = dataTable;
                pointageGrid.Columns["ID"].Visible = false;

            }
            catch (Exception ex)
            {
                Program.log.Error("get_calc_pointage" + ex.Message.ToString());
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {

                conn.Close();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
       




        private void pointageGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
            if (edit == true)
            {
                var oldValue = pointageGrid.Tag;
                TimeSpan a;
                DateTime b;
                bool change = true;
                string newValue = pointageGrid.CurrentCell.Value.ToString();
                if ((oldValue.ToString() != newValue) && (newValue != ""))
                {
                    int columnIndex = pointageGrid.CurrentCell.ColumnIndex;
                    string columnName = pointageGrid.Columns[columnIndex].Name;
                    DialogResult dialogResult = MessageBox.Show("vous avez changé la valeur de '" + columnName + "',voulez-vous l'enregistrer?", "Confirmation", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.OK)
                    {
                        if (pointageGrid.CurrentCell.ColumnIndex == 4)
                        {
                            if (DateTime.TryParse(newValue, out b) == false)
                            {
                                MessageBox.Show("Format de donnée inccorcet,veuillez respecter le format 'jj/mm/aaaa'", "Erreur");
                                pointageGrid.CurrentCell.Value = oldValue;
                                change = false;
                            }
                        }
                        else
                        if (pointageGrid.CurrentCell.ColumnIndex != 9)
                        {
                            if (TimeSpan.TryParse(newValue, out a) == false)
                            {
                                MessageBox.Show("Format de donnée inccorcet,veuillez respecter le format 'hh:mm:ss'", "Erreur");
                                pointageGrid.CurrentCell.Value = oldValue;
                                change = false;
                            }
                        }
                        if (change == true)
                        {

                            string id = pointageGrid.CurrentRow.Cells[0].Value.ToString();
                            int col = pointageGrid.CurrentCell.ColumnIndex;
                            string namcol = null, msg = null;
                            switch (col.ToString())
                            {
                                //case "4":
                                //    namcol = "Jour";
                                //    break;
                                //case "5":
                                //    namcol = "Entrée";
                                    
                                //    break;
                                //case "6":
                                //    namcol = "Sortie";
                                //    break;
                                case "9":
                                    namcol = "Observation";
                                    break;
                                default:
                                    break;
                            }
                            if (msg == null)
                            {
                                DateTime jour = new DateTime();
                                TimeSpan tt = new TimeSpan();
                                OleDbCommand oleDbCmd = new OleDbCommand();
                                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                                DataSet ds = new DataSet();
                                Cursor = Cursors.WaitCursor;
                                conn.Open();
                                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                                string sql = null;
                                //if (namcol == "Entrée")
                                //{
                                //    tt = TimeSpan.Parse(newValue);
                                //    sql = "update PointageCal set Entrée= '" + tt + "' where ID =" + id + "";

                                //}
                                //else
                                //if(namcol == "Sortie")
                                //{
                                //    tt = TimeSpan.Parse(newValue);
                                //    sql = "update PointageCal set Sortie= '" + tt + "' where ID =" + id + "";
                                //}
                                
                                //else
                                //if (namcol == "Jour")
                                //{
                                //    jour = DateTime.Parse(newValue);
                                //    sql = "update PointageCal set Jour= '" + jour + "' where ID =" + id + "";
                                //}
                                //else
                                if (namcol == "Observation")
                                {
                                    sql = "update PointageCal set Observation= '" + newValue.Replace("'", "''") + "' where ID =" + id + "";
                                }
                                try
                                {
                                    oledbAdapter.DeleteCommand = conn.CreateCommand();
                                    oledbAdapter.DeleteCommand.CommandText = sql;
                                    oledbAdapter.DeleteCommand.ExecuteNonQuery();
                                    MessageBox.Show("La valeur a été modifiée", "Succès");

                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.ToString());
                                    Program.log.Error("pointageGrid_CellEndEdit:update" + ex.Message.ToString());
                                }
                                finally
                                {
                                    conn.Close();
                                    Cursor = Cursors.Default;
                                }
                            }
                            else
                            {
                                MessageBox.Show(msg, "Alert");
                                pointageGrid.CurrentCell.Value = oldValue;
                            }
                        }
                    }
                    else if (dialogResult == DialogResult.Cancel)
                    {
                        pointageGrid.CurrentCell.Value = oldValue;
                    }
                }
                else
                {
                    pointageGrid.CurrentCell.Value = oldValue;

                }
            }
           
        }

        private void deleteP_Click(object sender, EventArgs e)
        {
            if (pointageGrid.RowCount > 0)
            {
                bool del = false;
                foreach (DataGridViewRow item in this.pointageGrid.SelectedRows)
                {
                    del = true;
                    DialogResult dialogResult = MessageBox.Show("voulez-vous Supprimer ce pointage?", "Confirmation", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.OK)
                    {
                        string id = pointageGrid.CurrentRow.Cells[0].Value.ToString();

                        OleDbCommand oleDbCmd = new OleDbCommand();
                        System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                        conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                        DataSet ds = new DataSet();
                        Cursor = Cursors.WaitCursor;
                        conn.Open();
                        OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                        string sql = null;
                        sql = "delete * from PointageCal where ID =" + id + "";
                        try
                        {
                            oledbAdapter.DeleteCommand = conn.CreateCommand();
                            oledbAdapter.DeleteCommand.CommandText = sql;
                            oledbAdapter.DeleteCommand.ExecuteNonQuery();
                            MessageBox.Show("Le pointage a été supprimé", "Succès");
                            pointageGrid.Rows.RemoveAt(item.Index);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        finally
                        {
                            conn.Close();
                            Cursor = Cursors.Default;
                        }
                    }
                }
                if(del==false)
                    MessageBox.Show("Veuillez sélectionner toute la ligne à supprimer", "Alert");
            }
        }


        private void editP_Click(object sender, EventArgs e)
        {
            if (pointageGrid.RowCount > 0)
            {
                int cell = pointageGrid.CurrentCell.ColumnIndex;
                if (cell !=9)
                {
                    MessageBox.Show("veuillez sélectionner la valeur de la colonne 'Observation' à modifer!  ", "Alert");

                }
                else

                {
                    pointageGrid.CurrentCell.ReadOnly = false;
                    edit = true;
                    pointageGrid.BeginEdit(true);
                }
            
            }
        }

        private void pointageGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            pointageGrid.Tag = pointageGrid.CurrentCell.Value;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            if((startdate.Text != enddate.Text) && (Convert.ToDateTime(startdate.Text.ToString())> Convert.ToDateTime(enddate.Text.ToString())))
            {
                MessageBox.Show("la date de début doit être inférieure ou égale à la date de fin!! ", "Alert");
                return;
            }
             DictionaryEntry deImgType;
            string ident = "",ident1="";
            
            if (users.SelectedItem != null)
            {
                deImgType = (DictionaryEntry)users.SelectedItem;
                ident = deImgType.Value.ToString();
            }
            if (structure.SelectedItem != null)
            {
                deImgType = (DictionaryEntry)structure.SelectedItem;
                ident1 = deImgType.Value.ToString();
            }
            DataTable dataTable = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                string query = null;
                if (ident != "" && ident1!="")
                    query = "select ID,NomEmp as Nom,PrenomEmp as Prénom,Matricule,Jour,Entrée,Sortie,Struct as Structure,Poste as Emploi,Observation from PointageCal  where  PointageCal.IdUser='" + ident + "' and PointageCal.Struct LIKE '%" + ident1 + "%' and DateValue(Jour) >= DateValue('" + startdate.Text + "') and DateValue(Jour) <= DateValue('" + enddate.Text + "') order by DateValue(Jour) desc, IdUser desc,Entrée asc ";
                else

                if (ident!="")
                 query = "select ID,NomEmp as Nom,PrenomEmp as Prénom,Matricule,Jour,Entrée,Sortie,Struct as Structure,Poste as Emploi,Observation from PointageCal  where  PointageCal.IdUser= '" + ident + "' and DateValue(Jour) >= DateValue('" + startdate.Text + "') and DateValue(Jour) <= DateValue('" + enddate.Text + "') order by DateValue(Jour) desc, IdUser desc,Entrée asc ";

                else
                    if (ident1 != "")
                    query = "select ID,NomEmp as Nom,PrenomEmp as Prénom,Matricule,Jour,Entrée,Sortie,Struct as Structure,Poste as Emploi,Observation from PointageCal  where  PointageCal.Struct LIKE '%" + ident1 + "%' and DateValue(Jour) >= DateValue('" + startdate.Text + "') and DateValue(Jour) <= DateValue('" + enddate.Text + "') order by DateValue(Jour) desc, IdUser desc,Entrée asc ";

                else
                    query = "select ID,NomEmp as Nom,PrenomEmp as Prénom,Matricule,Jour,Entrée,Sortie,Struct as Structure,Poste as Emploi,Observation  from PointageCal where DateValue(Jour) >= DateValue('" + startdate.Text + "') and DateValue(Jour) <= DateValue('" + enddate.Text + "') order by DateValue(Jour) desc, IdUser desc,Entrée asc";
                
                OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    pointageGrid.DataSource = dataTable;
                    pointageGrid.Columns["ID"].Visible = false;
                }
                else
                {
                    pointageGrid.DataSource = null;
                    MessageBox.Show("Aucune informations trouvée", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_search" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }

        }

        public  void get_Users()
        {
           
            this.users.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Identifiant,Nom,Prenom from Employer where IsActif='1'", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                
                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["Identifiant"].ToString();
                    nom = dataTable.Rows[i]["Nom"].ToString()+" "+ dataTable.Rows[i]["Prenom"].ToString();
                    htPointInf.Add(ident, nom);
                    
                }
                if (i > 0)
                {
                   
                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        users.Items.Add(PointInf);
                    }
                    users.DisplayMember = "value";
                    users.ValueMember = "key";
                    users.AutoCompleteMode = AutoCompleteMode.Suggest;
                    users.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_get_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }
        public void get_Struct()
        {

            this.structure.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib from Structures", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();

                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["lib"].ToString();
                    nom = dataTable.Rows[i]["lib"].ToString();
                    htPointInf.Add(ident, nom);

                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        structure.Items.Add(PointInf);
                    }
                    structure.DisplayMember = "value";
                    structure.ValueMember = "key";
                    structure.SelectedIndex = -1;
                    structure.AutoCompleteMode = AutoCompleteMode.Suggest;
                    structure.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_get_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }
        private void SearchRetard_Click(object sender, EventArgs e)
        {
            DateTime time = DateTime.Parse(Tretard.Text),ents;
            string jj = date_ret1.Value.ToShortDateString();
            DataTable dataTable = new DataTable(), dataTable1 = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                string sql1 = null;
                if(TimeSpan.Parse(Tretard.Text) <=TimeSpan.Parse(AttLogsMain.heurS1))
                sql1 = "select Min(Entrée),Jour, IdUser from PointageCal where Entrée<='"+ AttLogsMain.heurS1+ "' and Jour='" + jj + "' group by Jour,IdUser order by Jour";
                else
                    sql1 = "select Min(Entrée),Jour, IdUser from PointageCal where Entrée>'" + AttLogsMain.heurS1 + "' and Jour='" + jj + "' group by Jour,IdUser order by Jour";

                OleDbDataAdapter dAdapter1 = new OleDbDataAdapter(sql1, Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder1 = new OleDbCommandBuilder(dAdapter1);
                dAdapter1.Fill(dataTable1);
                if (dataTable1.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        if (DateTime.TryParse(dataTable1.Rows[i][0].ToString(), out ents) == true)
                        {
                            DateTime ent = DateTime.Parse(dataTable1.Rows[i][0].ToString());
                            string entre = dataTable1.Rows[i][0].ToString();
                            string jour = dataTable1.Rows[i][1].ToString();
                            string user = dataTable1.Rows[i][2].ToString();
                            if (ent > time)
                            {
                                string sql = "select top 1 ID,NomEmp as Nom,PrenomEmp as Prenom,Matricule,Jour,Entrée,Sortie,Struct as Structure,Poste as Emploi,Observation from PointageCal  where Entrée= '" + entre + "' and Jour='" + jour + "' and IdUser='" + user + "'";
                                OleDbDataAdapter dAdapter = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                                dAdapter.Fill(dataTable);

                            }
                        }
                    }
                }
                
                if (dataTable.Rows.Count > 0)
                {
                    pointageGrid.DataSource = dataTable;
                    pointageGrid.Columns["ID"].Visible = false;
                }
                else
                {
                    pointageGrid.DataSource = null;
                    MessageBox.Show("Aucune informations trouvée", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_search retard" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }

        }

        private void SearchAvant_Click(object sender, EventArgs e)
        {
            DateTime time = DateTime.Parse(Tavant.Text),ents;
            string jj = date_ret2.Value.ToShortDateString();
            DataTable dataTable = new DataTable(), dataTable1 = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                string sql = null;
                string sql1 = null;
                // sql = "select ID,NomEmp as Nom,PrenomEmp as Prenom,Matricule,Jour,Entrée,Sortie,Struct as Structure,Poste as Emploi,Observation from PointageCal  where Sortie <'" + time + "'";
                if (TimeSpan.Parse(Tavant.Text) > TimeSpan.Parse(AttLogsMain.heurS1))
                        sql1 = "select Max(Sortie),Jour, IdUser from PointageCal where  Sortie>'"+ AttLogsMain.heurS1+ "' and Jour='" + jj + "' group by Jour,IdUser order by Jour";
                else
                    sql1 = "select Max(Sortie),Jour, IdUser from PointageCal where Sortie<'" + AttLogsMain.heurS1 + "' and Jour='"+jj+"' group by Jour,IdUser order by Jour";

                OleDbDataAdapter dAdapter1 = new OleDbDataAdapter(sql1, Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder1 = new OleDbCommandBuilder(dAdapter1);
                dAdapter1.Fill(dataTable1);
                if (dataTable1.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        if (DateTime.TryParse(dataTable1.Rows[i][0].ToString(), out ents)==true)
                        {
                            DateTime ent = DateTime.Parse(dataTable1.Rows[i][0].ToString());
                            string sorti = dataTable1.Rows[i][0].ToString();
                            string jour = dataTable1.Rows[i][1].ToString();
                            string user = dataTable1.Rows[i][2].ToString();
                            if (ent < time)
                            {
                                sql = "select top 1 ID,NomEmp as Nom,PrenomEmp as Prenom,Matricule,Jour,Entrée,Sortie,Struct as Structure,Poste as Emploi,Observation from PointageCal  where Sortie= '" + sorti + "' and Jour='" + jour + "' and IdUser='" + user + "'";
                                OleDbDataAdapter dAdapter = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                                dAdapter.Fill(dataTable);

                            }
                        }
                    }
                }

                if (dataTable.Rows.Count > 0)
                {
                    pointageGrid.DataSource = dataTable;
                    pointageGrid.Columns["ID"].Visible = false;
                }
                else
                {
                    pointageGrid.DataSource = null;
                    MessageBox.Show("Aucune informations trouvée", "Alert");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_Search avant" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }

        }

        private void Export_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "export.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ToCsV(dataGridView1, @"c:\export.xls");
                ToCsV(pointageGrid, sfd.FileName); // Here dataGridview1 is your grid view name
            }
        }

        private void ToCsV(DataGridView dGV, string filename)
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            // creating new WorkBook within Excel application
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            // creating new Excelsheet in workbook
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            // see the excel sheet behind the program
            app.Visible = true;
            // get the reference of first sheet. By default its name is Sheet1.
            // store its reference to worksheet
            worksheet = (Excel.Worksheet)workbook.Sheets[1];
            //worksheet = workbook.ActiveSheet;
            // changing the name of active sheet
            worksheet.Name = "Liste des pointages";

            // storing header part in Excel
            int k = 1;
            for (int i = 1; i < pointageGrid.Columns.Count; i++)
            {
                worksheet.Cells[1, i] = pointageGrid.Columns[k].HeaderText;
                k++;
            }
             // storing Each row and column value to excel sheet
            
            for (int i = 0; i < pointageGrid.Rows.Count; i++)
            {
                k = 1;
                for (int j = 0; j < pointageGrid.Columns.Count - 1; j++)
                {
                    worksheet.Cells[i + 2, j + 1] =pointageGrid.Rows[i].Cells[k].Value.ToString();
                   
                    k++;
                }
            }
            
            app.DisplayAlerts = false; // Without this you will get two confirm overwrite prompts
            // Save the excel file under the captured location from the SaveFileDialog
            workbook.SaveAs(filename, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            workbook.Close(true, Type.Missing, Type.Missing);
            app.Quit();
            releaseObject(worksheet);
            releaseObject(workbook);
            releaseObject(app);
            // Clear Clipboard and DataGridView selection
            Clipboard.Clear();
            dGV.ClearSelection();
            // Open the newly saved excel file
            if (File.Exists(filename))
                System.Diagnostics.Process.Start(filename);

        }
       
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception lors de génération du fichier excel " + ex.ToString());
                Program.log.Error("pointageGrid_Export Excell" + ex.Message.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void toPdf_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF Documents (*.pdf)|*.pdf";
            sfd.FileName = "liste_des-pointages.pdf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                export_PDF(sfd.FileName); // Here dataGridview1 is your grid view name
            }

        }
        private void export_PDF( string path)
        {
            //Creating iTextSharp Table from the DataTable data
            PdfPTable pdfTable = new PdfPTable(pointageGrid.ColumnCount - 1);
            pdfTable.DefaultCell.PaddingLeft = 3;
            pdfTable.DefaultCell.PaddingRight = 3;
            pdfTable.DefaultCell.PaddingTop =8;
            pdfTable.DefaultCell.PaddingBottom = 8;
            pdfTable.WidthPercentage = 99;
            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable.DefaultCell.BorderWidth = 1;
            pdfTable.SetTotalWidth(new float[] { 80f,80f,50f,50f,50f,50f,150f,150f,100f });
            foreach (DataGridViewColumn column in pointageGrid.Columns)
            {
                if (column.HeaderText != "ID")
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                    cell.FixedHeight = (30);
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    pdfTable.AddCell(cell);
                }
            }

            //Adding DataRow
            foreach (DataGridViewRow row in pointageGrid.Rows)
            {

                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.ColumnIndex != 0)
                        pdfTable.AddCell(cell.Value.ToString());
                }
            }
            using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
            {
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 20f, 20f);
                PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Resources.header, System.Drawing.Imaging.ImageFormat.Png);
                jpg.ScaleToFit(900f,400f);
                jpg.SpacingBefore = 10f;
                jpg.SpacingAfter = 20f;
                jpg.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(jpg);
                var FontColour = new BaseColor(35, 31, 32);
                var Calibri8 = FontFactory.GetFont("Calibri", 20, FontColour);
                Paragraph paragraph = new Paragraph("Liste Des Pointages du "+startdate.Value.ToShortDateString()+" Au "+enddate.Value.ToShortDateString()+" :", Calibri8);
                paragraph.Alignment = Element.ALIGN_LEFT;
                paragraph.SpacingAfter = 20f;
                pdfDoc.Add(paragraph);
                pdfDoc.Add(pdfTable);
                pdfDoc.Close();
                stream.Close();
            }
            if (File.Exists(path))
                System.Diagnostics.Process.Start(path);
            AddPageNumber(path, path);
        }

        void AddPageNumber(string fileIn, string fileOut)
        {
            byte[] bytes = File.ReadAllBytes(fileIn);
            iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString(), blackFont), 568f, 15f, 0);
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(DateTime.Now.ToString(), blackFont), 1165f, 15f, 0);

                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(fileOut, bytes);
        }
    }


}
