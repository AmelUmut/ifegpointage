﻿namespace AttLogs
{
    partial class PersInactif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.searchP = new System.Windows.Forms.Button();
            this.listep = new System.Windows.Forms.ComboBox();
            this.pointGridView = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pointGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "Structure:";
            // 
            // searchP
            // 
            this.searchP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.searchP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchP.Location = new System.Drawing.Point(379, 76);
            this.searchP.Name = "searchP";
            this.searchP.Size = new System.Drawing.Size(116, 23);
            this.searchP.TabIndex = 7;
            this.searchP.Text = "Chercher";
            this.searchP.UseVisualStyleBackColor = true;
            this.searchP.Click += new System.EventHandler(this.searchP_Click);
            // 
            // listep
            // 
            this.listep.FormattingEnabled = true;
            this.listep.Location = new System.Drawing.Point(90, 76);
            this.listep.Name = "listep";
            this.listep.Size = new System.Drawing.Size(259, 21);
            this.listep.TabIndex = 6;
            // 
            // pointGridView
            // 
            this.pointGridView.AllowUserToAddRows = false;
            this.pointGridView.AllowUserToDeleteRows = false;
            this.pointGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pointGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.pointGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.pointGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pointGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pointGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.pointGridView.Location = new System.Drawing.Point(23, 121);
            this.pointGridView.Name = "pointGridView";
            this.pointGridView.Size = new System.Drawing.Size(770, 274);
            this.pointGridView.TabIndex = 9;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(671, 69);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(122, 33);
            this.button2.TabIndex = 10;
            this.button2.Text = "Activer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(669, 401);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 33);
            this.button1.TabIndex = 11;
            this.button1.Text = "Fermer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PersInactif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 443);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pointGridView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchP);
            this.Controls.Add(this.listep);
            this.Name = "PersInactif";
            this.Text = "Personels Inactifs";
            ((System.ComponentModel.ISupportInitialize)(this.pointGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button searchP;
        private System.Windows.Forms.ComboBox listep;
        private System.Windows.Forms.DataGridView pointGridView;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}