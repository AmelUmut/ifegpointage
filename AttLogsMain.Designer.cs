﻿using System.Windows.Forms;

namespace AttLogs
{
    partial class AttLogsMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AttLogsMain));
            this.structures = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.dowP = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.RepprotI = new System.Windows.Forms.Button();
            this.AppUsers = new System.Windows.Forms.Button();
            this.GetUsers = new System.Windows.Forms.Button();
            this.sendTmp = new System.Windows.Forms.Button();
            this.pointage = new System.Windows.Forms.Button();
            this.sendP = new System.Windows.Forms.Button();
            this.listPointeuse = new System.Windows.Forms.Button();
            this.addPoint = new System.Windows.Forms.Button();
            this.GetListeP = new System.Windows.Forms.Button();
            this.param = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnUploadUser = new System.Windows.Forms.Button();
            this.btnGetGeneralLogData = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.logout = new System.Windows.Forms.Button();
            this.changePwd = new System.Windows.Forms.Label();
            this.login = new System.Windows.Forms.Label();
            this.structures.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // structures
            // 
            this.structures.Controls.Add(this.button5);
            this.structures.Controls.Add(this.button4);
            this.structures.Controls.Add(this.dowP);
            this.structures.Controls.Add(this.button3);
            this.structures.Controls.Add(this.label1);
            this.structures.Controls.Add(this.button2);
            this.structures.Controls.Add(this.RepprotI);
            this.structures.Controls.Add(this.AppUsers);
            this.structures.Controls.Add(this.GetUsers);
            this.structures.Controls.Add(this.sendTmp);
            this.structures.Controls.Add(this.pointage);
            this.structures.Controls.Add(this.sendP);
            this.structures.Controls.Add(this.listPointeuse);
            this.structures.Controls.Add(this.addPoint);
            this.structures.Controls.Add(this.GetListeP);
            this.structures.Controls.Add(this.param);
            this.structures.Controls.Add(this.button1);
            this.structures.Controls.Add(this.btnUploadUser);
            this.structures.Controls.Add(this.btnGetGeneralLogData);
            this.structures.ForeColor = System.Drawing.Color.DarkBlue;
            this.structures.Location = new System.Drawing.Point(43, 143);
            this.structures.Name = "structures";
            this.structures.Size = new System.Drawing.Size(1083, 535);
            this.structures.TabIndex = 6;
            this.structures.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.RosyBrown;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.Image = global::AttLogs.Properties.Resources.Numbering_List_icon;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button5.Location = new System.Drawing.Point(877, 385);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(189, 116);
            this.button5.TabIndex = 26;
            this.button5.Text = "Rapport Présence/Retards";
            this.button5.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.RosyBrown;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.Image = global::AttLogs.Properties.Resources.diagram__1_;
            this.button4.Location = new System.Drawing.Point(227, 263);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(208, 116);
            this.button4.TabIndex = 25;
            this.button4.Text = "Structure";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // dowP
            // 
            this.dowP.BackColor = System.Drawing.Color.Tomato;
            this.dowP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dowP.FlatAppearance.BorderSize = 0;
            this.dowP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dowP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dowP.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dowP.Image = global::AttLogs.Properties.Resources.Data_Save_icon;
            this.dowP.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.dowP.Location = new System.Drawing.Point(651, 19);
            this.dowP.Name = "dowP";
            this.dowP.Size = new System.Drawing.Size(200, 116);
            this.dowP.TabIndex = 24;
            this.dowP.Text = "Télécharger Pointages";
            this.dowP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.dowP.UseVisualStyleBackColor = false;
            this.dowP.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.PaleVioletRed;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Image = global::AttLogs.Properties.Resources.Note_icon;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button3.Location = new System.Drawing.Point(877, 263);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(189, 116);
            this.button3.TabIndex = 23;
            this.button3.Text = "Rapport Présence";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1016, 509);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "A Propos";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.SteelBlue;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Image = global::AttLogs.Properties.Resources.File_Copy_2_icon;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.Location = new System.Drawing.Point(877, 141);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(189, 116);
            this.button2.TabIndex = 1;
            this.button2.Text = "Rapport Pointage Général";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // RepprotI
            // 
            this.RepprotI.BackColor = System.Drawing.Color.LightSeaGreen;
            this.RepprotI.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RepprotI.FlatAppearance.BorderSize = 0;
            this.RepprotI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RepprotI.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepprotI.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.RepprotI.Image = global::AttLogs.Properties.Resources.File_Copy_icon;
            this.RepprotI.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.RepprotI.Location = new System.Drawing.Point(877, 19);
            this.RepprotI.Name = "RepprotI";
            this.RepprotI.Size = new System.Drawing.Size(189, 116);
            this.RepprotI.TabIndex = 2;
            this.RepprotI.Text = "Rapport Pointage Individuel";
            this.RepprotI.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.RepprotI.UseVisualStyleBackColor = false;
            this.RepprotI.Click += new System.EventHandler(this.RepprotI_Click);
            // 
            // AppUsers
            // 
            this.AppUsers.BackColor = System.Drawing.Color.IndianRed;
            this.AppUsers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AppUsers.FlatAppearance.BorderSize = 0;
            this.AppUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AppUsers.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppUsers.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.AppUsers.Image = global::AttLogs.Properties.Resources.Lock_User_icon;
            this.AppUsers.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.AppUsers.Location = new System.Drawing.Point(15, 385);
            this.AppUsers.Name = "AppUsers";
            this.AppUsers.Size = new System.Drawing.Size(136, 116);
            this.AppUsers.TabIndex = 11;
            this.AppUsers.Text = "Utilisateurs";
            this.AppUsers.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.AppUsers.UseVisualStyleBackColor = false;
            this.AppUsers.Click += new System.EventHandler(this.AppUsers_Click);
            // 
            // GetUsers
            // 
            this.GetUsers.BackColor = System.Drawing.Color.Tan;
            this.GetUsers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GetUsers.FlatAppearance.BorderSize = 0;
            this.GetUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GetUsers.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetUsers.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.GetUsers.Image = global::AttLogs.Properties.Resources.Business_Todo_List_icon;
            this.GetUsers.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.GetUsers.Location = new System.Drawing.Point(227, 19);
            this.GetUsers.Name = "GetUsers";
            this.GetUsers.Size = new System.Drawing.Size(208, 116);
            this.GetUsers.TabIndex = 20;
            this.GetUsers.Text = "Afficher Personnel";
            this.GetUsers.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.GetUsers.UseVisualStyleBackColor = false;
            this.GetUsers.Click += new System.EventHandler(this.GetUsers_Click);
            // 
            // sendTmp
            // 
            this.sendTmp.BackColor = System.Drawing.Color.Pink;
            this.sendTmp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendTmp.FlatAppearance.BorderSize = 0;
            this.sendTmp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendTmp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendTmp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.sendTmp.Image = global::AttLogs.Properties.Resources.Finger_Print_icon;
            this.sendTmp.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.sendTmp.Location = new System.Drawing.Point(227, 141);
            this.sendTmp.Name = "sendTmp";
            this.sendTmp.Size = new System.Drawing.Size(208, 116);
            this.sendTmp.TabIndex = 4;
            this.sendTmp.Text = "Synchroniser Empreintes";
            this.sendTmp.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.sendTmp.UseVisualStyleBackColor = false;
            this.sendTmp.Click += new System.EventHandler(this.sendTmp_Click);
            // 
            // pointage
            // 
            this.pointage.BackColor = System.Drawing.Color.Khaki;
            this.pointage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pointage.FlatAppearance.BorderSize = 0;
            this.pointage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pointage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pointage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pointage.Image = global::AttLogs.Properties.Resources.Time_Timezone_icon;
            this.pointage.Location = new System.Drawing.Point(459, 386);
            this.pointage.Name = "pointage";
            this.pointage.Size = new System.Drawing.Size(392, 116);
            this.pointage.TabIndex = 3;
            this.pointage.Text = "Liste Présence";
            this.pointage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.pointage.UseVisualStyleBackColor = false;
            this.pointage.Click += new System.EventHandler(this.pointage_Click);
            // 
            // sendP
            // 
            this.sendP.BackColor = System.Drawing.Color.LightSkyBlue;
            this.sendP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendP.FlatAppearance.BorderSize = 0;
            this.sendP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendP.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.sendP.Image = global::AttLogs.Properties.Resources.Data_Save_icon;
            this.sendP.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.sendP.Location = new System.Drawing.Point(459, 141);
            this.sendP.Name = "sendP";
            this.sendP.Size = new System.Drawing.Size(392, 116);
            this.sendP.TabIndex = 17;
            this.sendP.Text = "Traitement Pointages";
            this.sendP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.sendP.UseVisualStyleBackColor = false;
            this.sendP.Click += new System.EventHandler(this.sendP_Click);
            // 
            // listPointeuse
            // 
            this.listPointeuse.BackColor = System.Drawing.Color.OliveDrab;
            this.listPointeuse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listPointeuse.FlatAppearance.BorderSize = 0;
            this.listPointeuse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.listPointeuse.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listPointeuse.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listPointeuse.Image = global::AttLogs.Properties.Resources.User_Interface_Checklist_icon;
            this.listPointeuse.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.listPointeuse.Location = new System.Drawing.Point(157, 385);
            this.listPointeuse.Name = "listPointeuse";
            this.listPointeuse.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.listPointeuse.Size = new System.Drawing.Size(136, 116);
            this.listPointeuse.TabIndex = 16;
            this.listPointeuse.Text = "Liste Pointeuses";
            this.listPointeuse.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.listPointeuse.UseVisualStyleBackColor = false;
            this.listPointeuse.Click += new System.EventHandler(this.listPointeuse_Click);
            // 
            // addPoint
            // 
            this.addPoint.BackColor = System.Drawing.Color.SteelBlue;
            this.addPoint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addPoint.FlatAppearance.BorderSize = 0;
            this.addPoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addPoint.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPoint.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.addPoint.Image = global::AttLogs.Properties.Resources.Add_Window_icon;
            this.addPoint.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.addPoint.Location = new System.Drawing.Point(299, 385);
            this.addPoint.Name = "addPoint";
            this.addPoint.Size = new System.Drawing.Size(136, 116);
            this.addPoint.TabIndex = 15;
            this.addPoint.Text = "Ajouter Pointeuse";
            this.addPoint.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.addPoint.UseVisualStyleBackColor = false;
            this.addPoint.Click += new System.EventHandler(this.addPoint_Click);
            // 
            // GetListeP
            // 
            this.GetListeP.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.GetListeP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GetListeP.FlatAppearance.BorderSize = 0;
            this.GetListeP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GetListeP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetListeP.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.GetListeP.Image = global::AttLogs.Properties.Resources.Food_List_Ingredients_icon;
            this.GetListeP.Location = new System.Drawing.Point(459, 263);
            this.GetListeP.Name = "GetListeP";
            this.GetListeP.Size = new System.Drawing.Size(392, 116);
            this.GetListeP.TabIndex = 0;
            this.GetListeP.Text = "Liste des Pointages";
            this.GetListeP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.GetListeP.UseVisualStyleBackColor = false;
            this.GetListeP.Click += new System.EventHandler(this.GetListeP_Click);
            // 
            // param
            // 
            this.param.BackColor = System.Drawing.Color.SlateGray;
            this.param.Cursor = System.Windows.Forms.Cursors.Hand;
            this.param.FlatAppearance.BorderSize = 0;
            this.param.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.param.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.param.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.param.Image = global::AttLogs.Properties.Resources.Time_And_Date_Timer_icon;
            this.param.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.param.Location = new System.Drawing.Point(15, 263);
            this.param.Name = "param";
            this.param.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.param.Size = new System.Drawing.Size(206, 116);
            this.param.TabIndex = 14;
            this.param.Text = "Emploi du Temps";
            this.param.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.param.UseVisualStyleBackColor = false;
            this.param.Click += new System.EventHandler(this.param_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Teal;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Image = global::AttLogs.Properties.Resources.Users_Group;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(15, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 116);
            this.button1.TabIndex = 13;
            this.button1.Text = "Ajouter Liste Personnel";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.UploadtUserList_Click);
            // 
            // btnUploadUser
            // 
            this.btnUploadUser.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.btnUploadUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUploadUser.FlatAppearance.BorderSize = 0;
            this.btnUploadUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUploadUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadUser.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnUploadUser.Image = global::AttLogs.Properties.Resources.Users;
            this.btnUploadUser.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnUploadUser.Location = new System.Drawing.Point(15, 19);
            this.btnUploadUser.Name = "btnUploadUser";
            this.btnUploadUser.Size = new System.Drawing.Size(206, 116);
            this.btnUploadUser.TabIndex = 12;
            this.btnUploadUser.Text = "Ajouter Employé";
            this.btnUploadUser.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnUploadUser.UseVisualStyleBackColor = false;
            this.btnUploadUser.Click += new System.EventHandler(this.btnUploadUser_Click);
            // 
            // btnGetGeneralLogData
            // 
            this.btnGetGeneralLogData.BackColor = System.Drawing.Color.PeachPuff;
            this.btnGetGeneralLogData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGetGeneralLogData.FlatAppearance.BorderSize = 0;
            this.btnGetGeneralLogData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetGeneralLogData.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetGeneralLogData.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnGetGeneralLogData.Image = global::AttLogs.Properties.Resources.Over_Time_2_icon;
            this.btnGetGeneralLogData.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGetGeneralLogData.Location = new System.Drawing.Point(459, 19);
            this.btnGetGeneralLogData.Name = "btnGetGeneralLogData";
            this.btnGetGeneralLogData.Size = new System.Drawing.Size(186, 116);
            this.btnGetGeneralLogData.TabIndex = 1;
            this.btnGetGeneralLogData.Text = "Afficher Pointages";
            this.btnGetGeneralLogData.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGetGeneralLogData.UseVisualStyleBackColor = false;
            this.btnGetGeneralLogData.Click += new System.EventHandler(this.btnGetGeneralLogData_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox2.Location = new System.Drawing.Point(43, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1083, 98);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pointeuses";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(18, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1048, 75);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AttLogs.Properties.Resources.icon;
            this.pictureBox1.Location = new System.Drawing.Point(985, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 34);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // logout
            // 
            this.logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logout.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logout.Location = new System.Drawing.Point(1003, 24);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(123, 26);
            this.logout.TabIndex = 9;
            this.logout.Text = "Se Déconnecter";
            this.logout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // changePwd
            // 
            this.changePwd.AutoSize = true;
            this.changePwd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changePwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changePwd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.changePwd.Location = new System.Drawing.Point(868, 29);
            this.changePwd.Name = "changePwd";
            this.changePwd.Size = new System.Drawing.Size(124, 13);
            this.changePwd.TabIndex = 10;
            this.changePwd.Text = "Changer le mot de passe";
            this.changePwd.Click += new System.EventHandler(this.changePwd_Click);
            // 
            // login
            // 
            this.login.AutoSize = true;
            this.login.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.login.Location = new System.Drawing.Point(728, 31);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(41, 13);
            this.login.TabIndex = 11;
            this.login.Text = "label1";
            // 
            // AttLogsMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1150, 680);
            this.Controls.Add(this.login);
            this.Controls.Add(this.changePwd);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.structures);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AttLogsMain";
            this.Resizable = false;
            this.Text = " ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_FormClosing);
            this.structures.ResumeLayout(false);
            this.structures.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GroupBox structures;
        private Button btnGetGeneralLogData;
        private Button btnUploadUser;
        private Button button1;
        private Button param;
        private Button addPoint;
        private Button listPointeuse;
        private Button sendP;
        private Button GetUsers;
        private Button GetListeP;
        private Button logout;
        private Button button2;
        private Button RepprotI;
        public GroupBox groupBox2;
        private Panel panel1;
        private Button pointage;
        private Button sendTmp;
        private Label changePwd;
        private Button AppUsers;
        private PictureBox pictureBox1;
        private Label login;
        private Label label1;
        private Button button3;
        private Button dowP;
        private Button button4;
        private Button button5;
    }
}

