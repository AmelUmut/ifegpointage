﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.OleDb;
using AttLogs.Properties;

namespace AttLogs
{
    public partial class Structure : MetroForm
    {
        public Structure()
        {
            InitializeComponent();
            DisplayData();
            foreach (DataGridViewRow row in pointGridView.Rows)
            {
                row.ReadOnly = true;
            }
        }

        public static void DisplayData()
        {
            Cursor.Current = Cursors.WaitCursor;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib as Structure from Structures  order by lib", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                pointGridView.DataSource = dataTable;
                pointGridView.Columns["ID"].Visible = false;

                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("UsersList" + ex.Message.ToString());
                Cursor.Current = Cursors.Default;

            }
            finally
            {

                conn.Close();
                Cursor.Current = Cursors.Default;

            }
        }
        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        private void update_Click(object sender, EventArgs e)
        {
            if (pointGridView.RowCount > 0)
            {
                int cell = pointGridView.CurrentCell.ColumnIndex;

                pointGridView.CurrentCell.ReadOnly = false;
                
                pointGridView.BeginEdit(true);
                

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var myForm = new AddStruct();
            myForm.Show();

        }

        private void pointGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            pointGridView.Tag = pointGridView.CurrentCell.Value;
        }

        private void pointGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var oldValue = pointGridView.Tag;
            string newValue = pointGridView.CurrentCell.Value.ToString();
            if ((oldValue.ToString() != newValue) && (newValue != ""))
            {
                int columnIndex = pointGridView.CurrentCell.ColumnIndex;
                string columnName = pointGridView.Columns[columnIndex].Name;
                DialogResult dialogResult = MessageBox.Show("vous avez changé le nom de la structure,voulez-vous l'enregistrer?", "Confirmation", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    

                        string id = pointGridView.CurrentRow.Cells[0].Value.ToString();
                        
                          
                            OleDbCommand oleDbCmd = new OleDbCommand();
                            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                            DataSet ds = new DataSet();
                            Cursor = Cursors.WaitCursor;
                            conn.Open();
                            OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                            string sql = null,sql2=null;
                           
                                sql = "update Structures set lib= '" + newValue + "' where ID =" + id + "";
                                sql2 = "update Employer set LibSP= '" + newValue + "' where LibSP ='" + oldValue + "'";

                            try
                            {
                                oledbAdapter.DeleteCommand = conn.CreateCommand();
                                oledbAdapter.DeleteCommand.CommandText = sql;
                                oledbAdapter.DeleteCommand.ExecuteNonQuery();
                                oledbAdapter.DeleteCommand = conn.CreateCommand();
                                oledbAdapter.DeleteCommand.CommandText = sql2;
                                oledbAdapter.DeleteCommand.ExecuteNonQuery();
                                MessageBox.Show("La valeur a été modifiée", "Succès");

                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.ToString());
                                Program.log.Error("pointageGrid_CellEndEdit:update" + ex.Message.ToString());
                            }
                            finally
                            {
                                conn.Close();
                                Cursor = Cursors.Default;
                            }
                        
                       
                    }

                else if (dialogResult == DialogResult.Cancel)
                {
                    pointGridView.CurrentCell.Value = oldValue;
                }
            }
            else
            {
                pointGridView.CurrentCell.Value = oldValue;

            }
        }

    }
}

