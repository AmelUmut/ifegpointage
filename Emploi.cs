﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.OleDb;
using AttLogs.Properties;

namespace AttLogs
{
    public partial class Emploi : MetroForm
    {
        public Emploi()
        {
            InitializeComponent();
            DisplayData();
            foreach (DataGridViewRow row in pointGridView.Rows)
            {
                row.ReadOnly = true;
            }
        }

        public static void DisplayData()
        {
            Cursor.Current = Cursors.WaitCursor;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select Postes.ID,lib as Structure,Titre from Postes inner join Structures on Postes.Structure=Structures.ID  order by Structure,Titre", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable= new DataTable();
                DataTable dataTable1 = new DataTable();
                dAdapter.Fill(dataTable);
                pointGridView.DataSource = dataTable;
                pointGridView.Columns["ID"].Visible = false;

                dAdapter = new OleDbDataAdapter("select lib from Structures order by lib", Settings.Default.PointeuseDBConnectionString);
                cBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.Fill(dataTable1);
                IList<string> lstFirst = new List<string>();
                foreach (DataRow row in dataTable1.Rows)
                {
                    lstFirst.Add(row.Field<string>("lib"));

                }
                listep.Items.AddRange(lstFirst.ToArray<string>());
                listep.AutoCompleteMode = AutoCompleteMode.Suggest;
                listep.AutoCompleteSource = AutoCompleteSource.ListItems;



            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("UsersList" + ex.Message.ToString());
                Cursor.Current = Cursors.Default;

            }
            finally
            {

                conn.Close();
                Cursor.Current = Cursors.Default;

            }
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void delete_Click(object sender, EventArgs e)
        {

        }

        private void update_Click(object sender, EventArgs e)
        {

        }

        private void add_Click(object sender, EventArgs e)
        {
            var myForm = new AddEmp();
            myForm.Show();
        }

        private void searchP_Click(object sender, EventArgs e)
        {

            string ident = "";
            if (listep.SelectedItem != null)
            {
                ident = listep.SelectedItem.ToString();
            }
            BindingSource bs = new BindingSource();
            bs.DataSource = pointGridView.DataSource;
            bs.Filter = pointGridView.Columns[1].HeaderText.ToString() + " LIKE '%" + ident + "%'";
            pointGridView.DataSource = bs;
        }
    }
}
