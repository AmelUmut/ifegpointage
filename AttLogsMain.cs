﻿/**********************************************************
 * Demo for Standalone SDK.Created by Darcy on Oct.15 2009*
***********************************************************/
using AttLogs.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class AttLogsMain : MetroForm
    {
        public static string heurE1 = "", heurE2 = "", heurS1 = "", heurS2 = "",heurE0="",heurS3="", pointeuse = "";
        public static List<Label> CheckBoxes = new List<Label>();
        public static List<Label> Labels = new List<Label>();
        public static List<Button> Buttons = new List<Button>();
        public static int inedxP = -1;
        public static string serial1 = "ODZ7030597030900002", serial2 = "ODZ7030597030900030";
        public static bool verrou = false;
         
        public AttLogsMain()
        {
            CheckBoxes = new List<Label>();
            Labels = new List<Label>();
            Buttons = new List<Button>();
            inedxP = -1;

            InitializeComponent();
           
                structures.Visible = true;
                login.Text = Login.usern;
                if (Login.idPro == "3")
                {
                    btnUploadUser.Enabled = false;
                    GetUsers.Enabled = false;
                    addPoint.Enabled = false;
                    button1.Enabled = false;
                    sendTmp.Enabled = false;
                    listPointeuse.Enabled = false;
                    AppUsers.Enabled = false;
                    button4.Enabled = false;
                }
                else
                    if (Login.idPro == "2")
                {
                    btnUploadUser.Enabled = false;
                    button1.Enabled = false;
                    GetUsers.Enabled = false;
                    dowP.Enabled = false;
                    sendP.Enabled = false;
                    button4.Enabled = false;
                    param.Enabled = false;
                    sendTmp.Enabled = false;
                    GetListeP.Enabled = false;
                    pointage.Enabled = false;
                    RepprotI.Enabled = false;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    button5.Enabled = false;
                    
                    }
                else
                    if (Login.idPro == "4")
                {
                    addPoint.Enabled = false;
                    AppUsers.Enabled = false;
                    listPointeuse.Enabled = false;
                }
                Cursor = Cursors.WaitCursor;
                get_param();
                get_pointeuse();
                Cursor = Cursors.Default;
                this.FormClosing += Form_FormClosing;
            }
            
        
        void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }


        //Create Standalone SDK class dynamicly.

        public static List<zkemkeeper.CZKEMClass> axCZKEM = new List<zkemkeeper.CZKEMClass>();
        public static List<bool> IsConnected = new List<bool>();


        /********************************************************************************************************************************************
        * Before you refer to this demo,we strongly suggest you read the development manual deeply first.                                           *
        * This part is for demonstrating the communication with your device.There are 3 communication ways: "TCP/IP","Serial Port" and "USB Client".*
        * The communication way which you can use duing to the model of the device.                                                                 *
        * *******************************************************************************************************************************************/
        #region Communication
        //  public static bool bIsConnected = false;//the boolean value identifies whether the device is connected
        public static int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.

        //If your device supports the TCP/IP communications, you can refer to this.
        //when you are using the tcp/ip communication,you can distinguish different devices by their IP address.
        private void btnConnect_Click(object sender, EventArgs e)
        {

            int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;
            int i = Convert.ToInt32(((Button)sender).Tag);

            if (((Button)sender).Text == "Déconnecter")
            {


                axCZKEM[i].Disconnect();
                IsConnected[i] = false;
                ((Button)sender).Text = "Connecter";
                Labels[i].Text = "";
                Labels[i].Text = CheckBoxes[i].Text + " est déconnectée";
                Labels[i].ForeColor = Color.Red;
                Cursor = Cursors.Default;
                return;

            }


            string deImgType = CheckBoxes[i].Tag.ToString();
            string[] infs = deImgType.Split('/');
            IsConnected[i] = axCZKEM[i].Connect_Net(infs[0], Convert.ToInt32(infs[1]));
            // bIsConnected =true;
            if (IsConnected[i] == true)
            {
                
            /*******************************************************************************************/
                 //Get the device's serial number
      
            string sdwSerialNumber = "";
              //  int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;
            if (axCZKEM[i].GetSerialNumber(iMachineNumber,out sdwSerialNumber))
            {
                    if (sdwSerialNumber == serial1 ||  sdwSerialNumber == serial2)
                    {
                        //  MessageBox.Show("Le numéro de série de la pointeuse" + i.ToString() + "est vérifié");
                        verrou = false;
                    }
                    else
                    {
                        MessageBox.Show("La licence est invalide, cette application va se fermer");
                        verrou = true;
                        this.Close();
                    }
            }
            else
            {
                    //   axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show("La vérification de la licence a échoué, veuillez redémarrer l'application");
            }
            Cursor = Cursors.Default;
            /***********************************************************************************************/
                
                Labels[i].Text = "";
                ((Button)sender).Text = "Déconnecter";
                ((Button)sender).Refresh();
                Labels[i].Text = CheckBoxes[i].Text.ToString() + ": " + infs[0] + " est connectée";
                pointeuse = infs[0];
                Labels[i].ForeColor = Color.Green;
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                axCZKEM[i].RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM[i].GetLastError(ref idwErrorCode);
                MessageBox.Show("Impossible de se connecter à la pointeuse,ErrorCode=" + idwErrorCode.ToString(), "Error");

            }

            Cursor = Cursors.Default;
        }
        /*
        //If your device supports the SerialPort communications, you can refer to this.
        private void btnRsConnect_Click(object sender, EventArgs e)
        {
            if (cbPort.Text.Trim() == "" || cbBaudRate.Text.Trim() == "" || txtMachineSN.Text.Trim() == "")
            {
                MessageBox.Show("Port,BaudRate and MachineSN cannot be null", "Error");
                return;
            }
            int idwErrorCode = 0;
            //accept serialport number from string like "COMi"
            int iPort;
            string sPort = cbPort.Text.Trim();
            for (iPort = 1; iPort < 10; iPort++)
            {
                if (sPort.IndexOf(iPort.ToString()) > -1)
                {
                    break;
                }
            }

            Cursor = Cursors.WaitCursor;
            if (btnRsConnect.Text == "Disconnect")
            {
                axCZKEM1.Disconnect();
                bIsConnected = false;
                btnRsConnect.Text = "Connect";
                btnRsConnect.Refresh();
                lblState.Text = "Current State:Disconnected";
                Cursor = Cursors.Default;
                return;
            }

            iMachineNumber = Convert.ToInt32(txtMachineSN.Text.Trim());//when you are using the serial port communication,you can distinguish different devices by their serial port number.
            bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, Convert.ToInt32(cbBaudRate.Text.Trim()));

            if (bIsConnected == true)
            {
                btnRsConnect.Text = "Disconnect";
                btnRsConnect.Refresh();
                lblState.Text = "Current State:Connected";

                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }

            Cursor = Cursors.Default;
        }

        //If your device supports the USBCLient, you can refer to this.
        //Not all series devices can support this kind of connection.Please make sure your device supports USBClient.
        //Connect the device via the virtual serial port created by USBClient
        private void btnUSBConnect_Click(object sender, EventArgs e)
        {
            int idwErrorCode = 0;

            Cursor = Cursors.WaitCursor;

            if (btnUSBConnect.Text == "Disconnect")
            {
                axCZKEM1.Disconnect();
                bIsConnected = false;
                btnUSBConnect.Text = "Connect";
                btnUSBConnect.Refresh();
                lblState.Text = "Current State:Disconnected";
                Cursor = Cursors.Default;
                return;
            }

            SearchforUSBCom usbcom = new SearchforUSBCom();
            string sCom = "";
            bool bSearch = usbcom.SearchforCom(ref sCom);//modify by Darcy on Nov.26 2009
            if (bSearch == false)//modify by Darcy on Nov.26 2009
            {
                MessageBox.Show("Can not find the virtual serial port that can be used", "Error");
                Cursor = Cursors.Default;
                return;
            }

            int iPort;
            for (iPort = 1; iPort < 10; iPort++)
            {
                if (sCom.IndexOf(iPort.ToString()) > -1)
                {
                    break;
                }
            }

            iMachineNumber = Convert.ToInt32(txtMachineSN2.Text.Trim());
            if (iMachineNumber == 0 || iMachineNumber > 255)
            {
                MessageBox.Show("The Machine Number is invalid!", "Error");
                Cursor = Cursors.Default;
                return;
            }

            int iBaudRate = 115200;//115200 is one possible baudrate value(its value cannot be 0)
            bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, iBaudRate);

            if (bIsConnected == true)
            {
                btnUSBConnect.Text = "Disconnect";
                btnUSBConnect.Refresh();
                lblState.Text = "Current State:Connected";
                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }

            Cursor = Cursors.Default;
        }
        */
        #endregion

        /*************************************************************************************************
        * Before you refer to this demo,we strongly suggest you read the development manual deeply first.*
        * This part is for demonstrating operations with(read/get/clear) the attendance records.         *
        * ************************************************************************************************/
        #region AttLogs

        //Download the attendance records from the device(For both Black&White and TFT screen devices).
        private void btnGetGeneralLogData_Click(object sender, EventArgs e)
        {
            var myForm = new AttLog();
            myForm.Show();
        }

        //Clear all attendance records from terminal (For both Black&White and TFT screen devices).
        public static void ClearGLog(zkemkeeper.CZKEMClass point,string name)
        {
            
            int idwErrorCode = 0;

            //for (int i = 0; i < axCZKEM.Count; i++)
            //{
            //    if (IsConnected[i] == true)
            //    {
                    point.EnableDevice(iMachineNumber, false);//disable the device
                    if (point.ClearGLog(iMachineNumber))
                    {
                        point.RefreshData(iMachineNumber);//the data in the device should be refreshed
                        //MessageBox.Show(name + ":Les pointages ont été bien supprimés de la pointeuse !", "Success");
                    }
                    else
                    {
                        point.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Echec,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    point.EnableDevice(iMachineNumber, true);//enable the device
            //    }
            //}
        }


        //Get the count of attendance records in from ternimal(For both Black&White and TFT screen devices).
        private void btnGetDeviceStatus_Click(object sender, EventArgs e)
        {
            int iValue = 0;
            int k = 0;
            bool bIsConnected = false;
            for (int i = 0; i < IsConnected.Count; i++)
            {
                if (IsConnected[i] == true)
                    bIsConnected = true;
            }
            if (bIsConnected == false)
            {
                MessageBox.Show("Aucune pointeuse n'est Connectée!", "Error");
                return;
            }

            for (int i = 0; i < axCZKEM.Count; i++)
            {
                if (IsConnected[i] == true)
                {

                    int idwErrorCode = 0;


                    axCZKEM[i].EnableDevice(iMachineNumber, false);//disable the device
                    if (axCZKEM[i].GetDeviceStatus(iMachineNumber, 6, ref iValue)) //Here we use the function "GetDeviceStatus" to get the record's count.The parameter "Status" is 6.
                    {
                        //MessageBox.Show("The count of the AttLogs in the device is " + iValue.ToString(), "Success");
                        k = k + iValue;
                    }
                    else
                    {
                        axCZKEM[i].GetLastError(ref idwErrorCode);
                        MessageBox.Show("Echec,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    axCZKEM[i].EnableDevice(iMachineNumber, true);//enable the device
                }
            }
            MessageBox.Show("Nombre de pointages enrigistré dans les pointeuses connectées est " + k.ToString(), "Success");
        }

        //Upload Users
        private void btnUploadUser_Click(object sender, EventArgs e)
        {

            if (IsConnected.Count == 0)
            {
                MessageBox.Show("l'application ne contient aucune pointeuse,veuillez ajouter au moins une pointeuse!", "Error");
                return;
            }
            if (inedxP == -1)
            {
                MessageBox.Show("veuillez indiquer la pointeuse principale dans la liste des pointeuses!", "Error");
                return;
            }

            if (IsConnected[inedxP] == false)
            {
                MessageBox.Show("la pointeuse principale n'est pas Connectées!", "Error");
                return;
            }
            string id =AttLogsMain.LastId(AttLogsMain.axCZKEM[AttLogsMain.inedxP]).ToString();
            var myForm = new UploadUserForm();
            myForm.Show();
            System.Windows.Forms.Form f = System.Windows.Forms.Application.OpenForms["UploadUserForm"];
            ((UploadUserForm)f).ident.Text = id;
            if (Login.idPro!= "1")
                ((UploadUserForm)f).ident.Enabled=false;
        }

        private void UploadtUserList_Click(object sender, EventArgs e)
        {
            if (IsConnected.Count == 0)
            {
                MessageBox.Show("l'application ne contient aucune pointeuse,veuillez ajouter au moins une pointeuse!", "Error");
                return;
            }
            if (inedxP == -1)
            {
                MessageBox.Show("veuillez indiquer la pointeuse principale dans la liste des pointeuses!", "Error");
                return;
            }

            if (IsConnected[inedxP] == false)
            {
                MessageBox.Show("la pointeuse principale n'est pas Connectées!", "Error");
                return;
            }
            var myForm = new UploadCSVFile();
            myForm.Show();

        }

        private void listPointeuse_Click(object sender, EventArgs e)
        {
            var myForm = new ListePForm();
            myForm.Show();
        }

        private void sendP_Click(object sender, EventArgs e)
        {
            if (AttLogsMain.heurE2 == "")
            {
                MessageBox.Show("Faut définir l'emploi du temps dans les paramétres", "Erreur");
                return;
            }
            Cursor = Cursors.WaitCursor;
            DateTime Idpoint = AttLogsMain.get_LastPoint();
            DateTime min = get_MinPoint(Idpoint);
            
            if(Idpoint.Date==min.Date)
            {
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                
               
                    conn.Open();
                    OleDbDataAdapter dAdapter = new OleDbDataAdapter(), dAdapter1 = new OleDbDataAdapter();
                    string sql = null,sql2=null,sql3=null;
                    sql = "delete * from Temp where DateValue(Jour)= DateValue('" + Idpoint.Date + "')";
                    sql2 = "delete * from PointageCal where DateValue(Jour)= DateValue('" + Idpoint.Date + "')";
                    sql3 = "delete * from Presence where DateValue(Jour)= DateValue('" + Idpoint.Date + "')";

                    try
                    {
                        dAdapter.DeleteCommand = conn.CreateCommand();
                        dAdapter.DeleteCommand.CommandText = sql;
                        dAdapter.DeleteCommand.ExecuteNonQuery();
                        dAdapter1.DeleteCommand = conn.CreateCommand();
                        dAdapter1.DeleteCommand.CommandText = sql2;
                        dAdapter1.DeleteCommand.ExecuteNonQuery();
                        dAdapter1.DeleteCommand = conn.CreateCommand();
                        dAdapter1.DeleteCommand.CommandText = sql3;
                        dAdapter1.DeleteCommand.ExecuteNonQuery();
                }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        Program.log.Error("delete from pointagecal" + ex.Message.ToString());
                    }
                conn.Close();
                SavePointages.insertnew(Idpoint.Date);
                

            }
            else
            SavePointages.insertnew(Idpoint);
            //CalPointage
            DateTime max = get_LastCalPoint();
            ListePointages.DisplayData(max);
            //Presence
            Cursor = Cursors.Default;
            MessageBox.Show("Traitement des pointages terminé avec succès","Succes");

        }

        private void GetUsers_Click(object sender, EventArgs e)
        {
            var myForm = new UsersList();
            myForm.Show();

        }

        void logout_Click(object sender, EventArgs e)
        {
            //this.FormClosing += Form_FormClosing;
            var login = new Login();
            this.Hide();
            login.Show();
        }

        private void addPoint_Click(object sender, EventArgs e)
        {
            var myForm = new AddPForm();
            myForm.Show();
        }

        private void GetListeP_Click(object sender, EventArgs e)
        {
            var myForm = new ListePointages();
            myForm.Show();
        }

        private void param_Click(object sender, EventArgs e)
        {
            var myForm = new ParamForm();
            if (heurE1 != "")
            {
                myForm.heureE1.Text = heurE1;
                myForm.heureE2.Text = heurE2;
                myForm.heureS1.Text = heurS1;
                myForm.heureS2.Text = heurS2;
               // myForm.heureS3.Text = heurS3;
               // myForm.heureE0.Text = heurE0;
            }
            myForm.Show();
        }

        public static int LastId(zkemkeeper.CZKEMClass axCZKEM1)
        {
            int size = 0;
            string sdwEnrollNumber = "";
            string sName = "";
            string sPassword = "";
            int iPrivilege = 0;
            bool bEnabled = false;
            axCZKEM1.ReadAllUserID(AttLogsMain.iMachineNumber);//read all the users
            while (axCZKEM1.SSR_GetAllUserInfo(AttLogsMain.iMachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
            {
                size = int.Parse(sdwEnrollNumber);
            }
            size++;
            return size;

        }

        //Charger les paramétres
        public static void get_param()
        {

            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select * from Parametrage", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    heurE1 = dataTable.Rows[i][1].ToString();
                    heurE2 = dataTable.Rows[i][2].ToString();
                    heurS1 = dataTable.Rows[i][3].ToString();
                    heurS2 = dataTable.Rows[i][4].ToString();
                    heurE0 = dataTable.Rows[i][5].ToString();
                    heurS3 = dataTable.Rows[i][6].ToString();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {

                conn.Close();
            }
        }
        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var myForm = new GeneralReport();
            myForm.Show();
        }

        private void RepprotI_Click(object sender, EventArgs e)
        {
            var myForm = new IndivRepport();
            myForm.Show();
        }

        private void pointage_Click(object sender, EventArgs e)
        {
            var myForm = new Pointages();
            myForm.Show();
        }

        private void sendTmp_Click(object sender, EventArgs e)
        {
            var myForm = new UsersTmp();
            myForm.Show();
        }

        private void changePwd_Click(object sender, EventArgs e)
        {
            var myForm = new ChangePwd();
            myForm.Show();
        }

        private void AppUsers_Click(object sender, EventArgs e)
        {
            var myForm = new Utilisateurs();
            myForm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var myForm = new PresentRepport();
            myForm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var myForm = new SavePointages();
            myForm.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            var myForm = new AboutUs();
            myForm.Show();
        }

        public List<Label> get_pointeuse()
        {

            CheckBoxes.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select * from Pointeuse order by ID", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                string ip, port, nom,princ;
                Hashtable htPointInf = new Hashtable();
                int i;

                Label box;
                Button butt;
                Label lab;
                bool bIsConnected;
                zkemkeeper.CZKEMClass axCZKEM1;
                int intialTop = 10;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    axCZKEM1 = new zkemkeeper.CZKEMClass();
                    axCZKEM.Add(axCZKEM1);
                    bIsConnected = false;
                    IsConnected.Add(bIsConnected);
                    box = new Label();
                    ip = dataTable.Rows[i]["IP"].ToString();
                    port = dataTable.Rows[i]["Port"].ToString();
                    nom = dataTable.Rows[i]["Nom"].ToString();
                    princ = dataTable.Rows[i]["IsPrincipal"].ToString();
                    box.Tag = ip + "/" + port + "/" + princ;
                    if (princ == "1")
                    {
                        box.Text = nom + ("(Pointeuse Principale)");
                        inedxP = i;
                    }
                    else
                        box.Text = nom;
                    box.Width = 240;
                    box.Top = intialTop + 3;
                    box.Left = 20;
                    CheckBoxes.Add(box);
                    butt = new Button();
                    butt.Text = "Connecter";
                    butt.Tag = i;
                    butt.Click += new EventHandler(btnConnect_Click);
                    butt.Top = intialTop;
                    butt.Left = box.Width + 20;
                    butt.Width = 100;
                    lab = new Label();
                    lab.Text = nom + " est déconnectée";
                    lab.ForeColor = Color.Red;
                    lab.AutoSize = true;
                    lab.Top = intialTop + 3;
                    lab.Left = 30 + box.Width + butt.Width;

                    Labels.Add(lab);
                    Buttons.Add(butt);
                    panel1.Controls.Add(butt);
                    panel1.Controls.Add(box);
                    panel1.Controls.Add(lab);
                    intialTop = intialTop + butt.Height + 10;

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {

                conn.Close();
            }
            return CheckBoxes;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            var myForm = new Structure();
            myForm.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var myForm = new RetardRepport();
            myForm.Show();
        }

        private void emploi_Click(object sender, EventArgs e)
        {
            var myForm = new Emploi();
            myForm.Show();
        }



        //Charger les utilisateurs
        public static void Insert_Users(zkemkeeper.CZKEMClass axCZKEM1,string point)
        {
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                DataRow row;
                DataSet ds = new DataSet();
                string sEnrollNumber = "";
                bool bEnabled;
                int iPrivilege = 0;
                string sPassword;
                string sName;

                axCZKEM1.EnableDevice(iMachineNumber, false);
                axCZKEM1.ReadAllUserID(iMachineNumber);//read all the user information to the memory
                OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM Employer", conn);
                OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                adapter.Fill(ds, "Employer");
                while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))
                {
                    if (Int32.Parse(sEnrollNumber) > get_LastUser(point))
                    //Insert new Row
                    {
                        row = ds.Tables["Employer"].NewRow();
                        row["Identifiant"] = sEnrollNumber;
                        row["UserName"] = sName;
                        row["Pointeuse"] = point;
                        ds.Tables["Employer"].Rows.Add(row);
                    }

                }
                adapter.Update(ds, "Employer");
                ds.AcceptChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {
                conn.Close();
                axCZKEM1.EnableDevice(iMachineNumber, true);

            }

        }
        //Get lat user
        public static int get_LastUser(string point)
        {
            int ident = 0;
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select top 1 Identifiant from Employer", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    dAdapter = new OleDbDataAdapter("select max(Identifiant) from Employer where Pointeuse='"+point+"'", Settings.Default.PointeuseDBConnectionString);
                    cBuilder = new OleDbCommandBuilder(dAdapter);
                    DataTable dataTable1 = new DataTable();
                    dAdapter.Fill(dataTable1);
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        ident = Int32.Parse(dataTable1.Rows[i][0].ToString());

                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {
                conn.Close();

            }
            return ident;

        }

        public static DateTime get_LastPoint()
        {
            DateTime ident = new DateTime();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select top 1 Jour from Temp", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    dAdapter = new OleDbDataAdapter("select max(Jour) from Temp", Settings.Default.PointeuseDBConnectionString);
                    cBuilder = new OleDbCommandBuilder(dAdapter);
                    DataTable dataTable1 = new DataTable();
                    dAdapter.Fill(dataTable1);
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        if (DateTime.TryParse(dataTable1.Rows[i][0].ToString(), out ident) == true)
                            ident = DateTime.Parse(dataTable1.Rows[i][0].ToString());

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {
                conn.Close();

            }
            return ident;

        }
        public static DateTime get_LastCalPoint()
        {
            DateTime ident = new DateTime();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select top 1 DateP from PointageCal", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    dAdapter = new OleDbDataAdapter("select max(DateP) from PointageCal", Settings.Default.PointeuseDBConnectionString);
                    cBuilder = new OleDbCommandBuilder(dAdapter);
                    DataTable dataTable1 = new DataTable();
                    dAdapter.Fill(dataTable1);
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        if (DateTime.TryParse(dataTable1.Rows[i][0].ToString(), out ident) == true)
                            ident = DateTime.Parse(dataTable1.Rows[i][0].ToString());

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {
                conn.Close();

            }
            return ident;

        }

        public static DateTime get_MinPoint(DateTime datex)
        {
            DateTime ident = new DateTime();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select top 1 DateP from Pointage", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();
                dAdapter.Fill(dataTable);
                
                if (dataTable.Rows.Count > 0)
                {
                    DateTime dt = Convert.ToDateTime(datex);
                    string st = dt.ToString("yyyy/MM/dd HH:mm:ss");
                    dAdapter = new OleDbDataAdapter("select min(DateP) from Pointage where Pointage.DateP> #" + st + "#", Settings.Default.PointeuseDBConnectionString);
                    cBuilder = new OleDbCommandBuilder(dAdapter);
                    DataTable dataTable1 = new DataTable();
                    dAdapter.Fill(dataTable1);
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        if(DateTime.TryParse(dataTable1.Rows[i][0].ToString(),out ident)==true)
                        ident = DateTime.Parse(dataTable1.Rows[i][0].ToString());

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
            }
            finally
            {
                conn.Close();

            }
            return ident;

        }


        public static void Presence(DateTime k)

        {
            if (AttLogsMain.heurE2 == "")
            {
                MessageBox.Show("Faut définir l'emploi du temps dans les paramétres", "Erreur");
                return;
            }
            DataTable tabel1 = new DataTable();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataSet ds = new DataSet();
            DataRow row,row1=null;
            TimeSpan tt;
            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter(), dAdapter1 = new OleDbDataAdapter();
                string sql = null;
                //sql = "delete * from PointageCal where Jour= DateValue('" + k + "')";
                //sql = "delete * from Presence where Jour= DateValue('" + k + "')";
                //try
                //{
                //    dAdapter.DeleteCommand = conn.CreateCommand();
                //    dAdapter.DeleteCommand.CommandText = sql;
                //    dAdapter.DeleteCommand.ExecuteNonQuery();
                //}
                //catch (Exception ex)
                //{
                //    MessageBox.Show(ex.ToString());
                //    Program.log.Error("delete from pointagecal" + ex.Message.ToString());
                //}
                dAdapter = new OleDbDataAdapter("SELECT * FROM PointageCal", conn);
                OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(dAdapter);
                dAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                string id = "", date = "";
                dAdapter.Fill(ds, "PointageCal");
                dAdapter1 = new OleDbDataAdapter("SELECT * FROM Presence", conn);
                cmdBuilder = new OleDbCommandBuilder(dAdapter1);
                dAdapter1.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                dAdapter1.Fill(ds, "Presence");
                for (int i = 0; i < tabel1.Rows.Count; i++)
                {
                    row = ds.Tables["PointageCal"].NewRow();
                    row["IdUser"] = tabel1.Rows[i][0];
                    row["Jour"] = tabel1.Rows[i][1];
                    row["Entrée"] = tabel1.Rows[i][2];
                    row["Sortie"] = tabel1.Rows[i][3];
                    row["NomEmp"] = tabel1.Rows[i][4];
                    row["Prenomemp"] = tabel1.Rows[i][5];
                    row["Matricule"] = tabel1.Rows[i][6];
                    row["Poste"] = tabel1.Rows[i][7];
                    row["Struct"] = tabel1.Rows[i][8];
                    if(row!=null)
                    ds.Tables["PointageCal"].Rows.Add(row);
                    if(id!= tabel1.Rows[i][0].ToString() || date!= tabel1.Rows[i][1].ToString())
                    {
                        if (id != "" && date!= "")
                        {

                            ds.Tables["Presence"].Rows.Add(row1);

                        }
                        row1 = ds.Tables["Presence"].NewRow();
                        row1["AM"] = "Absent";
                        row1["PM"] = "Absent";
                    }
                    id= tabel1.Rows[i][0].ToString();
                    date = tabel1.Rows[i][1].ToString();
                    row1["IdUser"] =id;
                    row1["Jour"] = date;
                    if (TimeSpan.TryParse(heurE2, out tt) == true)
                    {
                        if (TimeSpan.TryParse(tabel1.Rows[i][2].ToString(), out tt) == true)
                        {
                            if (TimeSpan.Parse(tabel1.Rows[i][2].ToString()) <= TimeSpan.Parse(heurE2))
                                row1["AM"] = "Présent";
                        }
                        if (TimeSpan.TryParse(tabel1.Rows[i][3].ToString(), out tt) == true)
                        {
                            if (TimeSpan.Parse(tabel1.Rows[i][3].ToString()) > TimeSpan.Parse(heurE2))
                                row1["PM"] = "Présent";
                        }
                    }
                }
                if(row1!=null)
                ds.Tables["Presence"].Rows.Add(row1);
                dAdapter.Update(ds, "PointageCal");
                dAdapter1.Update(ds, "Presence");
                ds.AcceptChanges();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {

                conn.Close();

            }

        }
        #endregion
    }



}