﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class Login : MetroForm
    {
        public static string idUser;
        public static string idPro;
        public static string usern;
        
        public Login()
        {
            InitializeComponent();
            this.FormClosing += Form_FormClosing;
        }
        void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user = txtUsername.Text;
            string pass = txtPassword.Text;
            string profile = "";
            string us = "";
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
            DataTable dataTable = new DataTable();
            OleDbDataAdapter adapter;
            OleDbCommandBuilder cmdBuilder;
                try
                {
                    conn.Open();
                    adapter = new OleDbDataAdapter("SELECT *  FROM Users", conn);
                    cmdBuilder = new OleDbCommandBuilder(adapter);
                    adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    adapter.Fill(dataTable);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (dataTable.Rows[i]["UserName"].ToString() == user && dataTable.Rows[i]["Pwd"].ToString()==pass)
                        {
                        profile = dataTable.Rows[i]["IdProfile"].ToString();
                        idUser= dataTable.Rows[i]["ID"].ToString();
                        us= dataTable.Rows[i]["UserName"].ToString();
                        i = dataTable.Rows.Count;

                        }
                    }
                idPro = profile;
                usern = us;
                }
                catch (OleDbException exp)
                {
                    MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                    Program.log.Error("login,select users" + exp.Message.ToString());
                }

                if (conn.State == ConnectionState.Open)
                    conn.Close();
            if (profile == "")
            {
                MessageBox.Show("Utilisateur ou mot de passe incorrect ", "login field!!");
                Program.log.Info("login field!!");
            }
            else
            
            {
                Program.log.Info("login:" + user);
                var FormRH = new AttLogsMain();
                FormRH.Show();
                this.Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //private void Login_Load(object sender, EventArgs e)
        //{
           
        //        this.ActiveControl = txtUsername;
            
        //}
    }
}
