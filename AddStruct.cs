﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.OleDb;
using AttLogs.Properties;

namespace AttLogs
{
    public partial class AddStruct : MetroForm
    {
        public AddStruct()
        {
            InitializeComponent();
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void add_Click(object sender, EventArgs e)
        {
            string name = namSt.Text;
            if (name != "")
            {
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                DataRow row;
                // DataSet ds = new PointeuseDBDataSet1();
                DataTable dataTable = new DataTable();
                Cursor = Cursors.WaitCursor;
                try
                {
                    conn.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT *  FROM Structures", conn);
                    OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
                    adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    adapter.Fill(dataTable);

                    //Insert new Row

                    row = dataTable.NewRow();
                    row["lib"] = name.ToString();
                    dataTable.Rows.Add(row);
                    adapter.Update(dataTable);
                    dataTable.AcceptChanges();
                    MessageBox.Show("la nouvelle Structure a été enregistrée", "succès");

                    this.Close();
                    Structure.DisplayData();
                    Program.log.Info("Ajouter une Structure," + name);



                }


                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message.ToString());
                    Program.log.Error("Ajouter une pointeuse," + exp.Message.ToString());
                }
            }
            else
            {
                MessageBox.Show("veuillez Introduire le nom", "Alert");
            }

        }

        
    }
}
