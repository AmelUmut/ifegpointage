﻿using System;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using AttLogs.Properties;
using System.Collections;
using System.Data.OleDb;

namespace AttLogs
{
    public partial class PresentRepport : MetroForm
    {
        public PresentRepport()
        {
            InitializeComponent();
            get_Struct();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
               

                Program.log.Info("Display general repport");
                DictionaryEntry deImgType;
                string ident = "";
                string emp = "";
                if (structs.SelectedItem != null)
                {
                    deImgType = (DictionaryEntry)structs.SelectedItem;
                    ident = deImgType.Key.ToString();
                    emp = deImgType.Value.ToString();
                }
                else
                {
                    MessageBox.Show("Veuillez sélectiooner une Structure ", "Alert");
                    return;
                }
                if ((startdate1.Text != enddate1.Text) && (Convert.ToDateTime(startdate1.Text.ToString()) > Convert.ToDateTime(enddate1.Text.ToString())))
                {
                    MessageBox.Show("la date de début doit être inférieure ou égale à la date de fin!! ", "Alert");
                    return;
                }
                string sql = null,jour;
                ReportDocument objRpt = new CrystalReport3();
                DataTable dataTable = new DataTable();
                DataTable dataTable1 = new DataTable();
                OleDbCommand oleDbCmd = new OleDbCommand();
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;
                conn.Open();
                sql = "select distinct Jour from Presence where DateValue(Jour) >= DateValue('" + startdate1.Text + "') and DateValue(Jour) <= DateValue('" + enddate1.Text + "')  ";
                OleDbDataAdapter dAdapter;
                OleDbCommandBuilder cBuilder;
                OleDbDataAdapter dAdapter1 = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilde1r = new OleDbCommandBuilder(dAdapter1);
                dAdapter1.Fill(dataTable1);
                if (dataTable1.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        jour = dataTable1.Rows[i][0].ToString();
                        sql = "select IIF(Jour IS NULL,DateValue('" +jour+ "'),Jour)AS aa,Nom,Prenom, IIF(AM IS NULL,'Absent',AM)AS AMidi, IIF(PM IS NULL,'Absent',PM)As ApMidi,IIF(Retard IS NULL,'--',Retard)AS Retard,LibSP from Employer LEFT OUTER JOIN Presence on(Presence.IdUser = Employer.Identifiant and DateValue(Presence.Jour) = DateValue('" + jour + "') )  where IsActif='1' and  LibSP like '%" + emp + "%' order by Nom";
                        dAdapter = new OleDbDataAdapter(sql, Settings.Default.PointeuseDBConnectionString);
                        cBuilder = new OleDbCommandBuilder(dAdapter);
                        dAdapter.Fill(dataTable);
                    }
                }
                DataSet1 ds = new DataSet1();
                DataTable t = ds.Tables.Add("Items");
                t.Columns.Add("Jour", Type.GetType("System.String"));
                t.Columns.Add("Nom", Type.GetType("System.String"));
                t.Columns.Add("Prenom", Type.GetType("System.String"));
                t.Columns.Add("AM", Type.GetType("System.String"));
                t.Columns.Add("PM", Type.GetType("System.String"));
                t.Columns.Add("SP", Type.GetType("System.String"));
                DataRow r;
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    r = t.NewRow();
                    r["Jour"] = dataTable.Rows[i][0];
                    r["Nom"] = dataTable.Rows[i][1];
                    r["Prenom"] = dataTable.Rows[i][2];
                    r["AM"] = dataTable.Rows[i][3];
                    r["PM"] = dataTable.Rows[i][4];
                    r["SP"] = dataTable.Rows[i][5];
                    t.Rows.Add(r);
                }
                
                objRpt.SetDataSource(ds.Tables[1]);
               // objRpt.SetParameterValue("orderdate", jour.Text);
                ParameterFieldDefinitions crParameterFieldDefinitions;
                ParameterFieldDefinition crParameterFieldDefinition;
                ParameterValues crParameterValues = new ParameterValues();
                ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();

                crParameterDiscreteValue.Value = startdate1.Text;
                crParameterFieldDefinitions = objRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["orderdate"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crParameterDiscreteValue.Value = enddate1.Text;
                crParameterFieldDefinitions = objRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["orderdate1"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crParameterDiscreteValue.Value = emp;
                crParameterFieldDefinitions = objRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["structure"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                //objRpt.SetParameterValue("structure", emp);

                crystalReportViewer1.ReportSource = objRpt;
                crystalReportViewer1.Refresh();
                crystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
                crystalReportViewer1.ShowGroupTreeButton = false;
                crystalReportViewer1.ShowParameterPanelButton = false;

            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message.ToString());
                Program.log.Error("Change pwd," + exp.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void get_Struct()
        {

            this.structs.Items.Clear();
            OleDbCommand oleDbCmd = new OleDbCommand();
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Settings.Default.PointeuseDBConnectionString;

            try
            {
                conn.Open();
                OleDbDataAdapter dAdapter = new OleDbDataAdapter("select ID,lib from Structures", Settings.Default.PointeuseDBConnectionString);
                OleDbCommandBuilder cBuilder = new OleDbCommandBuilder(dAdapter);
                DataTable dataTable = new DataTable();

                dAdapter.Fill(dataTable);
                string ident, nom;
                Hashtable htPointInf = new Hashtable();
                int i;
                for (i = 0; i < dataTable.Rows.Count; i++)
                {
                    ident = dataTable.Rows[i]["lib"].ToString();
                    nom = dataTable.Rows[i]["lib"].ToString();
                    htPointInf.Add(ident, nom);

                }
                if (i > 0)
                {

                    foreach (DictionaryEntry PointInf in htPointInf)
                    {
                        structs.Items.Add(PointInf);
                    }
                    structs.DisplayMember = "value";
                    structs.ValueMember = "key";
                    structs.SelectedIndex = -1;
                    structs.AutoCompleteMode = AutoCompleteMode.Suggest;
                    structs.AutoCompleteSource = AutoCompleteSource.ListItems;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ", "Erreur");
                Program.log.Error("pointageGrid_get_Users" + ex.Message.ToString());
            }
            finally
            {

                conn.Close();
            }
        }

    }
}
