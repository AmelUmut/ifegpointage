﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace AttLogs
{
    public partial class AttLog : MetroForm
    {
        public AttLog()
        {
            InitializeComponent();
            Program.log.Info("get log data");
           
            int iMachineNumber = 1;
            bool bIsConnected = false;
            for (int i = 0; i < AttLogsMain.IsConnected.Count; i++)
            {
                if (AttLogsMain.IsConnected[i] == true)
                    bIsConnected = true;
            }
            if (bIsConnected == false)
            {
                MessageBox.Show("Aucune pointeuse n'est Connectée!", "Erreur");
                return;
            }
            lvLogs.Items.Clear();
            int iGLCount = 0;
            int iIndex = 0;
            Cursor = Cursors.WaitCursor;
            try
            {
                for (int i = 0; i < AttLogsMain.axCZKEM.Count; i++)
                {
                    if (AttLogsMain.IsConnected[i] == true)
                    {
                        string sdwEnrollNumber = "";
                        //int idwTMachineNumber=0;
                        //int idwEMachineNumber=0;
                        int idwVerifyMode = 0;
                        int idwInOutMode = 0;
                        int idwYear = 0;
                        int idwMonth = 0;
                        int idwDay = 0;
                        int idwHour = 0;
                        int idwMinute = 0;
                        int idwSecond = 0;
                        int idwWorkcode = 0;

                        int idwErrorCode = 0;




                        AttLogsMain.axCZKEM[i].EnableDevice(iMachineNumber, false);//disable the device
                        if (AttLogsMain.axCZKEM[i].ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
                        {
                            while (AttLogsMain.axCZKEM[i].SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                                       out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                            {
                                iGLCount++;
                                lvLogs.Items.Add(iGLCount.ToString());
                                lvLogs.Items[iIndex].SubItems.Add(sdwEnrollNumber);//modify by Darcy on Nov.26 2009
                                                                                   //lvLogs.Items[iIndex].SubItems.Add(idwVerifyMode.ToString());
                                                                                   //lvLogs.Items[iIndex].SubItems.Add(idwInOutMode.ToString());
                                lvLogs.Items[iIndex].SubItems.Add(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                                lvLogs.Items[iIndex].SubItems.Add(AttLogsMain.CheckBoxes[i].Text);
                                iIndex++;
                            }
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            AttLogsMain.axCZKEM[i].GetLastError(ref idwErrorCode);

                            if (idwErrorCode != 0)
                            {
                                MessageBox.Show(AttLogsMain.CheckBoxes[i].Text + " impossible de lire les pointages,ErrorCode: " + idwErrorCode.ToString(), "Error");
                            }
                            else
                            {
                                MessageBox.Show("Aucun pointage dans " + AttLogsMain.CheckBoxes[i].Text + "!", "Error");
                            }
                        }
                        AttLogsMain.axCZKEM[i].EnableDevice(iMachineNumber, true);//enable the device
                    }
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                MessageBox.Show("Impossible de se connecter à la base de données", "Erreur");
                Program.log.Error("Display pointeuse list" + ex.Message.ToString());
            }
           
                
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
